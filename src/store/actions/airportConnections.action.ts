import { GET_AIRPORT_CONNECTIONS, AirportConnectionsActionType } from "../actionTypes/airportConnections.action.type";
import { AirportConnection } from "../../models/api/airportConnection.model";

/**
 * Akcja zapisania pobranych połączeń lotniczych w magazynie
 * @param airportConnections Tablica połączeń lotniczych
 */
export const getAirportConnectionsAction = (airportConnections: AirportConnection[]): AirportConnectionsActionType => ({
    type: GET_AIRPORT_CONNECTIONS,
    payload: airportConnections
});