import { DashboardFilterChangeActionType } from "../actionTypes/dashboardFilterChange.action.type";
import { DASHBOARD_FILTER_CHANGE_FIELD } from '../actionTypes/dashboardFilterChange.action.type';

export const dashboardChangeField = (field: string, value: any): DashboardFilterChangeActionType => ({
    type: DASHBOARD_FILTER_CHANGE_FIELD,
    fieldName: field,
    fieldValue: value
})