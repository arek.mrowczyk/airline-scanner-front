import { LOGIN_FORM_CHANGE_VALUE, LOGIN_FORM_CLEAR, LoginFormActionType } from "../actionTypes/loginForm.action.type";
import { FormControl } from "../../models/formControl.model";

/**
 * Akcja zmiany pola w formularzu logowania
 * @param fields Pola formularza
 */
export const loginFormUpdateAction = (fields: FormControl[]): LoginFormActionType => {

    return {
        type: LOGIN_FORM_CHANGE_VALUE,
        fields: fields
    }
};

/**
 * Akcja czyszczenia formularza logowania
 */
export const clearLoginFormAction = (): LoginFormActionType => ({
    type: LOGIN_FORM_CLEAR
});

/**
 * Akcja ustawienia komunikatu błędu przy nieudanej próbie logowania
 * @param errorMessage Komunikat błędu
 */
// export const setLoginErrorMessage = (errorMessage: string): LoginFormActionType => ({
//     type: SET_LOGIN_ERROR,
//     error: errorMessage
// });