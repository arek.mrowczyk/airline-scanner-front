import { LOGOUT, LOGIN, UPDATE_USER_DATA, AuthDataActionType } from "../actionTypes/authData.action.type";
import { LoginFormActionType } from '../actionTypes/loginForm.action.type';
import { User } from "../../models/api/user.model";

/**
 * Akcja zmiany statusu logowania na zalogowany
 * @param actionData Obiekt z polami error oraz value gdzie value to token dostępu użytkownika
 */
export const loginAction = (userData: User): AuthDataActionType | LoginFormActionType => {
    return {
        type: LOGIN,
        payload: userData
    };
};

/**
 * Akcja zmiany statusu logowania na wylogowany
 */
export const logoutAction = (): AuthDataActionType => ({
    type: LOGOUT,
});


export const updateUserDataAction = (userData: User): AuthDataActionType => {
    return {
        type: UPDATE_USER_DATA,
        payload: userData
    };
};