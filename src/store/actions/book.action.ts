import { Flight } from "../../models/api/flight.model";
import { BookActionType, BOOK_SET_FLIGHT, BOOK_SET_STEP, BOOK_RESET_STATE, BOOK_SET_PERSONS, BOOK_SET_SEATS } from "../actionTypes/book.action.type";
import { Person } from "../../models/api/person.model";

export const bookSetFlightAction = (flight: Flight): BookActionType => {
    return {
        type: BOOK_SET_FLIGHT,
        flight: flight
    };
};

export const bookSetStepAction = (step: number): BookActionType => {
    return {
        type: BOOK_SET_STEP,
        step: step
    };
};

export const bookResetStateAction = (): BookActionType => {
    return {
        type: BOOK_RESET_STATE
    };
};

export const bookSetPersonsAction = (persons: Person[]): BookActionType => {
    return {
        type: BOOK_SET_PERSONS,
        persons: persons
    }
}

export const bookSetSeatsAction = (seats: any[]): BookActionType => {
    return {
        type: BOOK_SET_SEATS,
        seats: seats
    }
}