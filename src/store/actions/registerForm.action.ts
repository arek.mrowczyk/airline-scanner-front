import { REGISTER_FORM_CHANGE_VALUE, RegisterFormActionType, REGISTER_FORM_CLEAR } from "../actionTypes/registerForm.action.type";
import { FormControl } from "../../models/formControl.model";

/**
 * Akcja zmiany pola w formularzu rejestracji
 * @param fields Pola formularza
 */
export const registerFormAction = (fields: FormControl[]): RegisterFormActionType => ({
    type: REGISTER_FORM_CHANGE_VALUE,
    fields: fields
});

/**
 * Akcja czyszczenia formularza rejestracji
 */
export const clearRegisterFormAction = (): RegisterFormActionType => ({
    type: REGISTER_FORM_CLEAR
});