import { GET_FLIGHTS, FlightsActionType } from "../actionTypes/flight.action.type";
import { Flight } from "../../models/api/flight.model";

/**
 * Akcja zapisania pobranych lotów w magazynie
 * @param flights Tablica lotów
 */
export const getFlightsAction = (flights: Flight[]): FlightsActionType => ({
    type: GET_FLIGHTS,
    payload: flights
});