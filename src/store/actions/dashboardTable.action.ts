import { DashboardTableActionType, DASHBOARD_TABLE_CHANGE } from "../actionTypes/dashboardTable.action.type";
import { TableData } from "../../models/tableData.model";

export const changeDashboardTable = (table: TableData): DashboardTableActionType => ({
    type: DASHBOARD_TABLE_CHANGE,
    payload: table
})