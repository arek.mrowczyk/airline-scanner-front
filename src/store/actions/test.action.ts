import { TestActionType, SEND_TYPE } from "../actionTypes/test.action.type";

/**
 * Testowa akcja
 * @param message Testowa wiadomość
 */
export const testAction = (message: string): TestActionType => {
    return {
        type: SEND_TYPE,
        message: message
    }
}