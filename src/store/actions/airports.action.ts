import { GET_AIRPORTS, AirportsActionType } from "../actionTypes/airports.action.type";
import { Airport } from "../../models/api/airport.model";

/**
 * Akcja zapisania pobranych lotnisk w magazynie
 * @param airports Tablica lotnisk
 */
export const getAirportsAction = (airports: Airport[]): AirportsActionType => ({
    type: GET_AIRPORTS,
    payload: airports
});