import { GET_COUNTRIES, CountriesActionType } from "../actionTypes/countries.action.type";
import { Country } from "../../models/api/country.model";

/**
 * Akcja zapisania pobranych państw w magazynie
 * @param countries Tablica państw
 */
export const getCountriesAction = (countries: Country[]): CountriesActionType => ({
    type: GET_COUNTRIES,
    payload: countries
});