import { Reducer } from "redux";
import { TestActionType, SEND_TYPE } from '../actionTypes/test.action.type';

/**
 * Testowy reducer
 */
export const testReducer: Reducer<string, TestActionType> = (state = '', action: TestActionType) => {
    if (action.type === SEND_TYPE) {
        console.log(`Reducer fired! - ${action.message}`);

        return action.message;
    }
    return state;
}