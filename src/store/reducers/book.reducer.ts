import { Reducer } from "redux";
import { CountriesActionType, GET_COUNTRIES } from "../actionTypes/countries.action.type";
import { Country } from "../../models/api/country.model";
import { BookState } from "../../models/book.model";
import { BookActionType, BOOK_SET_FLIGHT, BOOK_SET_STEP, BOOK_RESET_STATE, BOOK_SET_PERSONS, BOOK_SET_SEATS } from "../actionTypes/book.action.type";
import { Flight } from "../../models/api/flight.model";
import { Person } from "../../models/api/person.model";

const initial: BookState = {
    currentStep: 0,
    persons: [],
    seats: [],
    flight: {
        id: -1,
        aircraftModel: '',
        fare: 0,
        availableSeats: 0,
        date: [0, 0, 0, 0, 0],
        seat: {
            seats: '',
            id: 0
        },
        connection: {
            id: 0,
            flightType: '',
            active: false,
            originAirport: {
                id: 0,
                airportName: '',
                airportCode: '',
                cityName: '',
                cityCode: '',
                country: {
                    id: 0,
                    name: '',
                    code: ''
                }
            },
            destinationAirport: {
                id: 0,
                airportName: '',
                airportCode: '',
                cityName: '',
                cityCode: '',
                country: {
                    id: 0,
                    name: '',
                    code: ''
                }
            }
        }
    }
};

/**
 * Reducer zmieniający stan tablicy państw
 * @param state Stan tablicy państw
 * @param action Typ akcji wykonywaniej na tablicy państw
 */
export const bookReducer: Reducer<BookState, BookActionType> = (state = initial, action: BookActionType) => {

    if (action.type === BOOK_SET_FLIGHT) {
        const res = { ...state };
        res.flight = action.flight as Flight;
        return res;
    }

    if (action.type === BOOK_SET_STEP) {
        const res = { ...state };
        res.currentStep = action.step as number;
        return res;
    }

    if (action.type === BOOK_RESET_STATE) {
        const res = {...initial};
        return res;
    }

    if(action.type === BOOK_SET_PERSONS) {
        const res = {...state};
        res.persons = action.persons as Person[];
        return res;
    }

    if(action.type === BOOK_SET_SEATS) {
        const res = {...state};
        res.seats = action.seats as any[];
        return res;
    }

    return state;
}