import { Reducer } from "redux";
import { AirportConnectionsActionType, GET_AIRPORT_CONNECTIONS } from "../actionTypes/airportConnections.action.type";
import { AirportConnection } from "../../models/api/airportConnection.model";

const initial: AirportConnection[] = [];

/**
 * Reducer zmieniający stan tablicy połączeń lotniczych
 * @param state Stan tablicy połączeń lotniczych
 * @param action Typ akcji wykonywaniej na tablicy połączeń lotniczych
 */
export const airportConnectionsReducer: Reducer<AirportConnection[], AirportConnectionsActionType> = (state = initial, action: AirportConnectionsActionType) => {

    if (action.type === GET_AIRPORT_CONNECTIONS) {
        if (Array.isArray(action.payload)) {
            return [...action.payload];
        }
    }

    return state;
}