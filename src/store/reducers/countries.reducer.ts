import { Reducer } from "redux";
import { CountriesActionType, GET_COUNTRIES } from "../actionTypes/countries.action.type";
import { Country } from "../../models/api/country.model";

const initial: Country[] = [];

/**
 * Reducer zmieniający stan tablicy państw
 * @param state Stan tablicy państw
 * @param action Typ akcji wykonywaniej na tablicy państw
 */
export const countriesReducer: Reducer<Country[], CountriesActionType> = (state = initial, action: CountriesActionType) => {

    if (action.type === GET_COUNTRIES) {
        if (Array.isArray(action.payload)) {
            return [...action.payload];
        }
    }

    return state;
}