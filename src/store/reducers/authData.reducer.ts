import { Reducer } from "redux";
import { AuthData } from "../../models/authData.model";
import { AuthDataActionType, LOGIN, LOGOUT, UPDATE_USER_DATA } from "../actionTypes/authData.action.type";
import { Authority } from "../../models/api/user.model";

const getIsAdmin = (authoritiesArr: Authority[]): boolean => !!authoritiesArr.find(a=>a.authority === "ADMIN");

const initial: AuthData = {
    isLogged: false,
    user: undefined,
};

/**
 * Reducer zmieniający stan danych związanych z autoryzacją użytkownika
 * @param state Stan danych autoryzacji
 * @param action Typ akcji wykonywanej na danych autoryzacji
 */
export const authDataReducer: Reducer<AuthData, AuthDataActionType> = (state = initial, action: AuthDataActionType) => {

    if (action.type === LOGIN) {
        const res: AuthData = {
            ...state
        };
        if (action.payload) {
            res.isLogged = true;
            res.user = action.payload;
            res.user.isAdmin = getIsAdmin(res.user.authorities)

            return res;
        }
    }

    if (action.type === LOGOUT) {
        const res: AuthData = {
            ...state
        };

        res.isLogged = false;
        res.user = undefined;

        return res;
    }

    if (action.type === UPDATE_USER_DATA) {
        const res: AuthData = {
            ...state
        };

        res.user = action.payload;

        return res;
    }

    return state;
}