import { Reducer } from "redux";
import { FlightsActionType, GET_FLIGHTS } from "../actionTypes/flight.action.type";
import { Flight } from "../../models/api/flight.model";

const initial: Flight[] = [];

/**
 * Reducer zmieniający stan tablicy lotów
 * @param state Stan tablicy lotów
 * @param action Typ akcji wykonywaniej na tablicy lotów
 */
export const flightsReducer: Reducer<Flight[], FlightsActionType> = (state = initial, action: FlightsActionType) => {

    if (action.type === GET_FLIGHTS) {
        if (Array.isArray(action.payload)) {
            return [...action.payload];
        }
    }

    return state;
}