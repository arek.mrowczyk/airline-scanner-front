import { Reducer } from "redux";
import { RegisterForm } from "../../models/registerForm.model";
import { RegisterFormActionType, REGISTER_FORM_CHANGE_VALUE, REGISTER_FORM_CLEAR } from "../actionTypes/registerForm.action.type";

const initial: RegisterForm = {
    fields: [
        { fieldName: 'username', touched: false, fieldValue: '', error: { message: '', info: {} } },
        { fieldName: 'password', touched: false, fieldValue: '', error: { message: '', info: {} } },
        { fieldName: 'confirmPassword', touched: false, fieldValue: '', error: { message: '', info: {} } },
        { fieldName: 'firstName', touched: false, fieldValue: '', error: { message: '', info: {} } },
        { fieldName: 'lastName', touched: false, fieldValue: '', error: { message: '', info: {} } },
        { fieldName: 'dateOfBirth', touched: false, fieldValue: '', error: { message: '', info: {} } },
        { fieldName: 'accepted', touched: false, fieldValue: false, error: { message: '', info: {} } }
    ]
}

/**
 * Reducer zmieniający stan formularza rejestracji
 * @param state Stan formularza rejestracji
 * @param action Typ akcji wykonywaniej na formularzu rejestracji
 */
export const registerFormReducer: Reducer<RegisterForm, RegisterFormActionType> = (state = initial, action: RegisterFormActionType) => {

    if (action.type === REGISTER_FORM_CHANGE_VALUE) {
        const res = {
            ...state
        };

        if (action.fields) {
            res.fields = action.fields;
        }

        return res;
    }

    if (action.type === REGISTER_FORM_CLEAR) {
        const res: RegisterForm = {
            fields: []
        };

        for (let i = 0; i < state.fields.length; i++) {
            const field = state.fields[i];

            if (field.fieldName === 'accepted') {
                field.fieldValue = false;
            }
            else {
                field.fieldValue = '';
            }

            field.error.info = {};
            field.error.message = ''
            field.touched = false;

            res.fields.push(field);
        }

        return res;
    }

    return state;
}