import { Reducer } from "redux";
import { DashboardFilterState } from "../../models/dashboard.filter.model";
import { DashboardFilterChangeActionType, DASHBOARD_FILTER_CHANGE_FIELD } from "../actionTypes/dashboardFilterChange.action.type";

const initial: DashboardFilterState = {
    countries: [],
    countryFrom: undefined,
    countryTo: undefined,
    airportsFrom: [],
    airportsTo: [],
    airportFrom: undefined,
    airportTo: undefined,
    personCount: 1
};

/**
 * Reducer zmieniający stan danych związanych z autoryzacją użytkownika
 * @param state Stan danych autoryzacji
 * @param action Typ akcji wykonywanej na danych autoryzacji
 */
export const dashboardReducer: Reducer<DashboardFilterState, DashboardFilterChangeActionType> = (state = initial, action: DashboardFilterChangeActionType) => {

    if (action.type === DASHBOARD_FILTER_CHANGE_FIELD) {
        if (action.fieldName === 'countryFrom') {
            const res = { ...state };

            res.countryFrom = res.countries.filter(x => x.name === action.fieldValue as string)[0];

            return res;
        } else if (action.fieldName === 'airportsFrom') {
            const res = { ...state };

            res.airportsFrom = action.fieldValue;

            return res;
        } else if (action.fieldName === 'countries') {
            const res = { ...state };

            res.countries = action.fieldValue;

            return res;
        } else if (action.fieldName === 'airportFrom') {
            const res = { ...state };

            res.airportFrom = res.airportsFrom.filter(x => x.id + '' === action.fieldValue)[0];

            return res;
        } else if (action.fieldName === 'countryTo') {
            const res = { ...state };

            res.countryTo = res.countries.filter(x => x.name === action.fieldValue as string)[0];

            return res;
        } else if (action.fieldName === 'airportsTo') {
            const res = { ...state };

            res.airportsTo = action.fieldValue;

            return res;
        } else if (action.fieldName === 'airportTo') {
            const res = { ...state };

            res.airportTo = res.airportsTo.filter(x => x.id + '' === action.fieldValue)[0];

            return res;
        } else if (action.fieldName === 'personCount') {
            const res = { ...state };

            res.personCount = action.fieldValue;

            return res;
        }
    }

    return state;
}