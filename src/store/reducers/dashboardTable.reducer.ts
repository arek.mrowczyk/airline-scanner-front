import { Reducer } from "redux";
import { DashboardTableState } from "../../models/dashboardTable.model";
import { DashboardTableActionType, DASHBOARD_TABLE_CHANGE } from "../actionTypes/dashboardTable.action.type";

const initial: DashboardTableState = {
    table: {
        columns: [
            {
                name: 'Wylot z'
            },
            {
                name: 'Do'
            },
            {
                name: 'Koszt'
            },
            {
                name: 'Ilość miejsc'
            }
        ],
        rows: [{
            data: {},
            actions: {
                components: []
            }
        }]
    }
};

/**
 * Reducer zmieniający stan danych związanych z autoryzacją użytkownika
 * @param state Stan danych autoryzacji
 * @param action Typ akcji wykonywanej na danych autoryzacji
 */
export const dashboardTableReducer: Reducer<DashboardTableState, DashboardTableActionType> = (state = initial, action: DashboardTableActionType) => {

    if (action.type === DASHBOARD_TABLE_CHANGE) {
        const res = { ...state };

        res.table = action.payload;

        return res;
    }

    return state;
}