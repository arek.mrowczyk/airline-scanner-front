import { Reducer } from "redux";
import { LoginForm } from "../../models/loginForm.model";
import { LoginFormActionType, LOGIN_FORM_CHANGE_VALUE, LOGIN_FORM_CLEAR, LOGIN_SET_ERROR } from "../actionTypes/loginForm.action.type";

const initial: LoginForm = {
    fields: [
        { fieldName: 'username', touched: false, fieldValue: '', error: { message: '', info: {} } },
        { fieldName: 'password', touched: false, fieldValue: '', error: { message: '', info: {} } }
    ],
    error: '',
};

/**
 * Reducer zmieniający stan formularza logowania
 * @param state Stan formularza logowania
 * @param action Typ akcji wykonywanej na formularzu logowania
 */
export const loginFormReducer: Reducer<LoginForm, LoginFormActionType> = (state = initial, action: LoginFormActionType) => {

    if (action.type === LOGIN_FORM_CHANGE_VALUE) {
        const res = {
            ...state
        };

        if (action.fields) {
            res.fields = action.fields;
        }

        return res;
    }

    if (action.type === LOGIN_FORM_CLEAR) {
        const res: LoginForm = {
            fields: [],
            error: '',
        };

        for (let i = 0; i < state.fields.length; i++) {
            const field = state.fields[i];

            field.fieldValue = '';
            field.error.info = {};
            field.error.message = ''
            field.touched = false;
            res.fields.push(field);
        }

        return res;
    }

    if (action.type === LOGIN_SET_ERROR) {
        const res = {
            ...state
        };

        if(action.error) {
            res.error = action.error;
        }

        return res;
    }

    return state;
}