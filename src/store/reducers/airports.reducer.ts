import { Reducer } from "redux";
import { AirportsActionType, GET_AIRPORTS } from "../actionTypes/airports.action.type";
import { Airport } from "../../models/api/airport.model";

const initial: Airport[] = [];

/**
 * Reducer zmieniający stan tablicy lotnisk
 * @param state Stan tablicy lotnisk
 * @param action Typ akcji wykonywaniej na tablicy lotnisk
 */
export const airportsReducer: Reducer<Airport[], AirportsActionType> = (state = initial, action: AirportsActionType) => {

    if (action.type === GET_AIRPORTS) {
        if (Array.isArray(action.payload)) {
            return [...action.payload];
        }
    }

    return state;
}