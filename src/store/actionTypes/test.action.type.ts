
/**
 * Testowy typ akcji
 */
export interface TestActionType {
    type: typeof SEND_TYPE;
    message: string;
}

/**
 * Testowy typ wysłania wiadomości
 */
export const SEND_TYPE = 'SEND_MESSAGE';