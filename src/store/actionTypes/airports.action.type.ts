import { Airport } from "../../models/api/airport.model";


/**
 * Typ opisujący typ akcji zmiany w tablicy lotnisk
 */
export interface AirportsActionType {
    type: typeof GET_AIRPORTS;
    payload?: Airport | Airport[];
}

/**
 * Typ opisujący operację pobrania listy lotnisk
 */
export const GET_AIRPORTS = 'GET_AIRPORTS';