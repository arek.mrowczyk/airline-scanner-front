import { Country } from "../../models/api/country.model";


/**
 * Typ opisujący typ akcji zmiany w tablicy państw
 */
export interface CountriesActionType {
    type: typeof GET_COUNTRIES;
    payload?: Country | Country[];
}

/**
 * Typ opisujący operację pobrania listy państw
 */
export const GET_COUNTRIES = 'GET_COUNTRIES';