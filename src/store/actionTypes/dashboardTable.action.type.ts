import { TableData } from "../../models/tableData.model";

export interface DashboardTableActionType {
    type: typeof DASHBOARD_TABLE_CHANGE;
    payload: TableData;
}

export const DASHBOARD_TABLE_CHANGE = 'DASHBOARD_TABLE_CHANGE';