import { AirportConnection } from "../../models/api/airportConnection.model";


/**
 * Typ opisujący typ akcji zmiany w tablicy połączeń lotniczych
 */
export interface AirportConnectionsActionType {
    type: typeof GET_AIRPORT_CONNECTIONS;
    payload?: AirportConnection | AirportConnection[];
}

/**
 * Typ opisujący operację pobrania listy połączeń lotniczych
 */
export const GET_AIRPORT_CONNECTIONS = 'GET_AIRPORT_CONNECTIONS';