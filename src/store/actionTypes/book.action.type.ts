import { Flight } from "../../models/api/flight.model";
import { Person } from "../../models/api/person.model";

export interface BookActionType {
    type: typeof BOOK_SET_FLIGHT | typeof BOOK_SET_STEP | typeof BOOK_RESET_STATE | typeof BOOK_SET_PERSONS | typeof BOOK_SET_SEATS;
    flight?: Flight;
    step?: number;
    persons?: Person[];
    seats?: any[];
}

export const BOOK_SET_FLIGHT = 'BOOK_SET_FLIGHT';

export const BOOK_SET_STEP = 'BOOK_SET_STEP';

export const BOOK_RESET_STATE = 'BOOK_RESET_STATE';

export const BOOK_SET_PERSONS = 'BOOK_SET_PERSONS';

export const BOOK_SET_SEATS = 'BOOK_SET_SEATS';