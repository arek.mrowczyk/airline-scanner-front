import { User } from "../../models/api/user.model";


/**
 * Typ opisujący typ akcji zmiany danych autoryzacji
 */
export interface AuthDataActionType {
    type: typeof LOGIN | typeof LOGOUT | typeof UPDATE_USER_DATA;
    payload?: User;
}

/**
 * Typ opisujący operację zmiany w danych autoryzacji podczas logowania użytkownika
 */
export const LOGIN = 'LOGIN';

/**
 * Typ opisujący operację zmiany w danych autoryzacji podczas wylogowywania użytkownika
 */
export const LOGOUT = 'LOGOUT';

/**
 * Typ opisujący operację aktualizacji danych użytkownika
 */
export const UPDATE_USER_DATA = 'UPDATE_USER_DATA';
