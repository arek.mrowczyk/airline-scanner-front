import { FormControl } from "../../models/formControl.model";

/**
 * Typ opisujący typ akcji zmiany formularza rejestracji
 */
export interface RegisterFormActionType {
    type: typeof REGISTER_FORM_CHANGE_VALUE | typeof REGISTER_FORM_CLEAR;
    fields?: FormControl[];
}

/**
 * Typ opisujący operację zmiany w formularzu
 */
export const REGISTER_FORM_CHANGE_VALUE = 'REGISTER_FORM_CHANGE_VALUE';

/**
 * Typ opisujący uruchomienie operacji czyszczenia formularza
 */
export const REGISTER_FORM_CLEAR = 'REGISTER_FORM_CLEAR';
