import { FormControl } from "../../models/formControl.model";

/**
 * Typ opisujący typ akcji zmiany formularza logowania
 */
export interface LoginFormActionType {
    type: typeof LOGIN_FORM_CHANGE_VALUE | typeof LOGIN_FORM_CLEAR | typeof LOGIN_SET_ERROR;
    fields?: FormControl[];
    error?: string;
}

/**
 * Typ opisujący operację zmiany w formularzu logowania
 */
export const LOGIN_FORM_CHANGE_VALUE = 'LOGIN_FORM_CHANGE_VALUE';

/**
 * Typ opisujący uruchomienie operacji czyszczenia formularza logowania
 */
export const LOGIN_FORM_CLEAR = 'LOGIN_FORM_CLEAR';

/**
 * Typ opisujący ustawienie błędu po nieudanej próbie logowania
 */
export const LOGIN_SET_ERROR = 'LOGIN_SET_ERROR';
