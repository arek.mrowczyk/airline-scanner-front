import { Flight } from "../../models/api/flight.model";


/**
 * Typ opisujący typ akcji zmiany w tablicy lotów
 */
export interface FlightsActionType {
    type: typeof GET_FLIGHTS;
    payload?: Flight | Flight[];
}

/**
 * Typ opisujący operację pobrania listy lotów
 */
export const GET_FLIGHTS = 'GET_AIRPORT_COGET_FLIGHTSNECTIONS';