
export interface DashboardFilterChangeActionType {
    type: typeof DASHBOARD_FILTER_CHANGE_FIELD;
    fieldName: string;
    fieldValue: any;
}

export const DASHBOARD_FILTER_CHANGE_FIELD = 'DASHBOARD_FILTER_CHANGE_FIELD';