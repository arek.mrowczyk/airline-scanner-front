import { createStore, combineReducers } from 'redux';
import { testReducer } from './reducers/test.reducer';
import { AuthData } from '../models/authData.model';
import { authDataReducer } from './reducers/authData.reducer';
import { RegisterForm } from '../models/registerForm.model';
import { registerFormReducer } from './reducers/registerForm.reducer';
import { LoginForm } from '../models/loginForm.model';
import { loginFormReducer } from './reducers/loginForm.reducer';
import { Country } from "../models/api/country.model";
import { countriesReducer } from './reducers/countries.reducer';
import { DashboardFilterState } from '../models/dashboard.filter.model';
import { dashboardReducer } from './reducers/dashboard.reducer';
import { DashboardTableState } from '../models/dashboardTable.model';
import { dashboardTableReducer } from './reducers/dashboardTable.reducer';
import { Airport } from "../models/api/airport.model";
import { airportsReducer } from './reducers/airports.reducer';
import { AirportConnection } from "../models/api/airportConnection.model";
import { airportConnectionsReducer } from './reducers/airportConnections.reducer';
import { BookState } from '../models/book.model';
import { bookReducer } from './reducers/book.reducer';
import { Flight } from "../models/api/flight.model";
import { flightsReducer } from './reducers/flight.reducer';

/**
 * Interfejs globalnego stanu aplikacji
 */
export interface IAppState {
    testMessage: string;
    authData: AuthData;
    registerForm: RegisterForm;
    loginForm: LoginForm;
    countries: Country[];
    dashboardFilter: DashboardFilterState;
    dashboardTable: DashboardTableState;
    airports: Airport[];
    airportConnections: AirportConnection[];
    bookState: BookState;
    flights: Flight[];
}

const reducers = combineReducers<IAppState>({
    testMessage: testReducer,
    authData: authDataReducer,
    registerForm: registerFormReducer,
    loginForm: loginFormReducer,
    countries: countriesReducer,
    dashboardFilter: dashboardReducer,
    dashboardTable: dashboardTableReducer,
    airports: airportsReducer,
    airportConnections: airportConnectionsReducer,
    bookState: bookReducer,
    flights: flightsReducer,
});

/**
 * Globalny stan aplikacji
 */
export const Store = createStore(reducers);