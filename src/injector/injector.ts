import { providers, Provider } from './providers';

const createInstance = (constructor: any, useProviders?: Provider[]): any => {

  if (typeof constructor === 'string') {
    return constructor;
  }

  if (typeof constructor === 'number') {
    return constructor;
  }

  return inject(constructor, useProviders);
};

const constructProvider = (provider: any, deps?: any[]): any => {
  if (deps) {
    return new provider(...deps);
  } else {
    return new provider();
  }
};

const buildObject = (provider: Provider, deps?: any[]): any => {
  if (provider.useClass) {
    return constructProvider(provider.useClass, deps);
  } else {
    return constructProvider(provider.provider, deps);
  }
};

export type Constructor<T> = new (...args: any[]) => T;

/**
 * Funkcja wstrzykująca typ przekazany do funkcji. Przy czym każdy obiekt, który ma być wstrzyknięty musi mieć swoją definicję w providers.ts
 * bo bez tego nie zadziała.
 * @example
 * const apiService = inject(ApiService);
 * @param obj Typ, który funkcja ma wstrzyknąć
 */
export function inject<T>(obj: Constructor<T> | any, useProviders?: Provider[]): T {

  let provider = null;
  if (useProviders) {
    provider = useProviders?.find(x => x.provider === obj);
  } else {
    provider = providers?.find(x => x.provider === obj);
  }

  if (provider) {
    if (provider?.useDeps) {
      const instances = provider.useDeps.map(x => createInstance(x, useProviders));

      return buildObject(provider, instances);
    }

    return buildObject(provider);
  }

  throw new Error(`Brak providera dla typu <${obj}>`);

}
