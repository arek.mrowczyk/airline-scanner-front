import { ApiService, ApiProdService } from "../services/api/api.service";
import { LoggerService, LoggerProdService } from "../services/logger/logger.service";
import { ValidationBuilder } from "../validators/builder/validation.builder";
import { ApiDevService } from "../services/api/api.dev.service";
import { LoginService } from "../services/login/login.service";
import { RegisterService } from "../services/register/register.service";
import { LoggerDevService } from "../services/logger/logger.dev.service";
import { CountriesService } from "../services/countries/countries.service";
import { AirportService } from "../services/airport/airport.service";
import { AirportConnectionService } from "../services/airportConnection/airportConnection.service";
import { DashboardService } from "../services/dashboard/dashboard.service";
import { FlightService } from "../services/flight/flight.service";
import { BookingService } from "../services/booking/booking.service";
import { UserDataService } from "../services/userData/userData.service";

/**
 * Typ który w polu provider trzyma referencję do typu, 
 * natomiast pole useDeps zawiera referencje typów, które są wymagane w konstruktorze typu provider
 */
export interface Provider {

    /**
     * Klasa lub obiekt, który ma być wstrzyknięty do kontruktora, pod warunkiem że pole useClass jest puste
     */
    provider: any;

    /**
     * Zależności jakie posiada provider
     */
    useDeps?: any[];

    /**
     * Jeżeli provider jest klasą abstrakcyjną lub obiektem zależnym wtedy ta instancja jest wstrzykiwana
     */
    useClass?: any;
}

/**
 * Providery dla wersji development
 */
const developmentProviders: Provider[] = [
    { provider: LoggerService, useClass: LoggerDevService },
    { provider: ApiService, useClass: ApiDevService, useDeps: [LoggerService] },
    { provider: ValidationBuilder },
    { provider: RegisterService, useDeps: [ApiService, LoggerService] },
    { provider: LoginService, useDeps: [ApiService] },
    { provider: CountriesService, useDeps: [ApiService] },
    { provider: AirportService, useDeps: [ApiService] },
    { provider: AirportConnectionService, useDeps: [ApiService] },
    { provider: DashboardService, useDeps: [ApiService] },
    { provider: FlightService, useDeps: [ApiService] },
    { provider: BookingService, useDeps: [ApiService] },
    { provider: UserDataService, useDeps: [ApiService] }
];

/**
 * Providery dla wersji produkcyjno-developerskiej
 */
const proddevProviders: Provider[] = [
    { provider: LoggerService, useClass: LoggerDevService },
    { provider: ApiService, useClass: ApiProdService, useDeps: [LoggerService] },
    { provider: ValidationBuilder },
    { provider: RegisterService, useDeps: [ApiService, LoggerService] },
    { provider: LoginService, useDeps: [ApiService] },
    { provider: CountriesService, useDeps: [ApiService] },
    { provider: AirportService, useDeps: [ApiService] },
    { provider: AirportConnectionService, useDeps: [ApiService] },
    { provider: DashboardService, useDeps: [ApiService] },
    { provider: FlightService, useDeps: [ApiService] },
    { provider: BookingService, useDeps: [ApiService] },
    { provider: UserDataService, useDeps: [ApiService] }
];

/**
 * Providery dla wersji produkcyjnej
 */
const productionProviders: Provider[] = [
    { provider: LoggerService, useClass: LoggerProdService },
    { provider: ApiService, useClass: ApiProdService, useDeps: [LoggerService] },
    { provider: ValidationBuilder },
    { provider: RegisterService, useDeps: [ApiService, LoggerService] },
    { provider: LoginService, useDeps: [ApiService] },
    { provider: CountriesService, useDeps: [ApiService] },
    { provider: AirportService, useDeps: [ApiService] },
    { provider: AirportConnectionService, useDeps: [ApiService] },
    { provider: DashboardService, useDeps: [ApiService] },
    { provider: FlightService, useDeps: [ApiService] },
    { provider: BookingService, useDeps: [ApiService] },
    { provider: UserDataService, useDeps: [ApiService] }
];

/**
 * Lista providerów, które mogą zostać wstrzyknięte wraz z ich zależnościami
 */
export const providers = process.env.REACT_APP_ENV === 'development' ? developmentProviders : process.env.REACT_APP_ENV === 'proddev'? proddevProviders : productionProviders;