import { ApiService } from "../api/api.service";

import { Country } from "../../models/api/country.model";


export class CountriesService {

    constructor(private api: ApiService) { }

    public async getCountries() {
        const countries = (await (this.api.get<Country[]>('countries').toPromise())) as Country[];
        return countries;
    }

    public async addCountry(data: any) {
        try {
            console.log({data});
            await (this.api.post<Country>('countries', data).toPromise());
        }
        catch (e) {
            console.log("Error - Countries Service", e);
        }
    }

    public async editCountry(newData: Country) {
        try {
            await (this.api.post<Country>('countries', newData).toPromise());
        }
        catch (e) {
            console.log("Error - Countries Service", e);
        }
    }

    public async deleteCountry(data: Country) {
        try {
            await (this.api.post<Country>(`countries/delete/${data.id}`,{}).toPromise());
        }
        catch (e) {
            console.log("Error - Countries Service", e);
        }
    }
}