import { ApiService } from "../api/api.service";

import { Airport } from "../../models/api/airport.model";


export class AirportsService {

    constructor(private api: ApiService) { }

    public async getAirports() {
        const airports = (await (this.api.get<Airport[]>('airports').toPromise())) as Airport[];
        return airports;
    }

    public async addAirport(data: any) {
        try {
            console.log({data});
            await (this.api.post<Airport>('airports', data).toPromise());
        }
        catch (e) {
            console.log("Error - Airports Service", e);
        }
    }

    public async editAirport(newData: Airport) {
        try {
            await (this.api.post<Airport>('airports', newData).toPromise());
        }
        catch (e) {
            console.log("Error - Airports Service", e);
        }
    }

    public async deleteAirport(data: Airport) {
        try {
            await (this.api.post<Airport>(`airports/delete/${data.id}`,{}).toPromise());
        }
        catch (e) {
            console.log("Error - Airports Service", e);
        }
    }
}