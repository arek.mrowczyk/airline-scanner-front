import { ApiService } from "../api/api.service";
import { AirportConnection } from "../../models/api/airportConnection.model";

export class AirportConnectionService {

    constructor(private api: ApiService) { }

    public async getAllConnections(): Promise<AirportConnection[]> {
        return (await this.api.get<AirportConnection[]>('airport_connections').toPromise()) as AirportConnection[];
    }

    public async addConnection(data: any) {
        return (await (this.api.post<AirportConnection>('airport_connections/bidirectional', data).toPromise()));
    }

    public async editConnection(newData: AirportConnection) {
        // It doesnt really works xd
        return (await (this.api.post<AirportConnection>('airport_connections/bidirectional', newData).toPromise()));
    }

    public async deleteConnection(data: AirportConnection) {
        return (await (this.api.post<AirportConnection>(`airport_connections/delete/${data.id}`,{}).toPromise()));
    }
}