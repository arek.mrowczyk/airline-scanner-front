import { ApiService } from "../api/api.service";
import { User } from "../../models/api/user.model";

export class UserDataService {

    constructor(private api: ApiService) { }

    public async getUserByLogin(login: string): Promise<User> {
        return (await this.api.get<User>('accounts/login/' + login).toPromise()) as User;
    }

    public async addToBalance(data: any) {
        return (await (this.api.post<User>('accounts/balance', data).toPromise()));
    }




}