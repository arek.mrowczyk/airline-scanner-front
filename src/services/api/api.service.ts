import { Observable, of } from "rxjs";
import { ajax, AjaxRequest, AjaxError } from 'rxjs/ajax';
import { map, catchError, tap } from 'rxjs/operators';
import Configuration from "../../config/enviroment";
import { LoggerService } from '../logger/logger.service';

/**
 * Klasa abstrakcyjna api
 */
export abstract class ApiService {

    public abstract get<T>(endpoint: string, headers?: { [key: string]: string }): Observable<T | AjaxError>;
    public abstract post<T>(endpoint: string, body: any, headers?: { [key: string]: string }): Observable<T | AjaxError>
}

/**
 * Serwis odpowiadający za łączenie się z backend'em
 */
export class ApiProdService extends ApiService {

    private baseUrl = Configuration.api;

    constructor(private loggerService: LoggerService) {
        super();
    }

    public get<T>(endpoint: string, headers?: { [key: string]: string }): Observable<T | AjaxError> {

        const req: AjaxRequest = {
            url: `${this.baseUrl}/${endpoint}`,
            headers: headers,
            method: 'GET'
        };

        this.loggerService.info(`Wywoływanie endpoint'a ${endpoint} metodą GET`);
        this.loggerService.table('Nagłówki', headers);

        return this.request<T>(req);

    }

    public post<T>(endpoint: string, body: any, headers?: { [key: string]: string }): Observable<T | AjaxError> {
        const req: AjaxRequest = {
            url: `${this.baseUrl}/${endpoint}`,
            headers: {
                headers,
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: body,
        };

        this.loggerService.info(`Wywoływanie endpoint'a ${endpoint} metodą POST`);
        this.loggerService.table('Nagłówki', headers);

        return this.request<T>(req);
    }

    private request<T>(req: AjaxRequest): Observable<T | AjaxError> {
        return ajax(req).pipe(
            map(x => x.response),
            catchError((error: AjaxError) => {
                this.loggerService.error(error);
                return of(error);
            }),
            tap(x => this.loggerService.info(`Odebrano obiekt: ${x}`)));
    }
}