import { ApiService } from "./api.service";
import { LoggerService } from "../logger/logger.service";
import { getMock, postMock } from "../../mocks/mock";
import { of, Observable } from "rxjs";
import { AjaxError } from "rxjs/ajax";

/**
 * Zamockowany serwis api
 */
export class ApiDevService extends ApiService {

    constructor(private loggerService: LoggerService) {
        super();
    }

    public get<T>(endpoint: string, headers?: { [key: string]: string; } | undefined): Observable<T | AjaxError> {
        this.loggerService.info(`Wywoływanie endpoint'a ${endpoint} metodą GET`);
        this.loggerService.table('Nagłówki', headers);

        return of(getMock<T>(endpoint));
    }    
    
    public post<T>(endpoint: string, body: any, headers?: { [key: string]: string; } | undefined): Observable<T | AjaxError> {
        this.loggerService.info(`Wywoływanie endpoint'a ${endpoint} metodą POST`);
        this.loggerService.table('Nagłówki', headers);

        return of(postMock(endpoint, body));
    }

}