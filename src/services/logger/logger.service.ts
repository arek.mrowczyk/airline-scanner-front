
/**
 * Serwis odpowiadający za logowanie informacji i błędów
 */
export abstract class LoggerService {

    /**
     * Metoda wypisująca informację w konsoli w normalnym trybie
     * @param message Wiadomość
     */
    public abstract info(message: any): void;

    /**
     * Metoda wypisująca informację w konsoli w trybie błędu
     * @param message Wiadomość
     */
    public abstract error(message: any): void;

    /**
     * Metoda wypisująca tabelę w konsoli
     * @param data Dane do wypisania
     */
    public abstract table(message: string, data: any): void;
}

export class LoggerProdService extends LoggerService {

    public info(message: any): void {
    }

    public error(message: any): void {
    }

    public table(message: string, data: any): void {

    }

}