import { LoggerService } from "./logger.service";

export class LoggerDevService extends LoggerService {

    public info(message: any): void {
        console.log(`[INFO] ${message}`);
    }

    public error(message: any): void {
        console.error(`[ERROR] ${message}`);
    }

    public table(message: string, data: any): void {
        console.log(`[TABLE] ${message}`);
        console.table(data);
    }
}