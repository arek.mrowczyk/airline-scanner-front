import { ApiService } from "../api/api.service";
import { Airport } from "../../models/api/airport.model";

export class AirportService {

    constructor(private api: ApiService) { }

    public async getAirportsForCountry(id: string): Promise<Airport[]> {
        return (await this.api.get<Airport[]>('airports/country/' + id).toPromise()) as Airport[];
    }

    public async getAirports() {
        return (await (this.api.get<Airport[]>('airports').toPromise())) as Airport[];
    }

    public async addAirport(data: any) {
        return (await (this.api.post<Airport>('airports', data).toPromise()));
    }

    public async editAirport(newData: Airport) {
        return (await (this.api.post<Airport>('airports', newData).toPromise()));
    }

    public async deleteAirport(data: Airport) {
        return (await (this.api.post<Airport>(`airports/delete/${data.id}`,{}).toPromise()));
    }
}