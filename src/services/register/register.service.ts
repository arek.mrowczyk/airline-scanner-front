import { FormControl } from "../../models/formControl.model";
import { FormValue } from "../../models/formValue.model";
import { usernameValidator } from "../../validators/register/username.validator";
import { registerPasswordValidator } from "../../validators/register/registerPassword.validator";
import { ValidationBuilder } from "../../validators/builder/validation.builder";
import { inject } from "../../injector/injector";
import { ApiService } from "../api/api.service";
import { RegisterApiForm } from "../../models/api/registerApiForm.model";
import { acceptedValidator } from "../../validators/register/accepted.validator";
import { requiredValidatorBuilder } from "../../validators/register/required.validator";
import { dateConverter } from "../../utils/dateConverter";
import { LoggerService } from "../logger/logger.service";

/**
 * Serwis odpowiadający za logikę rejestracji
 */
export class RegisterService {

    constructor(private api: ApiService, private logger: LoggerService) { }

    /**
     * Metoda zmieniająca wartości formularza i walidująca formularz
     * @param fieldName Nazwa pola do zmiany
     * @param value Wartość tego pola
     * @param fields Pola formularza
     * @returns Zwraca wartości formularza
     */
    public async changeFormAndValidate(fieldName: string, value: FormValue, fields: FormControl[]): Promise<FormControl[]> {

        this.logger.info(`Zmiana wartości formularza dla pola ${fieldName} na ${value}`);

        this.changeFormValues(fieldName, value, fields);

        this.logger.table('Walidacja formularza', fields);

        await this.validateFieldsWithoutUsername(fields);

        this.logger.table('Wynik walidacji formularza', fields);

        return fields;
    }

    /**
     * Metoda wysyłająca formularz rejestracyjny
     * @param fields Pola formularza
     * @returns Zwraca czy udało się zarejestrować użytkownika
     */
    public async sendForm(fields: FormControl[]): Promise<boolean> {

        this.logger.table('Wysyłanie formularza', fields);

        const res = await this.validateFieldsFinal(fields);

        this.logger.info(`Wynik walidacji formularza: ${res}`);

        if (res) {
            const form: RegisterApiForm = {
                login: this.getFormValue(fields, 'username'),
                password: this.getFormValue(fields, 'password'),
                dateOfBirth: dateConverter(this.getFormValue(fields, 'dateOfBirth')),
                firstName: this.getFormValue(fields, 'firstName'),
                lastName: this.getFormValue(fields, 'lastName')
            };

            this.logger.table('Przygotowano formularz do wysłania', form);

            const result = await (this.api.post('accounts', form).toPromise());

            if(result === null || result === true) {
                this.logger.info('Pomyślnie zarejestrowano');
                return true;
            }

            return false;

        } else {
            return false;
        }
    }

    /**
     * Pobiera określoną wartość z formularza
     * @param fields Formularz
     * @param key Nazwa pola, które ma być zwrócone
     * @returns Zwraca wartość pola
     */
    private getFormValue(fields: FormControl[], key: string): string {
        return fields.filter(x => x.fieldName === key)[0].fieldValue as string;
    }

    /**
     * Metoda tworząca walidator dla formularza rejestracji
     * @param isUserName Dla wartości true będzie sprawdzał dokładnie każde pole w formularzu, 
     *                   dla wartości false będzie sprawdzał pobieżnie walidator required
     * @returns Zwraca walidator
     */
    private createValidator(isUserName: boolean): ValidationBuilder<FormControl[], { fieldName: string; }> {

        this.logger.info('Tworzenie walidatora');

        const validationBuilder = inject(ValidationBuilder) as ValidationBuilder<FormControl[], { fieldName: string; }>;

        validationBuilder.useValidators(registerPasswordValidator);

        if (isUserName) {
            validationBuilder.useAsyncValidators(usernameValidator);
            validationBuilder.useValidators(acceptedValidator, requiredValidatorBuilder(false));
        }else {
            validationBuilder.useValidators(requiredValidatorBuilder(true));
        }

        this.logger.info('Prawidłowo stworzono walidator');

        return validationBuilder;
    }

    /**
     * Zmienia wartości w formularzu
     * @param fieldName Nazwa pola, które będzie zmienione
     * @param value Wartość, na którą ma być pole zmienione
     * @param fields Pola formularza
     */
    private changeFormValues(fieldName: string, value: FormValue, fields: FormControl[]) {
        const toChange = fields.filter(x => x.fieldName === fieldName)[0];

        toChange.fieldValue = value;
        toChange.touched = true;

        if(toChange.fieldName !== 'accepted' && toChange.fieldValue === '') {
            toChange.touched = false;
        }
    }

    /**
     * Wykonuje pełną walidację przed wysłaniem do backend'u
     * @param fields Pola formularza
     * @returns Zwraca wynik walidacji
     */
    private async validateFieldsFinal(fields: FormControl[]): Promise<boolean> {

        const validator = this.createValidator(true);

        const result = await validator.validate(fields);

        for (let i = 0; i < fields.length; i++) {
            const field = result.find(x => x.info.fieldName === fields[i].fieldName);

            if (field) {
                fields[i].error.message = field.message;
            } else {
                fields[i].error.message = '';
            }
        }

        return fields.findIndex(x => x.error.message !== '') <= -1;
    };

    /**
     * Niedokładnie waliduje pola formularza
     * @param fields Pola formularza
     */
    private async validateFieldsWithoutUsername(fields: FormControl[]) {

        const validator = this.createValidator(false);

        const result = await validator.validate(fields);

        for (let i = 0; i < fields.length; i++) {
            const field = result.find(x => x.info.fieldName === fields[i].fieldName);

            if (field) {
                fields[i].error.message = field.message;
            } else {
                fields[i].error.message = '';
            }
        }
    };
}