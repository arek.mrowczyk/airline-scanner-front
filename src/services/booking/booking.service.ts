import { ApiService } from "../api/api.service";
import { Flight } from "../../models/api/flight.model";
import { BookingRequest } from "../../models/api/bookingRequest.model";
import { Person } from "../../models/api/person.model";
import { Booking } from "../../models/api/booking.model";

export class BookingService {

    constructor(private api: ApiService) { }

    public async sendBookingRequest(flight: Flight, login: string, peoples: Person[]): Promise<boolean> {
        const req: BookingRequest = {
            flightId: flight.id,
            login: login,
            people: peoples
        };

        const res = (await this.api.post<any[]>('booking', req).toPromise()) as any[];

        if (res && res.length > 0) {
            const seatRes = await this.api.post('seat', flight.seat).toPromise();
            return true;
        }

        return false;
    }

    public async getAllBooking(login: string): Promise<Booking[]> {
        return (await this.api.get<Booking[]>('booking/login/' + login).toPromise()) as Booking[];
    }
    
}