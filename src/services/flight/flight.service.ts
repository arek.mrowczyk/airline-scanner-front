import { ApiService } from "../api/api.service";
import { Flight } from "../../models/api/flight.model";

export class FlightService {

    constructor(private api: ApiService) { }

    public async getFlightsForCodes(origin: string, destination: string): Promise<Flight[]> {
        return (await this.api.get<Flight[]>(`flights/origin/${origin}/destination/${destination}`).toPromise()) as Flight[];
    }

    public async getAllFlights(): Promise<Flight[]> {
        return (await this.api.get<Flight[]>(`flights`).toPromise()) as Flight[];
    }

    public async addFlight(data: any) {
        return (await (this.api.post<Flight>('flights', data).toPromise()));
    }

    public async editFlight(newData: Flight) {
        return (await (this.api.post<Flight>('flights', newData).toPromise()));
    }

    public async deleteFlight(data: Flight) {
        return (await (this.api.post<Flight>(`flights/delete/${data.id}`,{}).toPromise()));
    }
}