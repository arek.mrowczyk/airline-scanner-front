import { FormControl } from "../../models/formControl.model";
import { FormValue } from "../../models/formValue.model";
import { ValidationBuilder } from "../../validators/builder/validation.builder";
import { inject } from "../../injector/injector";
import { ApiService } from "../api/api.service";
import { LoginApiForm } from "../../models/api/loginApiForm.model";
import { requiredValidatorBuilder } from "../../validators/register/required.validator";

import { Store } from '../../store/store';
import { loginAction } from '../../store/actions/authData.action';
import { User } from "../../models/api/user.model";

export class LoginService {

    constructor(private api: ApiService) { }

    public async changeFormAndValidate(fieldName: string, value: FormValue, fields: FormControl[]): Promise<FormControl[]> {

        this.changeFormValues(fieldName, value, fields);

        await this.validateFields(fields);

        return fields;
    }

    public async sendForm(fields: FormControl[], history: any): Promise<boolean> {

        const res = await this.validateFieldsFinal(fields);

        if (res) {
            const form: LoginApiForm = {
                login: this.getFormValue(fields, 'username'),
                password: this.getFormValue(fields, 'password')
            };

            const token = (await (this.api.post('authentication/login', form).toPromise())) as string;

            if(token === null){
                
                const userData = (await (this.api.get<User>(`accounts/login/${form.login}`).toPromise())) as User;
                this.updateAuthState(userData);
                history.push('/home');
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    private updateAuthState(userData: User): void {
        Store.dispatch(loginAction(userData));
    }

    private getFormValue(fields: FormControl[], key: string): string {
        return fields.filter(x => x.fieldName === key)[0].fieldValue as string;
    }

    private createValidator(isFinal: boolean): ValidationBuilder<FormControl[], { fieldName: string; }> {
        const validationBuilder = inject(ValidationBuilder) as ValidationBuilder<FormControl[], { fieldName: string; }>;
        if(isFinal){
            validationBuilder.useValidators(requiredValidatorBuilder(false));
        }
        else{
            validationBuilder.useValidators(requiredValidatorBuilder(true));
        }
        return validationBuilder;
    }

    private changeFormValues(fieldName: string, value: FormValue, fields: FormControl[]) {
        const toChange = fields.filter(x => x.fieldName === fieldName)[0];
        toChange.fieldValue = value;
        toChange.touched = true;

        if(toChange.fieldName !== 'accepted' && toChange.fieldValue === '') {
            toChange.touched = false;
        }
    }

    private async validateFieldsFinal(fields: FormControl[]): Promise<boolean> {

        const validator = this.createValidator(true);

        const result = await validator.validate(fields);

        for (let i = 0; i < fields.length; i++) {
            const field = result.find(x => x.info.fieldName === fields[i].fieldName);

            if (field) {
                fields[i].error.message = field.message;
            } else {
                fields[i].error.message = '';
            }
        }

        return fields.findIndex(x => x.error.message !== '') <= -1;
    };

    private async validateFields(fields: FormControl[]) {

        const validator = this.createValidator(false);

        const result = await validator.validate(fields);

        for (let i = 0; i < fields.length; i++) {
            const field = result.find(x => x.info.fieldName === fields[i].fieldName);

            if (field) {
                fields[i].error.message = field.message;
            } else {
                fields[i].error.message = '';
            }
        }
    };
}