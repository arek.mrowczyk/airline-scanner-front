import React from 'react';
import { ApiService } from "../api/api.service";
import { TableData } from "../../models/tableData.model";
import { IconButton } from '@material-ui/core';
import BookIcon from '@material-ui/icons/Book';
import { Flight } from '../../models/api/flight.model';
import { dateConverterFromNumberArray } from '../../utils/dateConverter';

export class DashboardService {

    constructor(private api: ApiService) { }

    public getTableData(data: Flight[], component: JSX.Element): TableData {
        const res: TableData = {
            columns: [{
                name: 'Wylot z'
            },
            {
                name: 'Dnia'
            },
            {
                name: 'Do'
            },
            {
                name: 'Koszt'
            },
            {
                name: 'Ilość miejsc'
            }],
            rows: []
        }

        for (let i = 0; i < data.length; i++) {
            res.rows.push({
                data: {
                    airportFrom: data[i].connection.originAirport.airportName,
                    dateFrom: dateConverterFromNumberArray(data[i].date),
                    airportTo: data[i].connection.destinationAirport.airportName,
                    cost: data[i].fare,
                    availableSeats: data[i].availableSeats
                },
                actions: {
                    components: [
                        React.cloneElement(
                        component,
                        { flight: data[i] }
                    )]
                }
            })
        }

        return res;
    }
}