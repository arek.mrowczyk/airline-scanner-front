import { FormControl } from "../../models/formControl.model";
import { Validator } from "../builder/validator";

/**
 * Walidator sprawdzający poprawność haseł z formularza rejestracji
 * @param fields Pola znajdujące się w formularzu
 * @returns Zwraca rezultat walidacji
 */
export const registerPasswordValidator: Validator<FormControl[], { fieldName: string }> = fields => {

    const passwordField = fields.find(x => x.fieldName === 'password') as FormControl;
    const confirmPasswordField = fields.find(x => x.fieldName === 'confirmPassword') as FormControl;

    if (passwordField && confirmPasswordField && passwordField.fieldValue !== confirmPasswordField.fieldValue) {
        return [{
            message: 'Hasła różnią się od siebie',
            info: {
                fieldName: passwordField.fieldName
            }
        }];
    }
    return [{
        message: '',
        info: {
            fieldName: passwordField.fieldName
        }
    }];
}