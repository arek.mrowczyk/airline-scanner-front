import { Validator } from "../builder/validator";
import { FormControl } from "../../models/formControl.model";

/**
 * Walidator sprawdzający czy checkbox jest zaznaczony
 * @param fields Pola znajdujące się w formularzu
 */
export const acceptedValidator: Validator<FormControl[], { fieldName: string }> = fields => {

    const acceptedField = fields.find(x => x.fieldName === 'accepted') as FormControl;

    if (acceptedField && (acceptedField.fieldValue === false)) {
        return [{
            message: 'Musisz zaakceptować warunki',
            info: {
                fieldName: acceptedField.fieldName
            }
        }];
    }
    return [{
        message: '',
        info: {
            fieldName: acceptedField.fieldName
        }
    }];

};