import { AsyncValidator } from "../builder/validator";
import { FormControl } from "../../models/formControl.model";
import { inject } from "../../injector/injector";
import { ApiService } from "../../services/api/api.service";

/**
 * Walidator sprawdzający czy nazwa użytkownika nie powtarza się
 */
export const usernameValidator: AsyncValidator<FormControl[], { fieldName: string }> = async fields => {

    const username = fields.filter(x => x.fieldName === 'username')[0];

    const api = inject(ApiService) as ApiService;

    const account = await (api.get<any>(`accounts/login/${username.fieldValue}`).toPromise());

    if(account) {

        return [{
            message: 'Nazwa użytkownika jest używana',
            info: {
                fieldName: 'username'
            }
        }];
    }

    return [{
        message: '',
        info: {
            fieldName: 'username'
        }
    }];
}