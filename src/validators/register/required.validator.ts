import { FormControl } from "../../models/formControl.model";
import { ValidationError } from "../../models/validationError.model";

/**
 * Budowniczy walidatora wymaganego pola, zwraca funkcję walidującą
 * @param onlyWithErrors parametr, który mówi czy za każdym razem pole z formularza ma być sprawdzane czy nie
 */
export const requiredValidatorBuilder = (onlyWithErrors: boolean) => {
    return (fields: FormControl[]): ValidationError<{ fieldName: string }>[] => {

        const res: ValidationError<{ fieldName: string; }>[] = [];

        for (let i = 0; i < fields.length; i++) {

            const field = fields[i];

            if (!onlyWithErrors) {
                if (field.fieldName !== 'accepted' && !field.touched && !field.fieldValue) {
                    res.push({
                        message: 'To pole jest wymagane',
                        info: {
                            fieldName: field.fieldName
                        }
                    });
                }
            } else {
                if(field.fieldName !== 'accepted' && field.error.message !== '' && !field.fieldValue) {
                    res.push({
                        message: 'To pole jest wymagane',
                        info: {
                            fieldName: field.fieldName
                        }
                    });
                }
            }


        }

        return res;
    }
}