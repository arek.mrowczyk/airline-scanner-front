import { ValidationError } from "../../models/validationError.model";
import { Validator, AsyncValidator } from "./validator";

/**
 * Budowniczy walidacji należy z niego korzystać gdy trzeba coś walidować
 * @param TObject Typ mówiący jakiego typu obiekty walidator ma sprawdzać
 * @param TResult Typ jaki ma być zwracany w polu info klasy ValidationError
 */
export class ValidationBuilder<TObject = {}, TResult = {}> {

    private validators: Validator<TObject, TResult>[] = [];

    private asyncValidators: AsyncValidator<TObject, TResult>[] = [];

    private errors: ValidationError<TResult>[] = [];

    public useValidators(...validators: Validator<TObject, TResult>[]) {
        this.validators.push(...validators);
    }

    public useAsyncValidators(...validators: AsyncValidator<TObject, TResult>[]) {
        this.asyncValidators.push(...validators);
    }

    public clearErrors() {
        this.errors = [];
    }

    public async validate(obj: TObject): Promise<ValidationError<TResult>[]> {
        for (let i = 0; i < this.validators.length; i++) {
            const res = this.validators[i](obj);

            for (let i = 0; i < res.length; i++) {
                if (res[i].message) {
                    this.errors.push(res[i]);
                }
            }
        }

        for (let i = 0; i < this.asyncValidators.length; i++) {
            const res = await this.asyncValidators[i](obj);

            for (let i = 0; i < res.length; i++) {
                if (res[i].message) {
                    this.errors.push(res[i]);
                }
            }
        }

        return this.errors;
    }
}