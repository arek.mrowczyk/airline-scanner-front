import { ValidationError } from "../../models/validationError.model";

/**
 * Typ walidatora służący do uproszczenia notacji.
 * 
 * @param TObject Typ mówiący jakiego typu obiekty walidator ma sprawdzać
 * @param TResult Typ jaki ma być zwracany w polu info klasy ValidationError
 */
export type Validator<TObject, TResult = {}> = ((obj: TObject) => ValidationError<TResult>[]);

export type AsyncValidator<TObject, TResult = {}> = ((obj: TObject) => Promise<ValidationError<TResult>[]>);