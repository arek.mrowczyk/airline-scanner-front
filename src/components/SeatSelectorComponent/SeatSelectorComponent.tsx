import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { IAppState } from '../../store/store';
import { BookState } from '../../models/book.model';
import { Button } from '@material-ui/core';
import { bookSetStepAction, bookSetFlightAction, bookSetSeatsAction } from '../../store/actions/book.action';

export const SeatSelectorComponent: React.FC = () => {

    const flight = useSelector<IAppState, BookState>(x => x.bookState).flight;
    const persons = useSelector<IAppState, BookState>(x => x.bookState).persons;
    const seat = useSelector<IAppState, BookState>(x => x.bookState).seats;
    const dispatch = useDispatch();
    const [selectedSeats, setSelectedSeats] = useState<any[]>(seat);
    const [avaiableSeats, setAvaiableSeats] = useState(persons.length - seat.length);

    const seats = JSON.parse(flight.seat.seats).seats as any[];

    console.log(seats);

    const parsed = [];

    for (let i = 0; i < seats.length; i += 4) {
        parsed.push({
            seat: [
                seats[i],
                seats[i + 1],
                seats[i + 2],
                seats[i + 3]
            ]
        });
    }

    console.log(parsed);

    const pushSeat = (seat: any) => {
        const res = [...selectedSeats];

        if (res.filter(x => x.position === seat.position).length === 0) {
            if (avaiableSeats > 0) {
                res.push(seat);
                setAvaiableSeats(avaiableSeats - 1);
            }
        } else {
            res.splice(res.findIndex(x => x.position === seat.position), 1);
            setAvaiableSeats(avaiableSeats + 1);
        }
        setSelectedSeats(res);
    };

    const PilotSeat = () => (
        <div style={{
            backgroundColor: 'aqua',
            border: '1px blue solid',
            padding: '1rem',
            writingMode: 'vertical-lr',
            textAlign: 'center',
            margin: '4px'
        }}>Przód</div>
    )

    const seatColor = (seat: any) => {
        if (selectedSeats.filter(x => x.position === seat.position).length === 1) {
            return 'blue';
        } else if (seat.taken) {
            return 'red';
        } else if (!seat.taken) {
            return 'aqua';
        }
    };

    const Seat = (seat: any) => (
        <div style={{
            backgroundColor: seatColor(seat.seat),
            border: '1px blue solid',
            padding: '0.5rem',
            margin: '4px',
            cursor: 'pointer'
        }} onClick={() => pushSeat(seat.seat)}>{seat.seat.position}</div>
    )

    const BackSeat = () => (
        <div style={{
            backgroundColor: 'aqua',
            border: '1px blue solid',
            padding: '1rem',
            writingMode: 'sideways-lr',
            textAlign: 'center',
            margin: '4px'
        }}>Tył</div>
    )

    const nextStep = () => {
        const seats = JSON.parse(flight.seat.seats).seats as any[];

        for (let i = 0; i < selectedSeats.length; i++) {
            const index = seats.findIndex(x => x.position === selectedSeats[i].position);

            seats[index].taken = true;
        }

        flight.seat.seats = JSON.stringify({
            seats: seats
        });

        dispatch(bookSetFlightAction(flight));
        dispatch(bookSetSeatsAction(selectedSeats));
        dispatch(bookSetStepAction(2));
    }

    const prevStep = () => {
        dispatch(bookSetStepAction(0));
    }

    return (
        <>
            <div>
                Ilość miejsc: {avaiableSeats}
            </div>
            <div style={{ display: 'flex', justifyContent: 'center' }}>
                <PilotSeat />
                {parsed.map(x =>
                    <div>
                        <Seat seat={x.seat[0]} />
                        <Seat seat={x.seat[1]} />
                        <br />
                        <Seat seat={x.seat[2]} />
                        <Seat seat={x.seat[3]} />
                    </div>
                )}
                <BackSeat />
            </div>
            <div>
                <Button
                    variant="outlined"
                    color="primary"
                    size="small"
                    onClick={prevStep}
                >
                    Wstecz
                </Button>
                <Button
                    variant="outlined"
                    color="primary"
                    size="small"
                    onClick={nextStep}
                >
                    Dalej
                </Button>
            </div>
        </>
    )
}