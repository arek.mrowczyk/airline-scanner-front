
/**
 * Typ komponentu głównego widoku
 */
export interface MainComponentTemplateType {
    testMessage?: string;
    sendTestMessage?: (msg: string) => void;
}