
import React from 'react';
import MainComponentView from './MainComponentView';

/**
 * Komponent głównego widoku
 */
export const MainComponentTemplate: React.FC<any> = (props: any) => {

    return (
        <>
            <MainComponentView />
        </>
    );
}

const MainComponent = MainComponentTemplate;

export default MainComponent;