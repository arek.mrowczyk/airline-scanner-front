import React from 'react';
import { Grid } from '@material-ui/core';
import LoginComponent from '../LoginComponent/LoginComponent';
import RegisterComponent from '../RegisterComponent/RegisterComponent';

/**
 * Komponent widokowy widoku głównego
 */
export const MainComponentView: React.FC<any> = (props: any) => {

    const styles = {
        grid: {
            paddingRight: '1rem'
        }
    }

    return (
        <Grid container>
            <Grid item xs={2}>

            </Grid>
            <Grid item xs={4} style={styles.grid}>
                <RegisterComponent />
            </Grid>
            <Grid item xs={4} style={styles.grid}>
                <LoginComponent />
            </Grid>
            <Grid item xs={2}>

            </Grid>
        </Grid>
    );
}

export default MainComponentView;