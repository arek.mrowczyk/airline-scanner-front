import React from 'react';
import { RegisterComponentView } from './RegisterComponentView';
import { registerFormAction, clearRegisterFormAction } from '../../store/actions/registerForm.action';
import { IAppState } from '../../store/store';
import { useSelector, useDispatch } from 'react-redux';
import { FormValue } from '../../models/formValue.model';
import { inject } from '../../injector/injector';
import { FormControl } from '../../models/formControl.model';
import { RegisterService } from '../../services/register/register.service';
import { KeyValue } from '../../models/keyValue.model';

/**
 * Komponent rejestracji
 */
export const RegisterComponentTemplate: React.FC = () => {

    const errors = useSelector<IAppState, KeyValue<string, string>[]>(x => x.registerForm.fields.map(y => ({ key: y.fieldName, value: y.error.message })));
    const fields = useSelector<IAppState, FormControl[]>(x => x.registerForm.fields);

    const dispatch = useDispatch();

    const service = inject(RegisterService) as RegisterService;

    const sendForm = async (): Promise<boolean> => {

        const res = await service.sendForm(fields);

        if (res) {
            dispatch(clearRegisterFormAction());

            return true;

        } else {
            dispatch(registerFormAction(fields))

            return false;
        }
    };

    const clearForm = () => {
        dispatch(clearRegisterFormAction());
    };

    const changeFormValue = async (fieldName: string, value: FormValue) => {

        const fieldss = await service.changeFormAndValidate(fieldName, value, fields);

        dispatch(registerFormAction(fieldss));
    };

    return (
        <RegisterComponentView changeFormValue={changeFormValue}
            errors={errors}
            sendForm={sendForm}
            fields={fields as FormControl[]}
            clearForm={clearForm} />
    );
}

const RegisterComponent = RegisterComponentTemplate;

export default RegisterComponent;