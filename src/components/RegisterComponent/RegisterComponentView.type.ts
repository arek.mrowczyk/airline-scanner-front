import { FormValue } from "../../models/formValue.model";
import { KeyValue } from "../../models/keyValue.model";
import { FormControl } from "../../models/formControl.model";

/**
 * Typ komponentu widokowego rejestracji
 */
export interface RegisterComponentViewType {
    changeFormValue: (fieldName: string, value: FormValue) => void;
    sendForm: () => Promise<boolean>;
    clearForm: () => void;
    fields: FormControl[];
    errors: KeyValue<string, string>[];
}