import React, { useState } from "react";
import AnimatedCard from "../common/AnimatedCard/AnimatedCard";
import AnimatedCardContent from "../common/AnimatedCard/AnimatedCardContent";
import { Typography, Button, Grid } from "@material-ui/core";
import AnimatedCardActions from "../common/AnimatedCard/AnimatedCardActions";
import ASTextField from "../common/ASTextField/ASTextField";
import { ASCheckBox } from "../common/ASCheckBox/ASCheckBox";
import { RegisterComponentViewType } from "./RegisterComponentView.type";
import { FormValue } from "../../models/formValue.model";
import { KeyValue } from "../../models/keyValue.model";
import { ASPopup } from "../common/ASPopup/ASPopup";

/**
 * Komponent widoku rejestracji
 */
export const RegisterComponentView: React.FC<RegisterComponentViewType> = (
  props: RegisterComponentViewType
) => {
  const styles = {
    form: {
      marginTop: "1rem",
    },
    section: {
      marginBottom: "0.5rem",
    },
    rightButton: {
      justifyContent: "right",
    },
  };

  const fields = [
    { fieldName: "username", title: "Nazwa użytkownika", type: "textField" },
    {
      fieldName: "password",
      title: "Hasło",
      type: "textField",
      inputType: "password",
    },
    {
      fieldName: "confirmPassword",
      title: "Powtórz hasło",
      type: "textField",
      inputType: "password",
    },
    { fieldName: "firstName", title: "Imię", type: "textField" },
    { fieldName: "lastName", title: "Nazwisko", type: "textField" },
    {
      fieldName: "dateOfBirth",
      title: "Data urodzenia",
      type: "textField",
      inputType: "date",
    },
    {
      fieldName: "accepted",
      title: "Wyrażam zgodę na przestrzeganie regulaminu",
      type: "checkBox",
    },
  ];

  const [show, setShow] = useState(false);

  const closePopup = () => {
    setShow(false);
  }

  const handleFormChanges = (fieldName: string, value: FormValue) => {
    props.changeFormValue(fieldName, value);
  };

  const handleSendForm = async () => {
    const result = await props.sendForm();

    if (result) {
      setShow(true);
    }
  };

  const getError = (fieldName: string): JSX.Element => {
    const err = props.errors.find((x) => x.key === fieldName);

    return err ? (
      <div>
        <label style={{ color: "red" }}>{err.value}</label>
      </div>
    ) : (
        <></>
      );
  };

  const renderFields = (): JSX.Element => {
    return <>{fields.map((item, index) => renderField(index))}</>;
  };

  const clearForm = () => {
    props.clearForm();
  };

  const renderField = (index: number): JSX.Element => {
    const field = fields[index];

    if (field.type === "textField") {
      return (
        <React.Fragment key={field.fieldName}>
          {getError(field.fieldName)}
          <ASTextField
            value={getFieldValue(field.fieldName) as string}
            label={field.title}
            type={field.inputType}
            onChange={(e) => handleFormChanges(field.fieldName, e)}
            error={isError(field.fieldName)}
          />
        </React.Fragment>
      );
    } else {
      return (
        <React.Fragment key={field.fieldName}>
          {getError(field.fieldName)}
          <ASCheckBox
            checked={getFieldValue(field.fieldName) as boolean}
            label={field.title}
            onChange={(e) => handleFormChanges(field.fieldName, e)}
          />
        </React.Fragment>
      );
    }
  };

  const isError = (fieldName: string): boolean => {
    const err = props.errors.find((x) => x.key === fieldName) as KeyValue<
      string,
      string
    >;
    return err.value !== "";
  };

  const getFieldValue = (key: string): FormValue => {
    const field = props.fields.filter((x) => x.fieldName === key)[0];

    return field.fieldValue;
  };

  return (
    <>
      <AnimatedCard>
        <AnimatedCardContent>
          <Typography
            color="textSecondary"
            variant="h4"
            align={"center"}
            gutterBottom
          >
            Rejestracja
          </Typography>
          <form style={styles.form} noValidate autoComplete="off">
            {renderFields()}
          </form>
        </AnimatedCardContent>
        <AnimatedCardActions>
          <Grid container justify="space-between">
            <Grid item>
              <Button
                variant="outlined"
                size="small"
                onClick={() => clearForm()}
              >
                Wyczyść
              </Button>
            </Grid>
            <Grid item>
              <Button
                style={styles.rightButton}
                variant="outlined"
                color="primary"
                size="small"
                onClick={() => handleSendForm()}
              >
                Zarejestruj
              </Button>
            </Grid>
          </Grid>
        </AnimatedCardActions>
      </AnimatedCard>

      <ASPopup show={show} 
               duration={3000} 
               onClose={closePopup} 
               severity="success"
               variant="filled"
               origin={{ horizontal: "center", vertical: "top" }} 
               text="Pomyślnie zarejestrowano użytkownika" />
    </>
  );
};
