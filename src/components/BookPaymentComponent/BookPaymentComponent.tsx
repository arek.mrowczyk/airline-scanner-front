import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { IAppState } from '../../store/store';
import { BookState } from '../../models/book.model';
import { Typography, Button } from '@material-ui/core';
import { bookSetStepAction } from '../../store/actions/book.action';
import { inject } from '../../injector/injector';
import { BookingService } from '../../services/booking/booking.service';
import { AuthData } from '../../models/authData.model';
import { User } from '../../models/api/user.model';
import { dateConverterFromNumberArray } from '../../utils/dateConverter';
import { useHistory } from 'react-router-dom';
import { ASPopup } from '../common/ASPopup/ASPopup';

export const BookPaymentComponent: React.FC = () => {

    const flight = useSelector<IAppState, BookState>(x => x.bookState);
    const auth = useSelector<IAppState, AuthData>(x => x.authData).user as User;
    const seats = useSelector<IAppState, BookState>(x => x.bookState).seats;
    const bookingService = inject(BookingService) as BookingService;
    const history = useHistory();
    const [show, setShow] = useState(false);
    const dispatch = useDispatch();

    let positions = seats.reduce((acc, val) => acc += val.position + ', ', '') as string;
    positions = positions.slice(0, positions.length - 2);

    const calculateFare = () => {
        return flight.persons.length * flight.flight.fare;
    };

    const calculateSeatCost = () => {
        return flight.persons.length * 30;
    };

    const send = async () => {
        const res = await bookingService.sendBookingRequest(flight.flight, auth.login, flight.persons);
        if (res) {
            history.push('/home');
        } else {
            setShow(true);
        }
    }

    const prevStep = () => {
        dispatch(bookSetStepAction(1));
    }

    return (
        <div style={{ marginLeft: '1rem', marginRight: '1rem' }}>
            <Typography variant="h4">{flight.flight.connection.originAirport.airportName} -> {flight.flight.connection.destinationAirport.airportName}</Typography>

            <div style={{ marginTop: '1rem' }}>Dane osobowe</div>

            {flight.persons.map(x =>
                <div style={{ marginTop: '1rem', marginLeft: '0.5rem' }}>
                    <div><b>Imię: </b>{x.firstName}</div>
                    <div><b>Nazwisko: </b>{x.lastName}</div>
                    <div><b>Adres e-mail: </b>{x.email}</div>
                </div>
            )}

            <div style={{ marginTop: '1rem' }}>Wybrane miejsca</div>

            <div style={{ marginTop: '1rem', marginLeft: '0.5rem' }}>
                {positions}
            </div>

            <div style={{ marginTop: '1rem' }}>Informacje o locie</div>
            <div style={{ marginTop: '1rem', marginLeft: '0.5rem' }}>
                <b>Wylot dnia:</b> {dateConverterFromNumberArray(flight.flight.date)}
            </div>

            <div style={{ marginTop: '1rem', marginBottom: '1rem' }}>Koszt: {calculateFare()}zł</div>

            <Button
                variant="outlined"
                color="primary"
                size="small"
                onClick={prevStep}
            >
                Wstecz
                </Button>
            <Button
                variant="outlined"
                color="primary"
                size="small"
                onClick={send}
            >
                Kup
            </Button>
            <ASPopup show={show}
                duration={3000}
                onClose={() => setShow(false)}
                severity="error"
                variant="filled"
                origin={{ horizontal: "center", vertical: "top" }}
                text="Błąd podczas rezerwowania" />
        </div>
    )
}