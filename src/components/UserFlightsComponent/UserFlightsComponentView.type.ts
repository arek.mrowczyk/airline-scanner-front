import { TableData } from "../../models/tableData.model";

export interface UserFlightsComponentViewType {
    table: TableData;
}