import React, { useEffect, useState } from 'react';
import { inject } from '../../injector/injector';
import { BookingService } from '../../services/booking/booking.service';
import { UserFlightsComponentView } from './UserFlightsComponentView';
import { useSelector } from 'react-redux';
import { AuthData } from '../../models/authData.model';
import { IAppState } from '../../store/store';
import { TableData } from '../../models/tableData.model';
import { dateConverterFromNumberArray } from '../../utils/dateConverter';

interface UserFlightData {
    code: string;
    airportFrom: string;
    airportTo: string;
    date: string;
    cost: number;
}

interface UserFlightActions {
    components: any;
}

interface UserFlightRow {
    data: UserFlightData;
    actions: UserFlightActions;
}

export const UserFlightsComponent: React.FC = () => {

    const bookingService = inject<BookingService>(BookingService);
    const auth = useSelector<IAppState, AuthData>(x => x.authData);
    const [flights, setFlights] = useState<TableData>({
        columns: [
            {
                name: 'Kod'
            },
            {
                name: 'Wylot z'
            },
            {
                name: 'Wylot do'
            },
            {
                name: 'Dnia'
            },
            {
                name: 'Koszt'
            }
        ],
        rows: [{
            data: {},
            actions: {
                components: []
            }
        }]
    });

    useEffect(() => {
        bookingService.getAllBooking(auth.user!.login).then(x => {
            const res = {
                columns: [
                    {
                        name: 'Kod'
                    },
                    {
                        name: 'Wylot z'
                    },
                    {
                        name: 'Wylot do'
                    },
                    {
                        name: 'Dnia'
                    },
                    {
                        name: 'Koszt'
                    }
                ],
                rows: [] as UserFlightRow[]
            };

            for (let i = 0; i < x.length; i++) {
                res.rows.push({
                    data: {
                        code: x[i].bookingNumber,
                        airportFrom: x[i].flight.connection.originAirport.airportName,
                        airportTo: x[i].flight.connection.destinationAirport.airportName,
                        date: dateConverterFromNumberArray(x[i].flight.date),
                        cost: x[i].price
                    },
                    actions: {
                        components: []
                    }
                })
            }

            setFlights(res);
        });
    }, []);

    return (
        <UserFlightsComponentView table={flights} />
    );
};