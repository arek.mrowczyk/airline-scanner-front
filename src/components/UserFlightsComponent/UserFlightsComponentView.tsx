import React from 'react';
import { Grid } from '@material-ui/core';
import { ASSimpleTable } from '../common/ASSimpleTable/ASSimpleTable';
import { UserFlightsComponentViewType } from './UserFlightsComponentView.type';

export const UserFlightsComponentView: React.FC<UserFlightsComponentViewType> = (props: UserFlightsComponentViewType) => {

    const styles = {
        grid: {
            marginLeft: '2.5rem',
            marginRight: '2.5rem',
            marginTop: '0.5rem'
        },
        divider: {
            marginBottom: '0.5rem'
        },
        field: {
            marginLeft: '0.5rem',
            marginRight: '0.5rem'
        }
    }

    return (
        <>
            <Grid container>
                <Grid item style={styles.grid} xs={12}>
                    <ASSimpleTable table={props.table} />
                </Grid>
            </Grid>
        </>
    )
}