import React, { useState } from 'react';
import { ASStepper } from '../common/ASStepper/ASStepper';
import { Step } from '../../models/step.model';
import { SeatSelectorComponent } from '../SeatSelectorComponent/SeatSelectorComponent';
import { BookInformationComponent } from '../BookInformationComponent/BookInformationComponent';
import { useSelector } from 'react-redux';
import { IAppState } from '../../store/store';
import { BookState } from '../../models/book.model';
import { BookPaymentComponent } from '../BookPaymentComponent/BookPaymentComponent';

export const BookComponentView: React.FC = () => {

    const step = useSelector<IAppState, BookState>(x => x.bookState);

    const steps: Step[] = [
        {
            label: 'Uzupełnij informacje',
            component: <BookInformationComponent />
        },
        {   
            label: 'Wybierz miejsce',
            component: <SeatSelectorComponent />
        },
        {
            label: 'Płatność',
            component: <BookPaymentComponent />
        }
    ]

    return (
        <ASStepper activeStep={step.currentStep} steps={steps} />
    );
}