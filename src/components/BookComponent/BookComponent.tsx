import React from 'react';
import { BookComponentView } from './BookComponentView';

export const BookComponent: React.FC = () => {

    return (
        <BookComponentView />
    );
}