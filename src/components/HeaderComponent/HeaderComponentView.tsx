import React, { CSSProperties, useState } from 'react';
import AppBar from '@material-ui/core/AppBar';
import { Toolbar, IconButton, Typography, Avatar } from '@material-ui/core';
import FlightTakeoffIcon from '@material-ui/icons/FlightTakeoff';
import { AccountSettingsComponent } from '../AccountSettingsComponent/AccountSettingsComponent';
import { useSelector } from 'react-redux';
import { IAppState } from '../../store/store';
import { AuthData } from '../../models/authData.model';
import { SidebarContentComponent } from '../SidebarContentComponent/SidebarContentComponent';

/**
 * Komponent nagłówka aplikacji
 */
export const HeaderComponentView: React.FC<any> = (props: any) => {

    const [showOptions, setShowOptions] = useState(false);
    const [showDrawer, setShowDrawer] = useState(false);

    const auth = useSelector<IAppState, AuthData>(x => x.authData);

    const styles: CSSProperties = {
        background: 'rgba(255,255,255,0)',
        boxShadow: 'none'
    }

    const titleStyle: CSSProperties = {
        flex: '1'
    };

    const avatarStyle: CSSProperties = {
        marginLeft: '0.4rem',
        cursor: 'pointer'
    };

    const showOptionsView = () => {
        setShowOptions(!showOptions);
    };

    const onOutsideClicked = () => {
        if (showOptions) {
            setShowOptions(false);
        }
    }

    const getFirstLettersOfUser = (): string => {
        if (auth.user) {
            return auth.user.firstName[0] + auth.user.lastName[0];
        }
        return '';
    }

    const onCloseDrawer = (): void => {
        setShowDrawer(false);
    }

    const toggleDrawer = () => {
        if (auth.isLogged) {
            setShowDrawer(true);
        }
    };

    return (
        <>
            <AppBar position="static" style={styles}>
                <Toolbar style={{ color: 'white' }}>
                    <IconButton onClick={toggleDrawer} edge="start" color="inherit" aria-label="menu">
                        <FlightTakeoffIcon />
                    </IconButton>
                    <Typography style={titleStyle} variant="h6">
                        Airline Scanner
                    </Typography>
                    {
                        auth.isLogged ?
                            <>
                                <Typography>{auth.user?.firstName} {auth.user?.lastName}</Typography>

                                <div id="avatar" style={avatarStyle} onClick={showOptionsView}><Avatar>{getFirstLettersOfUser()}</Avatar></div>
                            </> :
                            <></>
                    }
                </Toolbar>
            </AppBar>
            <SidebarContentComponent show={showDrawer} onClose={onCloseDrawer} />
            {
                showOptions ?
                    <AccountSettingsComponent onOutsideClicked={onOutsideClicked} /> :
                    <></>
            }
        </>
    );
}

export default HeaderComponentView;
