import React, { CSSProperties, useEffect } from 'react';
import { Card, CardContent, Typography, Avatar } from '@material-ui/core';
import { useSpring, animated } from 'react-spring';
import Chip from '@material-ui/core/Chip';
import Divider from '@material-ui/core/Divider';
import { AccountSettingsComponentType } from './AccountSettingsComponent.type';
import { useSelector } from 'react-redux';
import { IAppState } from '../../store/store';
import { AuthData } from '../../models/authData.model';
import {
    useHistory,
  } from "react-router-dom";

export const AccountSettingsComponent: React.FC<AccountSettingsComponentType> = (props: AccountSettingsComponentType) => {

    const auth = useSelector<IAppState, AuthData>(x => x.authData);

    const cardSpring = useSpring({
        from: {
            opacity: '0'
        },
        to: {
            opacity: '1'
        }
    });

    const optionsStyle: CSSProperties = {
        position: 'absolute',
        right: '0.5rem',
        zIndex: 1000
    };

    const cardStyle = {
        background: 'rgba(255,255,255,0.90)',
        padding: '1rem'
    }

    const avatarStyle: CSSProperties = {
        margin: 'auto',
        marginTop: '1rem',
        marginBottom: '2rem',
        transform: 'scale(2)'
    }

    const chipStyle: CSSProperties = {
        width: '100%',
        marginTop: '0.5rem'
    }

    const dividerStyle: CSSProperties = {
        marginBottom: '0.5rem'
    }

    useEffect(() => {
        const func = (e: any) => {
            if (document.getElementById('options')?.contains(e.target as HTMLElement) === false &&
                document.getElementById('avatar')?.contains(e.target as HTMLElement) === false) {
                props.onOutsideClicked();
            }
        };
        window.addEventListener('click', func);

        return () => {
            window.removeEventListener('click', func);
        }
    }, [props]);

    const getFirstLettersOfUser = (): string => {
        if (auth.user) {
            return auth.user.firstName[0] + auth.user.lastName[0];
        }
        return '';
    }

    const history = useHistory();

    const redirectToMyProfile = () => {
        history.push("/myProfile");
    }

    return (
        <div id="options" style={optionsStyle}>
            <animated.div style={cardSpring}>
                <Card style={cardStyle} variant="outlined">
                    <CardContent>
                        <Avatar style={avatarStyle}>{getFirstLettersOfUser()}</Avatar>
                        <Typography variant="h5" gutterBottom align="center">
                            {auth.user?.firstName} {auth.user?.lastName}
                        </Typography>
                        <Divider style={dividerStyle} variant="middle" />
                        <Chip style={chipStyle} variant="outlined" label="Mój profil" onClick={redirectToMyProfile} />
                        <Chip style={chipStyle} variant="outlined" label="Wyloguj się" onClick={() => console.log(2)} />
                    </CardContent>
                </Card>
            </animated.div>
        </div>
    )
};

