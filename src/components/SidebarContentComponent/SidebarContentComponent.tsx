import React from "react";
import AppBar from "@material-ui/core/AppBar";
import {
  Typography,
  Avatar,
  Drawer,
  List,
  ListItemText,
  ListItem
} from "@material-ui/core";
import { SidebarContentType } from "./SidebarContent.type";
import { useSelector } from "react-redux";
import { IAppState } from "../../store/store";
import { AuthData } from "../../models/authData.model";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import { Routing, IRouting } from "../../route/routing";
import { Link, useRouteMatch } from "react-router-dom";

const linkStyle: CSSProperties = {
  textDecoration: "none"
};

const MenuLink = ({ route }: { route: IRouting }) => {
  let match = useRouteMatch({
    path: route.path
  });

  return (
    <Link to={route.path} style={linkStyle}>
      <ListItem button key={route.path}>
        <ListItemText primary={route.title}/>
      </ListItem>
    </Link>
  );
};

export const SidebarContentComponent: React.FC<SidebarContentType> = (
  props: SidebarContentType
) => {
  const auth = useSelector<IAppState, AuthData>(x => x.authData);

  const containerStyle: CSSProperties = {
    width: "250px"
  };

  const appBarStyle: CSSProperties = {
    padding: "1rem",
    flexDirection: "row"
  };

  const colStyle: CSSProperties = {
    flex: 1
  };

  const avatarStyle: CSSProperties = {
    margin: "auto",
    marginBottom: "1rem",
    transform: "translateY(-50%)",
    top: "50%",
    width: "50px",
    height: "50px"
  };

  const getFirstLettersOfUser = (): string => {
    if (auth.user) {
      return auth.user.firstName[0] + auth.user.lastName[0];
    }
    return "";
  };

  const sidebarRoutes = Routing.filter(r => r.drawer).filter(r => !(r.admin && !auth?.user?.isAdmin));

  const renderRoutes = () =>
    sidebarRoutes.map(r => <MenuLink route={r} key={r.path} />);

  return (
    <Drawer anchor="left" open={props.show} onClose={props.onClose}>
      <div style={containerStyle}>
        <AppBar style={appBarStyle} position="static">
          <div style={colStyle}>
            <Typography align="center" variant="h5">
              Witaj,
            </Typography>
            <Typography align="center" variant="h5">
              {auth.user?.firstName}
            </Typography>
          </div>
          <div style={colStyle}>
            <Avatar style={avatarStyle}>{getFirstLettersOfUser()}</Avatar>
          </div>
        </AppBar>
        <List>{renderRoutes()}</List>
      </div>
    </Drawer>
  );
};
