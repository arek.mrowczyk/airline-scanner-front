export interface SidebarContentType {
    show: boolean;
    onClose: () => void;
}