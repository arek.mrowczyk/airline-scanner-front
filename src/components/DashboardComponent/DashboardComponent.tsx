import React, { useEffect } from 'react';
import { DashboardComponentView } from './DashboardComponentView';
import { inject } from '../../injector/injector';
import { CountriesService } from '../../services/countries/countries.service';
import { useSelector, useDispatch } from 'react-redux';
import { IAppState } from '../../store/store';
import { FilterGroup } from '../../models/filterGroup.model';
import { FilterField } from '../../models/filterField.model';
import { AirportService } from '../../services/airport/airport.service';
import { dashboardChangeField } from '../../store/actions/dashboard.action';
import { DashboardFilterState } from '../../models/dashboard.filter.model';
import { AirportConnectionService } from '../../services/airportConnection/airportConnection.service';
import { DashboardTableState } from '../../models/dashboardTable.model';
import { changeDashboardTable } from '../../store/actions/dashboardTable.action';
import { DashboardService } from '../../services/dashboard/dashboard.service';
import { FlightService } from '../../services/flight/flight.service';
import { IconButton } from '@material-ui/core';
import BookIcon from '@material-ui/icons/Book';
import { Flight } from '../../models/api/flight.model';
import { useHistory } from 'react-router-dom';
import { bookSetFlightAction, bookResetStateAction } from '../../store/actions/book.action';

export const DashboardComponent: React.FC = () => {

    const countriesService = inject(CountriesService) as CountriesService;
    const airportService = inject(AirportService) as AirportService;
    const dashboardService = inject(DashboardService) as DashboardService;
    const flightService = inject(FlightService) as FlightService;
    const filter = useSelector<IAppState, DashboardFilterState>(x => x.dashboardFilter);
    const table = useSelector<IAppState, DashboardTableState>(x => x.dashboardTable);
    const dispatch = useDispatch();

    const Actions = (flight: any) => {

        const history = useHistory();

        const book = () => {
            dispatch(bookResetStateAction());
            dispatch(bookSetFlightAction(flight.flight));

            history.push('/book');
        }

        return (
            <IconButton onClick={book}>
                <BookIcon fontSize="small" />
            </IconButton>
        )
    }

    useEffect(() => {
        countriesService.getCountries().then(x => {
            dispatch(dashboardChangeField('countries', x));
        });
        flightService.getAllFlights().then(x => {
            dispatch(changeDashboardTable(dashboardService.getTableData(x, <Actions />)));
        });
    }, []);

    const group: FilterGroup = {
        items: [
            {
                title: 'Wylot z',
                type: 'select',
                fieldName: 'countryFrom',
                items: filter.countries.map(x => ({ key: x.name, value: x.name })),
                fieldValue: filter.countryFrom ? filter.countryFrom.name : ''
            },
            {
                title: 'Lotnisko',
                type: 'select',
                fieldName: 'airportFrom',
                items: filter.airportsFrom.map(x => ({ key: x.id + '', value: x.airportName })),
                fieldValue: filter.airportFrom ? filter.airportFrom.id + '' : ''
            },
            {
                title: 'Cel lotu',
                type: 'select',
                fieldName: 'countryTo',
                items: filter.countries.map(x => ({ key: x.name, value: x.name })),
                fieldValue: filter.countryTo ? filter.countryTo.name : ''
            },
            {
                title: 'Lotnisko docelowe',
                type: 'select',
                fieldName: 'airportTo',
                items: filter.airportsTo.map(x => ({ key: x.id + '', value: x.airportName })),
                fieldValue: filter.airportTo ? filter.airportTo.id + '' : ''
            },
            {
                title: 'Liczba osób',
                type: 'textbox',
                fieldName: 'personCount',
                fieldValue: filter.personCount + ''
            },
        ]
    };

    const onChange = async (field: FilterField, value: string) => {
        dispatch(dashboardChangeField(field.fieldName, value));

        if (field.fieldName === 'countryFrom') {
            dispatch(dashboardChangeField('airportsFrom', await airportService.getAirportsForCountry(value)))
        } else if (field.fieldName === 'countryTo') {
            dispatch(dashboardChangeField('airportsTo', await airportService.getAirportsForCountry(value)))
        }
    };

    const onFilter = () => {
        flightService.getFlightsForCodes(
            filter.airportFrom!.airportCode,
            filter.airportTo!.airportCode
        ).then(x => dispatch(changeDashboardTable(dashboardService.getTableData(x, <Actions />))))
    };

    return (
        <DashboardComponentView group={group} onChange={onChange} onFilter={onFilter} table={table.table} />
    );
};