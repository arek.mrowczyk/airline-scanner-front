import React from 'react';
import { Grid } from '@material-ui/core';
import ASFilter from '../common/ASFilter/ASFilter';
import { DashboardComponentViewType } from './DashboardComponentView.type';
import { ASSimpleTable } from '../common/ASSimpleTable/ASSimpleTable';

export const DashboardComponentView: React.FC<DashboardComponentViewType> = (props: DashboardComponentViewType) => {

    const styles = {
        grid: {
            marginLeft: '2.5rem',
            marginRight: '2.5rem',
            marginTop: '0.5rem'
        },
        divider: {
            marginBottom: '0.5rem'
        },
        field: {
            marginLeft: '0.5rem',
            marginRight: '0.5rem'
        }
    }

    return (
        <>
            <Grid container>
                <Grid item style={styles.grid} xs={12}>
                    <ASFilter group={props.group} onChange={props.onChange} onFilter={props.onFilter} />
                </Grid>
            </Grid>
            <Grid container>
                <Grid item style={styles.grid} xs={12}>
                    <ASSimpleTable table={props.table} />
                </Grid>
            </Grid>
        </>
    );
};