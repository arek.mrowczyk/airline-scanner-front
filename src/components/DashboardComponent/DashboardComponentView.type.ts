import { FilterGroup } from "../../models/filterGroup.model";
import { FilterField } from "../../models/filterField.model";
import { TableData } from "../../models/tableData.model";

export interface DashboardComponentViewType {
    group: FilterGroup;
    table: TableData;
    onChange: (field: FilterField, value: string) => void;
    onFilter: () => void;
}