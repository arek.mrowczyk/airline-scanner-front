import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { IAppState } from '../../store/store';
import { BookState } from '../../models/book.model';
import { Typography, Button, Grid, List, ListItem, ListItemText } from '@material-ui/core';
import { bookSetStepAction, bookSetPersonsAction } from '../../store/actions/book.action';
import ASTextField from '../common/ASTextField/ASTextField';
import { Person } from '../../models/api/person.model';
import { useHistory } from 'react-router-dom';

export const BookInformationComponent: React.FC = () => {

    const flight = useSelector<IAppState, BookState>(x => x.bookState).flight;
    const person = useSelector<IAppState, BookState>(x => x.bookState).persons;
    const dispatch = useDispatch();
    const history = useHistory();
    const [persons, setPersons] = useState<Person[]>(person);
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');

    const addPerson = () => {
        const res = [...persons];
        res.push({ firstName, lastName, email });

        setPersons(res);
        setFirstName('');
        setLastName('');
        setEmail('');
    };

    const calculateFare = () => {
        return persons.length * flight.fare;
    };

    const calculateSeatCost = () => {
        return persons.length * 30;
    };

    const nextStep = () => {
        dispatch(bookSetPersonsAction(persons));
        dispatch(bookSetStepAction(1));
    };

    const prevStep = () => {
        history.push('/home');
    }

    return (
        <div style={{ marginLeft: '1rem', marginRight: '1rem' }}>
            <Typography variant="h4">{flight.connection.originAirport.airportName} -> {flight.connection.destinationAirport.airportName}</Typography>

            <Grid container>

                <Grid item xs={2} style={{ marginTop: '1rem', marginBottom: '1rem' }}>
                    <Typography variant="caption">Uzupełnij informacje o osobach</Typography>
                    <ASTextField label="Imię osoby" value={firstName} onChange={setFirstName} />
                    <ASTextField label="Nazwisko osoby" value={lastName} onChange={setLastName} />
                    <ASTextField label="Adres e-mail osoby" value={email} onChange={setEmail} />
                    <Button
                        variant="outlined"
                        color="primary"
                        size="small"
                        onClick={addPerson}
                    >
                        Dodaj osobę
                </Button>
                </Grid>
                <Grid item xs={2} style={{ marginTop: '1rem', marginBottom: '1rem' }}>
                    <List>
                        {persons.map(x =>
                            <ListItem>
                                <ListItemText primary={x.firstName + ' ' + x.lastName} secondary={x.email} />
                            </ListItem>
                        )}
                    </List>
                </Grid>
                <Grid item xs={2} style={{ marginTop: '1rem', marginBottom: '1rem' }}>
                    <Typography variant="subtitle1">Koszt: {calculateFare()}zł</Typography>
                </Grid>

            </Grid>

            <Button
                variant="outlined"
                color="primary"
                size="small"
                onClick={prevStep}
            >
                Wstecz
                </Button>
            <Button
                variant="outlined"
                color="primary"
                size="small"
                onClick={() => nextStep()}
            >
                Dalej
            </Button>
        </div>
    )
}