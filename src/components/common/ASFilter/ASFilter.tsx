import React from 'react';
import AnimatedCard from '../AnimatedCard/AnimatedCard';
import AnimatedCardContent from '../AnimatedCard/AnimatedCardContent';
import AnimatedCardActions from '../AnimatedCard/AnimatedCardActions';
import { Grid, Typography, Divider, Button } from '@material-ui/core';
import { ASFilterType } from './ASFilter.type';
import { FilterField } from '../../../models/filterField.model';
import ASSelect from '../ASSelect/ASSelect';
import { KeyValue } from '../../../models/keyValue.model';
import ASTextField from '../ASTextField/ASTextField';

export const ASFilter: React.FC<ASFilterType> = (props: ASFilterType) => {

    const styles = {
        grid: {
            marginLeft: '2.5rem',
            marginRight: '2.5rem',
            marginTop: '0.5rem'
        },
        divider: {
            marginBottom: '0.5rem'
        },
        field: {
            marginLeft: '0.5rem',
            marginRight: '0.5rem'
        },
        rightButton: {
            justifyContent: "right",
        }
    };

    const onChange = (field: FilterField, value: string) => {
        if (props.onChange) {
            props.onChange(field, value);
        }
    }

    const resolveField = (field: FilterField): JSX.Element => {
        if (field.type === 'select') {
            return <ASSelect key={field.fieldName} label={field.title} value={field.fieldValue} items={field.items as KeyValue<string, string>[]} onChange={(v) => onChange(field, v)} />
        } else {
            return <ASTextField key={field.fieldName} label={field.title} value={field.fieldValue} onChange={(v) => onChange(field, v)} />
        }
    };

    return (
        <AnimatedCard>
            <AnimatedCardContent>
                <Typography
                    variant="subtitle1"
                    align="left"
                    gutterBottom
                >
                    Filtr
                        </Typography>
                <Divider style={styles.divider} variant="middle" />
                <Grid container justify="space-between">
                    {props.group.items.map(x =>
                        <Grid key={x.fieldName + 'g'} item xs style={styles.field}>
                            {resolveField(x)}
                        </Grid>
                    )}
                </Grid>
            </AnimatedCardContent>
            <AnimatedCardActions>
                <Grid container justify="space-between">
                    <Grid item>
                    </Grid>
                    <Grid item>
                        <Button
                            style={styles.rightButton}
                            variant="outlined"
                            color="primary"
                            size="small"
                            onClick={() => props.onFilter()}>
                            Filtruj
                        </Button>
                    </Grid>
                </Grid>
            </AnimatedCardActions>
        </AnimatedCard>
    );
}

export default ASFilter;