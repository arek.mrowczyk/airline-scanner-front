import { FilterGroup } from "../../../models/filterGroup.model";
import { FilterField } from "../../../models/filterField.model";

export interface ASFilterType {
    group: FilterGroup;
    onChange?: (field: FilterField, value: string) => void;
    onFilter: () => void;
}