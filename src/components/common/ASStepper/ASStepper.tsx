import React from 'react';
import { Stepper, Step, StepLabel, StepIconProps, makeStyles, withStyles, StepConnector, Grid } from '@material-ui/core';
import Check from '@material-ui/icons/Check';
import clsx from 'clsx';
import AnimatedCard from '../AnimatedCard/AnimatedCard';
import AnimatedCardContent from '../AnimatedCard/AnimatedCardContent';
import { ASStepperType } from './ASStepper.type';

const useQontoStepIconStyles = makeStyles({
    root: {
        color: '#eaeaf0',
        display: 'flex',
        height: 22,
        alignItems: 'center',
    },
    active: {
        color: '#2196F3',
    },
    circle: {
        width: 8,
        height: 8,
        borderRadius: '50%',
        backgroundColor: 'currentColor',
    },
    completed: {
        color: '#2196F3',
        zIndex: 1,
        fontSize: 18,
    },
});

const QontoConnector = withStyles({
    alternativeLabel: {
        top: 10,
        left: 'calc(-50% + 16px)',
        right: 'calc(50% + 16px)',
    },
    active: {
        '& $line': {
            borderColor: '#2196F3',
        },
    },
    completed: {
        '& $line': {
            borderColor: '#2196F3',
        },
    },
    line: {
        borderColor: '#eaeaf0',
        borderTopWidth: 3,
        borderRadius: 1,
    },
})(StepConnector);

function QontoStepIcon(props: StepIconProps) {
    const classes = useQontoStepIconStyles();
    const { active, completed } = props;

    return (
        <div
            className={clsx(classes.root, {
                [classes.active]: active,
            })}
        >
            {completed ? <Check className={classes.completed} /> : <div className={classes.circle} />}
        </div>
    );
}

export const ASStepper: React.FC<ASStepperType> = (props: ASStepperType) => {

    const styles = {
        grid: {
            paddingLeft: '2.5rem',
            paddingRight: '2.5rem',
            paddingTop: '0.5rem'
        },
        stepper: {
            backgroundColor: 'inherit'
        }
    }

    return (
        <Grid container style={styles.grid}>
            <Grid item xs>
                <AnimatedCard>
                    <AnimatedCardContent>
                        <Stepper style={styles.stepper} alternativeLabel activeStep={props.activeStep} connector={<QontoConnector />}>
                            {
                                props.steps.map((x, i) =>
                                    <Step key={x.label}>
                                        <StepLabel StepIconComponent={QontoStepIcon}>{x.label}</StepLabel>
                                    </Step>
                                )
                            }
                        </Stepper>
                        {props.steps[props.activeStep].component}
                    </AnimatedCardContent>
                </AnimatedCard>
            </Grid>
        </Grid>
    );
}