import { Step } from "../../../models/step.model";

export interface ASStepperType {
    steps: Step[];
    activeStep: number;
}