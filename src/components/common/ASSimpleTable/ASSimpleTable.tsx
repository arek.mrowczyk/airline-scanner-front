import React, { useState } from 'react';
import { TableContainer, Paper, Table, TableCell, TableRow, TableHead, TableBody, TableFooter, TablePagination } from '@material-ui/core';
import { useSpring, animated } from 'react-spring';
import { ASSimpleTableType } from './ASSimpleTable.type';
import TablePaginationActions from '@material-ui/core/TablePagination/TablePaginationActions';

export const ASSimpleTable: React.FC<ASSimpleTableType> = (props: ASSimpleTableType) => {

    const [page, setPage] = useState(1);
    const [rowPerPage, setRowPerPage] = useState(5);

    const cardSpring = useSpring({
        from: {
            transform: 'ScaleY(0)'
        },
        to: {
            transform: 'ScaleY(1)'
        }
    });

    return (
        <animated.div style={cardSpring}>
            <TableContainer component={Paper}>
                <Table size="small">
                    <TableHead>
                        <TableRow>
                            {props.table.columns.map(x => <TableCell key={x.name}>{x.name}</TableCell>)}
                            {props.table.rows.some(x => x.actions) ? <TableCell key="Akcje">Akcje</TableCell> : <></>}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {props.table.rows.slice(page * rowPerPage, page * rowPerPage + rowPerPage).map((x, i) =>
                            <TableRow key={i}>
                                {Object.keys(x.data).map((y, i2) =>
                                    <TableCell key={i + '_' + i2}>{x.data[y]}</TableCell>
                                )}
                                {x.actions ?
                                    <TableCell key={'a' + i}>
                                        {x.actions.components.map(Y =>
                                            Y
                                        )}
                                    </TableCell> :
                                    <></>}
                            </TableRow>
                        )}
                    </TableBody>
                    <TableFooter>
                        <TableRow>
                            <TablePagination
                                rowsPerPageOptions={[5]}
                                colSpan={3}
                                count={props.table.rows.length}
                                rowsPerPage={rowPerPage}
                                page={page}
                                SelectProps={{
                                    inputProps: { 'aria-label': 'rows per page' },
                                    native: true,
                                }}
                                onChangePage={(ev, page) => setPage(page)}
                                onChangeRowsPerPage={(ev) => setRowPerPage(parseInt(ev.target.value))}
                                ActionsComponent={TablePaginationActions}
                            />
                        </TableRow>
                    </TableFooter>
                </Table>
            </TableContainer>
        </animated.div>
    )
}