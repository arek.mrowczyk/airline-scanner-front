import { TableData } from "../../../models/tableData.model";

export interface ASSimpleTableType {
    table: TableData;
}