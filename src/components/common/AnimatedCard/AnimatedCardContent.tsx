import React from 'react';

/**
 * Komponent zawartości anonimowanej karty
 */
export const AnimatedCardContent: React.FC = props => {
    return (
        <>{props.children}</>
    );
}

export default AnimatedCardContent;