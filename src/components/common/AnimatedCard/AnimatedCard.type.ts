import AnimatedCardContent from "./AnimatedCardContent";
import AnimatedCardActions from "./AnimatedCardActions";

/**
 * Typ animowaniej karty
 */
export interface AnimatedCardType {
    children: React.ReactElement<typeof AnimatedCardContent | typeof AnimatedCardActions>[] | React.ReactElement<typeof AnimatedCardContent | typeof AnimatedCardActions>;
}