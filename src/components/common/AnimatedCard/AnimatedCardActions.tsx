import React from 'react';

/**
 * Komponent akcji anonimowanej karty
 */
export const AnimatedCardActions: React.FC = props => {
    return (
        <>{props.children}</>
    );
}

export default AnimatedCardActions;