export interface AuthRouteComponentType {
    isLogged?: boolean;
    children?: React.ReactElement<any>;
    path?: string;
    strict?: boolean;
    exact?: boolean;
}