import React from 'react';
import { AuthRouteComponentType } from './AuthRouteComponent.type';
import { IAppState } from '../../../store/store';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { Route, Redirect } from 'react-router-dom';

/**
 * Komponent odpowiadający routingowi dla zalogowanego użytkownika
 */
export const AuthRouteComponent: React.FC<AuthRouteComponentType> = (props: AuthRouteComponentType) => {

    return (
        props.isLogged ? 
        <Route path={props.path} strict={props.strict} exact={props.exact} render={prop => React.Children.only(props.children)}></Route> : 
        <Redirect to="/" />
    )
}

const mapProps = (state: IAppState, props: AuthRouteComponentType): AuthRouteComponentType => ({
    isLogged: state.authData.isLogged
})

const mapDispatch = (dispatch: Dispatch<any>) => ({
})

const AuthRoute = connect(mapProps, mapDispatch)(AuthRouteComponent);

export default AuthRoute;