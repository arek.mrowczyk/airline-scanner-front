import React from 'react';
import { ASPopupType } from './ASPopup.type';
import { Snackbar } from '@material-ui/core';
import { Alert } from '@material-ui/lab';

export const ASPopup = (props: ASPopupType) => {

    const handleCloseNotification = () => props.onClose();

    return (
        <Snackbar
            open={props.show}
            autoHideDuration={props.duration}
            anchorOrigin={props.origin}
            onClose={handleCloseNotification}
        >
            <Alert
                onClose={handleCloseNotification}
                variant={props.variant}
                severity={props.severity}
            >
                {props.text}
        </Alert>
        </Snackbar>
    );
}