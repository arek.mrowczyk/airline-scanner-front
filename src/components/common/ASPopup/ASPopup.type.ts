import { SnackbarOrigin } from "@material-ui/core";

export interface ASPopupType {
    show: boolean;
    duration: number;
    onClose: () => void;
    origin: SnackbarOrigin;
    variant?: "outlined" | "standard" | "filled" | undefined;
    severity?: "success" | "info" | "warning" | "error" | undefined;
    text: string;
}