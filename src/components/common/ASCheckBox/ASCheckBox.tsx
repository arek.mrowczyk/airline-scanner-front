import React from 'react';
import { ASCheckBoxType } from './ASCheckBox.type';
import { FormControlLabel, Checkbox } from '@material-ui/core';

/**
 * Komponent checkbox'a
 */
export const ASCheckBox: React.FC<ASCheckBoxType> = (props: ASCheckBoxType) => {

    const onChangeHandler = (value: boolean) => {
        if (props.onChange) {
            props.onChange(value);
        }
    };

    return (
        <FormControlLabel control={<Checkbox color="primary" />}
                          label={props.label}
                          checked={props.checked}
                          onChange={(e)=>onChangeHandler((e.target as any).checked)} />
    );
}