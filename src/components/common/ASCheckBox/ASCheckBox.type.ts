/**
 * Typ komponentu checkboxa
 */
export interface ASCheckBoxType {
    label?: string;
    checked?: boolean;
    onChange?: (value: boolean) => void;
}