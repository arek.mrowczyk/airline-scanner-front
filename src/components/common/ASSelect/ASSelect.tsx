import React, { CSSProperties } from 'react';
import { Select, MenuItem, InputLabel, FormControl } from '@material-ui/core';
import { ASSelectType } from './ASSelect.type';

/**
 * Komponent pola wybieralnego
 */
export const ASSelect: React.FC<ASSelectType> = (props: ASSelectType) => {

    const style: CSSProperties = {
        width: '100%',
        marginBottom: '0.5rem',
        marginTop: '0.5rem',
        marginLeft: '0.25rem',
        marginRight: '0.25rem'
    };

    const onChangeHandler = (value: string) => {
        if (props.onChange) {
            props.onChange(value);
        }
    };

    return (
        <FormControl key={props.label + 'f'} style={style} variant="outlined" margin="dense">
            <InputLabel key={props.label + 'i'}>{props.label}</InputLabel>
            <Select
                key={props.label + 's'}
                label={props.label}
                placeholder={props.label}
                error={props.error}
                onChange={(p) => onChangeHandler(p.target.value as string)}
                value={props.value}>
                {props.items.map(x => <MenuItem key={x.key + props.label} value={x.key}>{x.value}</MenuItem>)}
            </Select>
        </FormControl>
    );
}

export default ASSelect;