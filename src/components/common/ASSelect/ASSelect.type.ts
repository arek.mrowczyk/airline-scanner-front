/**
 * Typ komponentu pola wybieralnego
 */
export interface ASSelectType {
    label?: string;
    onChange?: (value: string) => void;
    value?: string;
    items: {key: string, value: string}[];
    error?: boolean;
}