import React, { CSSProperties } from 'react';
import { TextField } from '@material-ui/core';
import { ASTextFieldType } from './ASTextField.type';

/**
 * Komponent pola tekstowego
 */
export const ASTextField: React.FC<ASTextFieldType> = (props: ASTextFieldType) => {

    const style: CSSProperties = {
        width: '100%',
        marginBottom: '0.5rem',
        marginTop: '0.5rem'
    };

    const onChangeHandler = (value: string) => {
        if (props.onChange) {
            props.onChange(value);
        }
    };

    return (
        <TextField style={style}
            label={props.label}
            error={props.error}
            variant="outlined"
            type={props.type}
            onChange={(p) => onChangeHandler(p.target.value)}
            value={props.value}
            margin="dense" />
    );
}

export default ASTextField;