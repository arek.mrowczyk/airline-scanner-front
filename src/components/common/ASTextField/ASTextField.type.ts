/**
 * Typ komponentu pola tekstowego
 */
export interface ASTextFieldType {
    type?: string;
    label?: string;
    onChange?: (value: string) => void;
    value?: string;
    error?: boolean;
}