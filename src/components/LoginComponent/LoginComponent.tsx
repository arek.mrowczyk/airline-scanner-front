import React from "react";
import { LoginComponentView } from "./LoginComponentView";
import { useSelector, useDispatch } from "react-redux";
import { IAppState } from "../../store/store";
import { FormValue } from "../../models/formValue.model";
import { inject } from "../../injector/injector";
import { LoginService } from "../../services/login/login.service";
import { FormControl } from "../../models/formControl.model";
import {
  loginFormUpdateAction,
  clearLoginFormAction,
} from "../../store/actions/loginForm.action";
import { KeyValue } from "../../models/keyValue.model";
import { useHistory } from "react-router-dom";

/**
 * Komponent logowania
 */
export const LoginComponentTemplate: React.FC = (
) => {

  const errors = useSelector<IAppState, KeyValue<string, string>[]>(x => x.loginForm.fields.map(y => ({ key: y.fieldName, value: y.error.message })));
  const fields = useSelector<IAppState, FormControl[]>(x => x.loginForm.fields);
  const loginError = useSelector<IAppState, string | undefined>(x => x.loginForm.error);

  const dispatch = useDispatch();

  const service = inject(LoginService) as LoginService;
  const history = useHistory();

  const sendForm = async () => {
    const res = await service.sendForm(fields, history);

    if (res) {
      dispatch(clearLoginFormAction());
    } else {
      dispatch(loginFormUpdateAction(fields));
    }
  };

  const changeFormValue = async (fieldName: string, value: FormValue) => {
    const fieldss = await service.changeFormAndValidate(fieldName, value, fields);

    dispatch(loginFormUpdateAction(fieldss));
  }
  return (
    <LoginComponentView
      errors={errors}
      changeFormValue={changeFormValue}
      fields={fields}
      sendForm={sendForm}
      loginError={loginError}
    />
  );
};

const LoginComponent = LoginComponentTemplate

export default LoginComponent;
