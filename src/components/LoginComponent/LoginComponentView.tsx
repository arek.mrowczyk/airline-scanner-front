import React from "react";
import AnimatedCard from "../common/AnimatedCard/AnimatedCard";
import { Typography, Button, Grid } from "@material-ui/core";
import AnimatedCardContent from "../common/AnimatedCard/AnimatedCardContent";
import AnimatedCardActions from "../common/AnimatedCard/AnimatedCardActions";
import { LoginComponentViewType } from "./LoginComponentView.type";
import ASTextField from "../common/ASTextField/ASTextField";
import { FormValue } from "../../models/formValue.model";
import { KeyValue } from "../../models/keyValue.model";

const renderErrorLabel = (value: any): JSX.Element => (
  <div>
    <label style={{ color: "red" }}>{value}</label>
  </div>
);

/**
 * Komponent widoku logowania
 */
export const LoginComponentView: React.FC<LoginComponentViewType> = (
  props: LoginComponentViewType
) => {
  const styles = {
    form: {
      marginTop: "1rem"
    }
  };

  const fields = [
    { fieldName: "username", title: "Nazwa użytkownika", type: "textField" },
    {
      fieldName: "password",
      title: "Hasło",
      type: "textField",
      inputType: "password"
    }
  ];

  const handleSendForm = () => {
    props.sendForm();
  };

  const handleFormChanges = (fieldName: string, value: FormValue) => {
    props.changeFormValue(fieldName, value);
  };

  const getFieldValue = (key: string): FormValue => {
    const field = props.fields.filter(x => x.fieldName === key)[0];

    return field.fieldValue;
  };

  const getError = (fieldName: string): JSX.Element => {
    const err = props.errors.find(x => x.key === fieldName);
    return err ? renderErrorLabel(err.value) : <></>;
  };

  const isError = (fieldName: string): boolean => {
    const err = props.errors.find(x => x.key === fieldName) as KeyValue<
      string,
      string
    >;
    return err.value !== "";
  };

  const renderFields = (): JSX.Element => {
    return <>{fields.map((item, index) => renderField(index))}</>;
  };

  const renderField = (index: number): JSX.Element => {
    const field = fields[index];
    return (
      <React.Fragment key={field.fieldName}>
        {getError(field.fieldName)}
        <ASTextField
          value={getFieldValue(field.fieldName) as string}
          label={field.title}
          type={field.inputType}
          onChange={e => handleFormChanges(field.fieldName, e)}
          error={isError(field.fieldName)}
        />
      </React.Fragment>
    );
  };

  const renderLoginError = (): JSX.Element => {
    if (!props.loginError) return <></>;
    return renderErrorLabel(props.loginError);
  };

  return (
    <AnimatedCard>
      <AnimatedCardContent>
        <Typography
          color="textSecondary"
          variant="h4"
          align={"center"}
          gutterBottom
        >
          Logowanie
        </Typography>
        <form style={styles.form} noValidate autoComplete="off">
          {renderFields()}
        </form>
        {renderLoginError()}
      </AnimatedCardContent>
      <AnimatedCardActions>
        <Grid container justify="flex-end">
          <Grid item>
            <Button
              variant="outlined"
              color="primary"
              size="small"
              onClick={handleSendForm}
            >
              Zaloguj
            </Button>
          </Grid>
        </Grid>
      </AnimatedCardActions>
    </AnimatedCard>
  );
};
