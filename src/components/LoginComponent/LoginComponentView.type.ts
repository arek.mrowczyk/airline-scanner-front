import { FormValue } from "../../models/formValue.model";
import { KeyValue } from '../../models/keyValue.model';
import { FormControl } from "../../models/formControl.model";

/**
 * Komponent typu widoku logowania
 */
export interface LoginComponentViewType {
    changeFormValue: (fieldName: string, value: FormValue) => void;
    fields: FormControl[];
    errors: KeyValue<string, string>[];
    loginError?: string;
    sendForm: () => void;
}