import React from "react";
import AnimatedCardContent from "../../../components/common/AnimatedCard/AnimatedCardContent";
import AnimatedCard from "../../../components/common/AnimatedCard/AnimatedCard";
import { Grid } from "@material-ui/core";
import { FlightsPageViewType } from "./FlightsPageView.type";
import ASTable from "../../../components/common/ASTable/ASTable";

const FlightsPageView: React.FC<FlightsPageViewType> = props => {
  const columns = [
    { title: "Samolot", field: "aircraftModel" },
    { title: "Koszt", field: "fare", type: "numeric" },
    { title: "Dostępne miejsca", field: "availableSeats",type: "numeric" },
    { title: "Data", field: "date", type: "datetime" },
    {
      title: "Połączenie",
      field: "connection",
      lookup: props.additionalData.airportConnections
    }
  ];

  return (
    <Grid container>
      <Grid item xs={2}></Grid>
      <Grid item xs={8}>
        <AnimatedCard>
          <AnimatedCardContent>
            <ASTable
              title="Loty"
              columns={columns}
              data={props.data}
              editable={{
                onRowAdd: props.add,
                onRowUpdate: props.edit,
                onRowDelete: props.delete
              }}
            />
          </AnimatedCardContent>
        </AnimatedCard>
      </Grid>
      <Grid item xs={2}></Grid>
    </Grid>
  );
};

export default FlightsPageView;
