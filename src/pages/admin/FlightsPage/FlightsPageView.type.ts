import { Flight } from "../../../models/api/flight.model";

/**
 * Strona z lotami
 */
export interface FlightsPageViewType {
    data: Flight[];
    additionalData: any;
    add: (data: Flight) => void;
    edit: (data: Flight) => void;
    delete: (data: Flight) => void;
}