import React, { useEffect } from "react";
import FlightsPageView from "./FlightsPageView";
import { inject } from "../../../injector/injector";
import { FlightService } from "../../../services/flight/flight.service";
import { AirportConnectionService } from "../../../services/airportConnection/airportConnection.service";

import { useSelector, useDispatch } from "react-redux";
import { IAppState } from "../../../store/store";
import { Flight } from "../../../models/api/flight.model";
import { AirportConnection } from "../../../models/api/airportConnection.model";

import { getFlightsAction } from "../../../store/actions/flight.action";
import { getAirportConnectionsAction } from "../../../store/actions/airportConnections.action";

const getAirportConnectionRepresentation = (c: AirportConnection) => `${c.originAirport.airportCode} → ${c.destinationAirport.airportCode}`;
const getDate = (arr: [number,number,number,number,number]) => new Date(...arr);

const addLeadingZero = (num: number): string => num >= 10 ? num.toString() : '0' + num;
const getDateToSend = (date: Date): string => {
  const day = date.getDate() + 1;
  const month = date.getMonth() + 1;
  const year = date.getFullYear();
  const hour = date.getHours();
  const minute = date.getMinutes();

  return `${addLeadingZero(day)}-${addLeadingZero(month)}-${year} ${addLeadingZero(hour)}:${addLeadingZero(minute)}`
};

/**
 * Strona zawierająca podgląd lotów
 */
export const FlightsPageTemplate: React.FC<any> = (props) => {

  const flights = useSelector<IAppState, any[]>(x =>
    x.flights.map(f=>({
      ...f,
      connection: f.connection.id ? f.connection.id : '',
      date: f.date ? getDate(f.date) : null,
    }))
  );

  const airportConnections = useSelector<IAppState, AirportConnection[]>(x =>
    x.airportConnections
  );

  const airportConnectionsLookup = airportConnections.reduce((acc: any, c: AirportConnection) => {
    if (c.id) {
      acc[c.id] = getAirportConnectionRepresentation(c);
    }
    return acc;
  }, {});

  const airportConnectionService = inject(AirportConnectionService) as AirportConnectionService;
  const service = inject(FlightService) as FlightService;

  const dispatch = useDispatch();

  useEffect(() => {
    getFlights();
    getAirportConnections();
  }, []);

  const getFlights = async () => {
    const flights = await service.getAllFlights();
    console.log({flights});
    dispatch(getFlightsAction(flights));
  };

  const getAirportConnections = async () => {
    const connections = await airportConnectionService.getAllConnections();
    dispatch(getAirportConnectionsAction(connections));
  };


  const getConnection = (id: any) => airportConnections.find(c => c.id == id);

  const prepareToSend = (data: any): void => {
    console.log({data},"BEFORE PREPARE");
    const connection = getConnection(data.connection);
    if(!connection) throw new Error("Error");

    data.originCode = connection.originAirport.airportCode;
    data.destinationCode = connection.destinationAirport.airportCode;
    data.date = getDateToSend(data.date);
  }


  const addFlight = async (data: any) => {
    prepareToSend(data);
    await service.addFlight(data);
    getFlights();
  };

  const editFlight = async (data: any) => {
    prepareToSend(data);
    await service.editFlight(data);
    getFlights();
  };

  const deleteFlight = async (data: any) => {
    await service.deleteFlight(data);
    getFlights();
  };

  console.log({airportConnectionsLookup, flights});

  return (
    <FlightsPageView
      data={flights}
      additionalData={{ airportConnections: airportConnectionsLookup }}
      add={addFlight}
      edit={editFlight}
      delete={deleteFlight}
    />
  );
};

const FlightsPage = FlightsPageTemplate;

export default FlightsPage;
