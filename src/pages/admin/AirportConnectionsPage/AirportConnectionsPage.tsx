import React, { useEffect } from "react";
import AirportConnectionsPageView from "./AirportConnectionsPageView";
import { inject } from "../../../injector/injector";
import { AirportConnectionService } from "../../../services/airportConnection/airportConnection.service";
import { AirportService } from "../../../services/airport/airport.service";


import { useSelector, useDispatch } from "react-redux";
import { IAppState } from "../../../store/store";

import { Airport } from "../../../models/api/airport.model";
import { AirportConnection } from "../../../models/api/airportConnection.model";

import { getAirportConnectionsAction } from "../../../store/actions/airportConnections.action";
import { getAirportsAction } from "../../../store/actions/airports.action";

/**
 * Strona zawierająca podgląd połączeń lotniczych
 */
export const AirportConnectionsPageTemplate: React.FC = () => {
  const airports = useSelector<IAppState, any>(x => x.airports);

  const airportsLookup = airports.reduce((acc: any, a: Airport) => {
    if (a.id) {
      acc[a.id] = a.airportCode;
    }
    return acc;
  }, {});

  const airportConnections = useSelector<IAppState, any[]>(x =>
    x.airportConnections.map(a => ({
      ...a,
      destinationAirport: a.destinationAirport.id
        ? a.destinationAirport.id
        : "",
      originAirport: a.originAirport.id ? a.originAirport.id : ""
    }))
  );

  const service = inject(AirportConnectionService) as AirportConnectionService;
  const airportService = inject(AirportService) as AirportService;
  
  const dispatch = useDispatch();

  useEffect(() => {
    getAirportConnections();
    getAirports();
  }, []);

  const getAirports = async () => {
    const airports = await airportService.getAirports();
    dispatch(getAirportsAction(airports));
  };

  const getAirportConnections = async () => {
    const connections = await service.getAllConnections();
    dispatch(getAirportConnectionsAction(connections));
  };

  // const getAirportById = (id: any) => airports.find((a: Airport) =>id == a.id);

  const getBoolean = (num: any) => !!num;

  const prepareToSend = (data: any | AirportConnection) => {
    data.active = getBoolean(data.active);
    data.destinationId = parseInt(data.destinationAirport);
    data.originId = parseInt(data.originAirport);
  }

  const addAirportConnection = async (data: any) => {
    prepareToSend(data);
    await service.addConnection(data);
    getAirportConnections();
  };

  // const editAirportConnection = async (data: AirportConnection) => {
  //   prepareToSend(data);
  //   await service.editConnection(data);
  //   getAirportConnections();
  // };

  const deleteAirportConnection = async (data: AirportConnection) => {
    prepareToSend(data);
    await service.deleteConnection(data);
    getAirportConnections();
  };

  console.log({ airportConnections, airportsLookup });

  return (
    <AirportConnectionsPageView
      data={airportConnections}
      additionalData={{ airportsLookup }}
      add={addAirportConnection}
      // edit={editAirportConnection}
      delete={deleteAirportConnection}
    />
  );
};

const AirportConnectionsPage = AirportConnectionsPageTemplate;

export default AirportConnectionsPage;
