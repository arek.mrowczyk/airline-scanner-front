import { AirportConnection } from "../../../models/api/airportConnection.model";

/**
 * Strona z połączeniami lotniczymi
 */
export interface AirportConnectionsPageViewType {
    data: AirportConnection[];
    additionalData: any;
    add: (data: AirportConnection) => void;
    // edit: (newData: AirportConnection, prevData: AirportConnection) => void;
    delete: (data: AirportConnection) => void;
}