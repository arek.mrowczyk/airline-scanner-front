import React from "react";
import AnimatedCardContent from "../../../components/common/AnimatedCard/AnimatedCardContent";
import AnimatedCard from "../../../components/common/AnimatedCard/AnimatedCard";
import { Grid } from "@material-ui/core";
import { AirportConnectionsPageViewType } from "./AirportConnectionsPageView.type";
import ASTable from "../../../components/common/ASTable/ASTable";

const AirportConnectionsPageView: React.FC<AirportConnectionsPageViewType> = props => {

  const booleanLookup = {
    false: "Nie",
    true: "Tak"
  };

  const flightTypeLookup = {
    SHORT: "SHORT",
    LONG: "LONG"
  };

  const columns = [
    { title: "Typ lotu", field: "flightType", lookup: flightTypeLookup },
    { title: "Aktywny", field: "active", lookup: booleanLookup },
    {
      title: "Lotnisko początkowe",
      field: "originAirport",
      lookup: props.additionalData.airportsLookup
    },
    {
      title: "Lotnisko końcowe",
      field: "destinationAirport",
      lookup: props.additionalData.airportsLookup
    }
  ];

  return (
    <Grid container>
      <Grid item xs={2}></Grid>
      <Grid item xs={8}>
        <AnimatedCard>
          <AnimatedCardContent>
            <ASTable
              title="Połączenia lotnicze"
              columns={columns}
              data={props.data}
              editable={{
                onRowAdd: props.add,
                // onRowUpdate: props.edit,
                onRowDelete: props.delete
              }}
            />
          </AnimatedCardContent>
        </AnimatedCard>
      </Grid>
      <Grid item xs={2}></Grid>
    </Grid>
  );
};

export default AirportConnectionsPageView;
