import { Airport } from "../../../models/api/airport.model";

/**
 * Strona z Państwami
 */
export interface AirportsPageViewType {
    data: Airport[];
    additionalData: any;
    add: (data: Airport) => void;
    edit: (newData: Airport, prevData: Airport) => void;
    delete: (data: Airport) => void;
}