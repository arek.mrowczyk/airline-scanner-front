import React, { useEffect } from "react";
import AirportPageView from "./AirportsPageView";
import { inject } from "../../../injector/injector";
import { CountriesService } from "../../../services/countries/countries.service";
import { AirportService } from "../../../services/airport/airport.service";

import { useSelector, useDispatch } from "react-redux";
import { IAppState } from "../../../store/store";
import { Country } from "../../../models/api/country.model";

import { getCountriesAction } from "../../../store/actions/countries.action";
import { getAirportsAction } from "../../../store/actions/airports.action";

/**
 * Strona zawierająca podgląd lotnisk
 */
export const AirportsPageTemplate: React.FC<any> = (props) => {

  const countries = useSelector<IAppState, any>(x =>
    x.countries
  );

  const countriesLookup = countries.reduce((acc: any, c: Country) => {
    if (c.id) {
      acc[c.id] = c.name;
    }
    return acc;
  }, {});

  const airports = useSelector<IAppState, any[]>(x =>
    x.airports.map(a => ({
      ...a,
      country: a.country?.id ? a.country.id : ''
    }))
  );

  const countriesService = inject(CountriesService) as CountriesService;
  const service = inject(AirportService) as AirportService;

  const dispatch = useDispatch();

  useEffect(() => {
    getCountries();
    getAirports();
  }, []);

  const getCountries = async () => {
    const countries = await countriesService.getCountries();
    dispatch(getCountriesAction(countries));
  };

  const getAirports = async () => {
    const airports = await service.getAirports();
    dispatch(getAirportsAction(airports));
  };

  const getCountryById = (id: any) => countries.find((c: Country) =>id == c.id);

  const addAirport = async (data: any) => {
    data.country = getCountryById(data.country);
    await service.addAirport(data);
    getAirports();
  };

  const editAirport = async (data: any) => {
    data.country = getCountryById(data.country);
    await service.editAirport(data);
    getAirports();
  };

  const deleteAirport = async (data: any) => {
    await service.deleteAirport(data);
    getAirports();
  };

  return (
    <AirportPageView
      data={airports}
      additionalData={{ countries: countriesLookup }}
      add={addAirport}
      edit={editAirport}
      delete={deleteAirport}
    />
  );
};

const AirportsPage = AirportsPageTemplate;

export default AirportsPage;
