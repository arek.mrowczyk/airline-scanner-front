import React from "react";
import AnimatedCardContent from "../../../components/common/AnimatedCard/AnimatedCardContent";
import AnimatedCard from "../../../components/common/AnimatedCard/AnimatedCard";
import { Grid } from "@material-ui/core";
import { AirportsPageViewType } from "./AirportsPageView.type";
import ASTable from "../../../components/common/ASTable/ASTable";
import { Country } from "../../../models/api/country.model";

const CountriesPageView: React.FC<AirportsPageViewType> = (props) => {

  const columns = [
    { title: "Nazwa", field: "airportName" },
    { title: "Kod", field: "airportCode" },
    { title: "Miasto", field: "cityName" },
    { title: "Kod miasta", field: "cityCode" },
    {
      title: 'Kraj',
      field: 'country',
      lookup: props.additionalData.countries,
    },

  ];

  return (
    <Grid container>
      <Grid item xs={2}></Grid>
      <Grid item xs={8}>
        <AnimatedCard>
          <AnimatedCardContent>
            <ASTable
              title="Lotniska"
              columns={columns}
              data={props.data}
              editable={{
                onRowAdd: props.add,
                onRowUpdate: props.edit,
                onRowDelete: props.delete,
              }}
            />
          </AnimatedCardContent>
        </AnimatedCard>
      </Grid>
      <Grid item xs={2}></Grid>
    </Grid>
  );
};

export default CountriesPageView;
