import React from "react";
import AnimatedCardContent from "../../../components/common/AnimatedCard/AnimatedCardContent";
import AnimatedCard from "../../../components/common/AnimatedCard/AnimatedCard";
import { Grid } from "@material-ui/core";
import { CountriesPageViewType } from "./CountriesPageView.type";
import ASTable from "../../../components/common/ASTable/ASTable";

const CountriesPageView: React.FC<CountriesPageViewType> = (props) => {

  const columns = [
    { title: "Nazwa", field: "name" },
    { title: "Kod", field: "code" },
  ];

  return (
    <Grid container>
      <Grid item xs={2}></Grid>
      <Grid item xs={8}>
        <AnimatedCard>
          <AnimatedCardContent>
            <ASTable
              title="Państwa"
              columns={columns}
              data={props.data}
              editable={{
                onRowAdd: props.add,
                onRowUpdate: props.edit,
                onRowDelete: props.delete,
              }}
            />
          </AnimatedCardContent>
        </AnimatedCard>
      </Grid>
      <Grid item xs={2}></Grid>
    </Grid>
  );
};

export default CountriesPageView;
