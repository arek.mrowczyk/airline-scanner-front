import { Country } from "../../../models/api/country.model";

/**
 * Strona z Państwami
 */
export interface CountriesPageViewType {
    data: Country[];
    add: (data: Country) => void;
    edit: (newData: Country, prevData: Country) => void;
    delete: (data: Country) => void;
}