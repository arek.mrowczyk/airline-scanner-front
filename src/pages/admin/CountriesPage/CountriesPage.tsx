import React, { useEffect } from "react";
import CountriesPageView from "./CountriesPageView";
import { inject } from "../../../injector/injector";
import { CountriesService } from "../../../services/countries/countries.service";

import { useSelector, useDispatch } from "react-redux";
import { IAppState } from "../../../store/store";
import { Country } from "../../../models/api/country.model";

import { getCountriesAction } from '../../../store/actions/countries.action';

/**
 * Strona zawierająca podgląd krajów
 */
export const CountriesPageTemplate: React.FC = () => {
  const countries = useSelector<IAppState, Country[]>(x => x.countries);

  const service = inject(CountriesService) as CountriesService;
  const dispatch = useDispatch();

  useEffect(() => {
    getCountries();
  }, []);

  const getCountries = async () => {
    const countries = await service.getCountries();
    dispatch(getCountriesAction(countries));
  }

  const addCountry = async (newCountry:any) => {
    await service.addCountry(newCountry);
    getCountries();
  };

  const editCountry = async (newData: Country) => {
    await service.editCountry(newData);
    getCountries();
  }

  const deleteCountry = async (data: Country) => {
    await service.deleteCountry(data);
    getCountries();
  }

  return <CountriesPageView data={countries} add={addCountry} edit={editCountry} delete={deleteCountry}/>;
};

const ContriesPage = CountriesPageTemplate;

export default ContriesPage;
