import { User } from "../../models/api/user.model";

/**
 * Strona z profilem użytkownika
 */
export interface UserSettingsPageViewType {
    userData: User | undefined;
    rechargeUserBalance: (value: number) => any;
}