import React from "react";
import UserSettingsPageView from "./UserSettingsPageView";

import { useSelector, useDispatch } from "react-redux";

import { inject } from "../../injector/injector";
import { UserDataService } from "../../services/userData/userData.service";

import { IAppState } from "../../store/store";
import { User } from "../../models/api/user.model";

import { updateUserDataAction } from "../../store/actions/authData.action";

/**
 * Strona zawierająca podgląd profilu użytkownika
 */
export const UserSettingsPageTemplate: React.FC = () => {
  const userData = useSelector<IAppState, User | undefined>(
    x => x.authData.user
  );

  const dispatch = useDispatch();

  const service = inject(UserDataService) as UserDataService;

  const getUserData = async () => {
    if (userData && userData.login) {
      const result = (await service.getUserByLogin(userData.login)) as User;
      dispatch(updateUserDataAction(result));
    }
  };

  const rechargeUserBalance = async (value: number) => {
    if (userData && userData.login) {
      const data = {
        login: userData.login,
        balance: value
      };
      const response = await service.addToBalance(data);
      getUserData();
      return response === null;
    }
  };

  return (
    <UserSettingsPageView
      userData={userData}
      rechargeUserBalance={rechargeUserBalance}
    />
  );
};

const UserSettingsPage = UserSettingsPageTemplate;

export default UserSettingsPage;
