import React, { CSSProperties, useState } from "react";
import AnimatedCardContent from "../../components/common/AnimatedCard/AnimatedCardContent";
import AnimatedCard from "../../components/common/AnimatedCard/AnimatedCard";
import { Grid } from "@material-ui/core";
import { UserSettingsPageViewType } from "./UserSettingsPageView.type";
import { Typography, Avatar, Button } from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import { dateConverterFromNumberArray } from "../../utils/dateConverter";
import ASTextField from "../../components/common/ASTextField/ASTextField";
import { ASPopup } from "../../components/common/ASPopup/ASPopup";

const avatarStyle: CSSProperties = {
  height: "60px",
  width: "60px",
  margin: "2rem",
  transform: "scale(2)",
  float: "right",
  marginRight: "3rem"
};

const dividerStyle: CSSProperties = {
  marginTop: "1.5rem",
  marginBottom: "1.5rem"
};

const userDataStyle: CSSProperties = {
  lineHeight: "1.7"
};

const UserSettingsPageView: React.FC<UserSettingsPageViewType> = props => {
  const [rechargeAmount, setRechargeAmount] = useState("0");
  const [rechargeVisibility, setRechargeVisible] = useState(false);
  const [show, setShow] = useState(false);

  const closePopup = () => {
    setShow(false);
  }

  const getFirstLettersOfUser = (): string => {
    if (props.userData) {
      const { userData: user } = props;
      return user.firstName[0] + user.lastName[0];
    }
    return "";
  };

  const getMoney = (amount: number): string => {
    let res = amount.toFixed(2);
    return `${res}zł`;
  };


  const showRechargeAccount = () => {
    setRechargeVisible(true);
  };

  const rechargeAccount = async() => {
    // console.log("Do smth", { rechargeAmount });
    const rechargeAmountFloat = parseFloat(rechargeAmount)
    const res = await props.rechargeUserBalance(rechargeAmountFloat);
    setRechargeVisible(false);
    setShow(res);
  };

  const renderUserData = () => {
    if (!props.userData) return null;
    return (
      <Grid container>
        <Grid item xs={3}>
          <Avatar style={avatarStyle}>{getFirstLettersOfUser()}</Avatar>
        </Grid>
        <Grid item xs={9}>
          <Typography variant="body1" style={userDataStyle}>
            Imię: {user.firstName}
          </Typography>
          <Typography variant="body1" style={userDataStyle}>
            Nazwisko: {user.lastName}
          </Typography>
          <Typography variant="body1" style={userDataStyle}>
            Data urodzenia: {dateConverterFromNumberArray(user.dateOfBirth)}
          </Typography>
          <Typography variant="body1" style={userDataStyle}>
            Stan konta: {getMoney(user.balance)}
          </Typography>
        </Grid>
      </Grid>
    );
  };

  if (!props.userData) {
    return (
      <Typography variant="h4" gutterBottom align="center">
        Nie znaleziono użytkownika
      </Typography>
    );
  }

  const { userData: user } = props;

  return (
    <>
    <Grid container>
      <Grid item xs={2}></Grid>
      <Grid item xs={8}>
        <AnimatedCard>
          <AnimatedCardContent>
            <Typography variant="h4" gutterBottom align="center">
              {user.login}
            </Typography>
            <Divider variant="middle" style={dividerStyle} />
            {renderUserData()}
            <Divider variant="middle" style={dividerStyle} />
            <Grid container>
              <Grid item xs={3}></Grid>
              <Grid item xs={9}>
                {!rechargeVisibility ? (
                  <Button
                    variant="outlined"
                    color="primary"
                    size="small"
                    onClick={showRechargeAccount}
                  >
                    Doładuj konto
                  </Button>
                ) : (
                  <React.Fragment>
                    <ASTextField
                      value={rechargeAmount}
                      label={"Ilość"}
                      type="number"
                      onChange={setRechargeAmount}
                    />
                    <Button
                      variant="outlined"
                      color="primary"
                      size="small"
                      onClick={rechargeAccount}
                    >
                      Doładuj
                    </Button>
                  </React.Fragment>
                )}
              </Grid>
            </Grid>
          </AnimatedCardContent>
        </AnimatedCard>
      </Grid>
      <Grid item xs={2}></Grid>
    </Grid>
    <ASPopup show={show} 
               duration={3000} 
               onClose={closePopup} 
               severity="success"
               variant="filled"
               origin={{ horizontal: "center", vertical: "top" }} 
               text="Pomyślnie doładowano konto" />
    </>
  );
};

export default UserSettingsPageView;
