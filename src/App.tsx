import React from 'react';
import './App.css';
import HeaderComponentView from './components/HeaderComponent/HeaderComponentView';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { blue } from '@material-ui/core/colors';
import { Box } from '@material-ui/core';
import { Provider } from "react-redux";
import { Store } from './store/store';
import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom";
import AuthRoute from './components/common/AuthRoute/AuthRouteComponent';
import { Routing } from './route/routing';

function App() {

  const theme = createMuiTheme({
    palette: {
      primary: {
        main: blue['500'],

      },
      secondary: {
        main: '#f44336',
      }
    },
  });

  return (
    <Provider store={Store}>
      <div style={{
        backgroundImage: `linear-gradient(to bottom, transparent 10%, white 60%), url('test.jpg')`, height: '100vh'
      }}>
        <ThemeProvider theme={theme}>
          <Router>
            <HeaderComponentView />
            <Box marginTop={2}>
            {
              Routing.map(x => (
                x.auth ? 
                <AuthRoute key={x.path} path={x.path} strict={x.strict} exact={x.exact}>{x.render}</AuthRoute>:
                <Route key={x.path} path={x.path} strict={x.strict} exact={x.exact}>{x.render}</Route>
              ))
            }
            </Box>
          </Router>
        </ThemeProvider>
      </div>
    </Provider>
  );
}

export default App;
