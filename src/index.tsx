import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { LoggerService } from './services/logger/logger.service';
import { inject } from './injector/injector';
import { doForBuildType } from './utils/doForBuildType';

const logger = inject<LoggerService>(LoggerService);

doForBuildType(
    () => logger.info('Uruchomiono wersję deweloperską'),
    () => logger.info('Uruchomiono wersję dewelopersko-produkcyjną')
);

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
