import { FormControl } from "./formControl.model";

/**
 * Model formularza logowania
 */
export interface LoginForm {
    fields: FormControl[];
    error?: string;
}