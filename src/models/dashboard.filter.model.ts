import { Country } from "./api/country.model";
import { Airport } from "./api/airport.model";

export interface DashboardFilterState {
    countries: Country[];
    airportsFrom: Airport[];
    airportFrom: Airport | undefined;
    airportsTo: Airport[];
    airportTo: Airport | undefined;
    countryFrom: Country | undefined;
    countryTo: Country | undefined;
    personCount: number;
}