import { FormControl } from "./formControl.model";

/**
 * Model formularza rejestracyjnego
 */
export interface RegisterForm {
    fields: FormControl[];
}