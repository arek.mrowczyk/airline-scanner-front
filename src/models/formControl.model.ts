import { FormValue } from "./formValue.model";
import { ValidationError } from "./validationError.model";

/**
 * Model kontrolki formularza
 */
export interface FormControl {
    fieldName: string;
    fieldValue: FormValue;
    error: ValidationError;
    touched: boolean;
}