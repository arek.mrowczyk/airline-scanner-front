
/**
 * Model błędu walidacji o opcjonalnym type generycznym dla pola info
 */
export interface ValidationError<T = {}> {
    message: string;
    info: T;
}