import { Country } from './country.model';

/**
 * Klasa lotniska, która jest reprezentacją lotniska po stronie backend'u
 */
export interface Airport {
    id: number;
    airportCode: string;
    airportName: string;
    cityName: string;
    cityCode: string;
    country: Country;
}