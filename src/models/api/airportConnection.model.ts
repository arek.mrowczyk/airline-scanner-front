import { Airport } from "./airport.model";

export interface AirportConnection {
    id: number;
    flightType: string;
    active: boolean;
    destinationAirport: Airport;
    originAirport: Airport;
}