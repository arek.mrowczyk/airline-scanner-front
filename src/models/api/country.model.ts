
/**
 * Klasa państwa, która jest reprezentacją państwa po stronie backend'u
 */
export interface Country {
    id: number;
    name: string;
    code: string;
}