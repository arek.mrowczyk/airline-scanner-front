
/**
 * Klasa formularza logowania, która jest reprezentacją formularza logowania po stronie backend'u
 */
export interface LoginApiForm {
    login: string;
    password: string;
}