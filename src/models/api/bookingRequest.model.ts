import { Person } from "./person.model";

export interface BookingRequest {
    login: string;
    flightId: number;
    people: Person[];
}