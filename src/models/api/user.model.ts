
/**
 * Klasa użytkownika, która jest reprezentacją użytkownika po stronie backend'u
 */

export interface Authority{
    authority: string;
}

export interface User {
    id: number;
    login: string;
    password: string;
    firstName: string;
    lastName: string;
    dateOfBirth: number[];
    balance: number;
    autorities?: string;
    creationDate: number[];
    enabled: boolean;
    username: string;
    authorities: Authority[];
    accountNonExpired: boolean;
    accountNonLocked: boolean;
    credentialsNonExpired: boolean;
    isAdmin?: boolean;
}