
/**
 * Klasa formularza rejestracji, która jest reprezentacją formularza rejestracji po stronie backend'u
 */
export interface RegisterApiForm {
    login: string;
    password: string;
    firstName: string;
    lastName: string;
    dateOfBirth: string;
}