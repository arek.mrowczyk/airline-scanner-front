import { AirportConnection } from "./airportConnection.model";
import { Seat } from "./seat.model";

export interface Flight {
    id: number;
    aircraftModel: string;
    fare: number;
    availableSeats: number;
    date: [number,number,number,number,number];
    connection: AirportConnection;
    seat: Seat;
}