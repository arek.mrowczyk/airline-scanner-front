import { User } from "./user.model";
import { Flight } from "./flight.model";

export interface Booking {
    id: number;
    price: number;
    firstName: string;
    lastName: string;
    email: string;
    bookingNumber: string;
    account: User;
    flight: Flight;
}