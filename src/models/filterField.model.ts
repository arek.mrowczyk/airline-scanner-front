import { KeyValue } from "./keyValue.model";

export interface FilterField {
    title: string;
    fieldName: string;
    type: 'select' | 'textbox';
    items?: KeyValue<string, string>[];
    fieldValue: string;
}