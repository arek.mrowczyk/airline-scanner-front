import { User } from "./api/user.model";

/**
 * Model danych autentykacji
 */
export interface AuthData {
    isLogged: boolean;
    user?: User;
}