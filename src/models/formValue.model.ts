
/**
 * Model dla wartości z formularza
 */
export type FormValue = string | boolean;