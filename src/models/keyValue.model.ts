
/**
 * Model klucz-wartość
 */
export interface KeyValue<TKey, TValue> {
    key: TKey;
    value: TValue;
}