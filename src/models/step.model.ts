export interface Step {
    label: string;
    component: JSX.Element;
}