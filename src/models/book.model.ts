import { Flight } from "./api/flight.model";
import { Person } from "./api/person.model";

export interface BookState {
    flight: Flight;
    currentStep: number;
    persons: Person[];
    seats: any[];
}