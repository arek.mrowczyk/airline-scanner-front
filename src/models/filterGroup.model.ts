import { FilterField } from "./filterField.model";

export interface FilterGroup {
    items: FilterField[];
}