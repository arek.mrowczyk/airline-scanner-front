import { TableData } from "./tableData.model";

export interface DashboardTableState {
    table: TableData;
}