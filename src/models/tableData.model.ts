export interface TableData {
    columns: TableColumn[];
    rows: TableRowContent[];
}

export interface TableColumn {
    name: string;
}

export interface TableRowContent {
    data: any;
    actions?: TableRowActions;
}

export interface TableRowActions {
    components: JSX.Element[];
}