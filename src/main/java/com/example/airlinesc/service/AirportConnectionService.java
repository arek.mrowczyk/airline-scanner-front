package com.example.airlinesc.service;

import com.example.airlinesc.model.Airport;
import com.example.airlinesc.model.AirportConnection;
import com.example.airlinesc.repository.AirportConnectionRepository;
import com.example.airlinesc.repository.AirportRepository;
import com.example.airlinesc.request.BidirectionalConnectionCodeRequest;
import com.example.airlinesc.request.BidirectionalConnectionIdRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirportConnectionService {

    private final AirportConnectionRepository airportConnectionRepository;
    private final AirportRepository airportRepository;

    @Autowired
    public AirportConnectionService(AirportConnectionRepository airportConnectionRepository, AirportRepository airportRepository) {
        this.airportConnectionRepository = airportConnectionRepository;
        this.airportRepository = airportRepository;
    }

    public List<AirportConnection> getAll(){
        return airportConnectionRepository.findAll();
    }

    public AirportConnection getById(Long id){
        return airportConnectionRepository.findAirportConnectionById(id);
    }

    public AirportConnection getByOriginCodeAndDestinationCode(String originCode, String destinationCode){
        return airportConnectionRepository.findByOriginAirport_AirportCodeAndDestinationAirport_AirportCode(originCode, destinationCode);
    }

    public List<AirportConnection> getAllByOrigin(String origin){
        return airportConnectionRepository.findAllByOriginAirport_AirportCodeOrderByDestinationAirport_airportCode(origin);
    }

    public List<AirportConnection> getAllByDestination(String destination){
        return airportConnectionRepository.findAllByDestinationAirport_AirportCodeOrderByOriginAirport_airportCode(destination);
    }

    public void bidirectionalConnectionOfIds(BidirectionalConnectionIdRequest request){
        Airport origin = airportRepository.findAirportById(request.getOriginId());
        Airport destination = airportRepository.findAirportById(request.getDestinationId());

        AirportConnection airportConnection = new AirportConnection();
        airportConnection.setOriginAirport(origin);
        airportConnection.setDestinationAirport(destination);
        airportConnection.setActive(request.isActive());
        airportConnection.setFlightType(request.getFlightType());
        airportConnectionRepository.save(airportConnection);

        AirportConnection airportConnectionSecondSide = new AirportConnection();
        airportConnectionSecondSide.setOriginAirport(destination);
        airportConnectionSecondSide.setDestinationAirport(origin);
        airportConnectionSecondSide.setActive(request.isActive());
        airportConnectionSecondSide.setFlightType(request.getFlightType());
        airportConnectionRepository.save(airportConnectionSecondSide);
    }

    public void bidirectionalConnectionOfCodes(BidirectionalConnectionCodeRequest request){
        Airport origin = airportRepository.findAirportByAirportCode(request.getOriginCode());
        Airport destination = airportRepository.findAirportByAirportCode(request.getDestinationCode());

        AirportConnection airportConnection = new AirportConnection();
        airportConnection.setOriginAirport(origin);
        airportConnection.setDestinationAirport(destination);
        airportConnection.setActive(request.isActive());
        airportConnection.setFlightType(request.getFlightType());
        airportConnectionRepository.save(airportConnection);

        AirportConnection airportConnectionSecondSide = new AirportConnection();
        airportConnectionSecondSide.setOriginAirport(destination);
        airportConnectionSecondSide.setDestinationAirport(origin);
        airportConnectionSecondSide.setActive(request.isActive());
        airportConnectionSecondSide.setFlightType(request.getFlightType());
        airportConnectionRepository.save(airportConnectionSecondSide);
    }

    public void deleteById(Long id) {
        airportConnectionRepository.deleteById(id);
    }

}
