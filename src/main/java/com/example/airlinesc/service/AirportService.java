package com.example.airlinesc.service;

import com.example.airlinesc.model.Airport;
import com.example.airlinesc.repository.AirportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AirportService {

    private final AirportRepository airportRepository;

    @Autowired
    public AirportService(AirportRepository airportRepository) {
        this.airportRepository = airportRepository;
    }

    public List<Airport> getAll(){
        return airportRepository.findAll();
    }

    public Airport getById(Long id){
        return airportRepository.findAirportById(id);
    }

    public Airport getByAirportCode(String code){
        return airportRepository.findAirportByAirportCode(code);
    }

    public List<Airport> getAllByCountry(String country){
        return airportRepository.findAllByCountry_NameOrderByAirportCode(country);
    }

    public void save(Airport airport){
        airportRepository.save(airport);
    }

    public void deleteById(Long id) {
        airportRepository.deleteById(id);
    }
}
