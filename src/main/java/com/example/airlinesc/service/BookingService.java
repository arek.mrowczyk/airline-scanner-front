package com.example.airlinesc.service;

import com.example.airlinesc.model.Account;
import com.example.airlinesc.model.Booking;
import com.example.airlinesc.model.Flight;
import com.example.airlinesc.repository.AccountRepository;
import com.example.airlinesc.repository.BookingRepository;
import com.example.airlinesc.repository.FlightRepository;
import com.example.airlinesc.request.BookingRequest;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gadzik on 20.04.20.
 */
@Service
public class BookingService {

    private final BookingRepository bookingRepository;
    private final AccountRepository accountRepository;
    private final FlightRepository flightRepository;
    private final PaymentService paymentService;

    @Autowired
    public BookingService(BookingRepository bookingRepository, AccountRepository accountRepository, FlightRepository flightRepository, PaymentService paymentService) {
        this.bookingRepository = bookingRepository;
        this.accountRepository = accountRepository;
        this.flightRepository = flightRepository;
        this.paymentService = paymentService;
    }

    public List<Booking> getAll() {
        return bookingRepository.findByOrderByFlight_Date();
    }

    public Booking getById(Long id) {
        return bookingRepository.findBookingById(id);
    }

    public List<Booking> getAllByLogin(String login) {
        return bookingRepository.findAllByAccount_LoginOrderByFlight_Date(login);
    }

    public List<Booking> save(BookingRequest request) {
        Flight flight = flightRepository.findFlightById(request.getFlightId());
        Account account = accountRepository.findByLogin(request.getLogin());

        BigDecimal totalPrice = flight.getFare().multiply(new BigDecimal(request.getPeople().size()));
        paymentService.payment(request.getLogin(), totalPrice);

        List<Booking> bookings = new ArrayList<>();
        request.getPeople().forEach(person -> {
            Booking booking = new Booking();
            booking.setAccount(account);
            booking.setFlight(flight);
            booking.setPrice(flight.getFare());
            booking.setFirstName(person.getFirstName());
            booking.setLastName(person.getFirstName());
            booking.setEmail(person.getEmail());
            String reservationNumber = RandomStringUtils.randomAlphabetic(1).toUpperCase() + RandomStringUtils.randomNumeric(5);
            booking.setBookingNumber(reservationNumber);
            bookings.add(bookingRepository.save(booking));
        });
        return bookings;
    }
}
