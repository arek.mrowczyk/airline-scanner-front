package com.example.airlinesc.service;

import com.example.airlinesc.model.Account;
import com.example.airlinesc.repository.AccountRepository;
import com.example.airlinesc.request.AccountRequest;
import com.example.airlinesc.request.BalanceRequest;
import com.example.airlinesc.util.DateConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by gadzik on 15.03.20.
 */
@Service
public class AccountService {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public List<Account> getAll(){
        return accountRepository.findAll();
    }

    public Account getByLogin(String login){
        return accountRepository.findByLogin(login);
    }

    public Account getById(Long id){
        return accountRepository.findAccountById(id);
    }

    public List<Account> getAllByLastName(String lastName){
        return accountRepository.findAllByLastName(lastName);
    }

    public void save(AccountRequest request){
        Account existingAccount = accountRepository.findByLogin(request.getLogin());
        if(existingAccount != null) {
            throw new IllegalArgumentException();
        }
        Account account = new Account();
        account.setLogin(request.getLogin());
        account.setPassword(request.getPassword());
        account.setFirstName(request.getFirstName());
        account.setLastName(request.getLastName());
        account.setDateOfBirth(DateConverter.toDate(request.getDateOfBirth()));
        accountRepository.save(account);
    }

    public void save(Account account){
        accountRepository.save(account);
    }

    public void balanceUpdate(BalanceRequest request){
        Account account = accountRepository.findByLogin(request.getLogin());
        if(account == null){
            throw new IllegalArgumentException();
        }
        account.setBalance(account.getBalance().add(request.getBalance()));
        accountRepository.save(account);
    }

    public void deleteById(Long id) {
        accountRepository.deleteById(id);
    }
}
