package com.example.airlinesc.service;

import com.example.airlinesc.model.Account;
import com.example.airlinesc.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.logging.Logger;

/**
 * Created by gadzik on 21.04.20.
 */
@Service
public class PaymentService {

    private static final Logger LOGGER = Logger.getLogger("PaymentService.class");

    private final AccountRepository accountRepository;

    @Autowired
    public PaymentService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public void payment(String login, BigDecimal price){
        LOGGER.info("paymentSerice - payment: " + price);
        Account account = accountRepository.findByLogin(login);
        Account airlines = accountRepository.findByLogin("airlines");
        if(account.getBalance().compareTo(price) < 0) {
            throw new IllegalArgumentException("You haven't enough money to buy ticket");
        }
        LOGGER.info("payment acc: " + account.getBalance());
        LOGGER.info("payment airlines: " + airlines.getBalance());
        account.setBalance(account.getBalance().subtract(price));
        airlines.setBalance(airlines.getBalance().add(price));
        LOGGER.info("after payment acc: " + account.getBalance());
        LOGGER.info("after payment airlines: " + airlines.getBalance());
        accountRepository.save(account);
        accountRepository.save(airlines);
    }
}
