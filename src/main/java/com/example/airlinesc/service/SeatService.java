package com.example.airlinesc.service;

import com.example.airlinesc.model.Seat;
import com.example.airlinesc.repository.SeatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by gadzik on 07.05.20.
 */
@Service
public class SeatService {

    private final SeatRepository seatRepository;

    @Autowired
    public SeatService(SeatRepository seatRepository) {
        this.seatRepository = seatRepository;
    }

    public Seat save(Seat seat) {
        return seatRepository.save(seat);
    }

    public Seat getById(Long id) {
        return seatRepository.getById(id);
    }
}
