package com.example.airlinesc.service;

import com.example.airlinesc.model.AirportConnection;
import com.example.airlinesc.model.Flight;
import com.example.airlinesc.repository.FlightRepository;
import com.example.airlinesc.request.FlightRequest;
import com.example.airlinesc.request.FlightRequestBatch;
import com.example.airlinesc.util.DateConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by gadzik on 15.03.20.
 */
@Service
public class FlightService {

    private final FlightRepository flightRepository;
    private final AirportConnectionService airportConnectionService;

    @Autowired
    public FlightService(FlightRepository flightRepository, AirportConnectionService airportConnectionService) {
        this.flightRepository = flightRepository;
        this.airportConnectionService = airportConnectionService;
    }


    public List<Flight> getAll(){
        return flightRepository.findAll();
    }

    public List<Flight> getAllByOriginCode(String code){
        return flightRepository.findAllByConnection_OriginAirport_AirportCodeOrderByDate(code);
    }

    public List<Flight> getAllByDestinationCode(String code){
        return flightRepository.findAllByConnection_DestinationAirport_AirportCodeOrderByDate(code);
    }

    public List<Flight> getAllByOriginCodeAndDestinationCode(String origin, String destination){
        return flightRepository.findAllByConnection_OriginAirport_AirportCodeAndConnection_DestinationAirport_AirportCodeOrderByDate(origin, destination);
    }

    public List<Flight> getAllByOriginCodeBeforeDate(String code, LocalDateTime date){
        return flightRepository.findAllByConnection_OriginAirport_AirportCodeAndDateBeforeOrderByDate(code, date);
    }

    public List<Flight> getAllByDestinationCodeBeforeDate(String code, LocalDateTime date){
        return flightRepository.findAllByConnection_DestinationAirport_AirportCodeAndDateBeforeOrderByDate(code, date);
    }

    public List<Flight> getAllByOriginCodeAfterDate(String code, LocalDateTime date){
        return flightRepository.findAllByConnection_OriginAirport_AirportCodeAndDateAfterOrderByDate(code, date);
    }

    public List<Flight> getAllByDestinationCodeAfterDate(String code, LocalDateTime date){
        return flightRepository.findAllByConnection_DestinationAirport_AirportCodeAndDateAfterOrderByDate(code, date);
    }

    public List<Flight> getAllByOriginCodeBetweenDate(String code, LocalDateTime from, LocalDateTime to){
        return flightRepository.findAllByConnection_OriginAirport_AirportCodeAndDateBetweenOrderByDate(code, from, to);
    }

    public List<Flight> getAllByDestinationCodeBetweenDate(String code, LocalDateTime from, LocalDateTime to){
        return flightRepository.findAllByConnection_DestinationAirport_AirportCodeAndDateBetweenOrderByDate(code, from, to);
    }

    public List<Flight> getAllByOriginCodeAndDestinationCodeBetweenDate(String origin, String destination, LocalDateTime from, LocalDateTime to){
        return flightRepository
                .findAllByConnection_OriginAirport_AirportCodeAndConnection_DestinationAirport_AirportCodeAndDateBetweenOrderByDate(
                        origin, destination, from, to);
    }

    public void save(FlightRequest request){
        Flight flight = new Flight();
        flight.setAircraftModel(request.getAircraftModel());
        flight.setFare(request.getFare());
        flight.setAvailableSeats(request.getAvailableSeats());
        flight.setDate(DateConverter.toDateTime(request.getDate()));
        AirportConnection airportConnection = airportConnectionService
                .getByOriginCodeAndDestinationCode(request.getOriginCode(), request.getDestinationCode());
        flight.setConnection(airportConnection);
        flightRepository.save(flight);
    }

    public void saveForBatch(FlightRequestBatch request){
        Flight flight = new Flight();
        flight.setAircraftModel(request.getAircraftModel());
        flight.setFare(request.getFare());
        flight.setAvailableSeats(request.getAvailableSeats());
        flight.setSeat(request.getSeat());
        flight.setDate(request.getDate());
        AirportConnection airportConnection = airportConnectionService
                .getByOriginCodeAndDestinationCode(request.getOriginCode(), request.getDestinationCode());
        flight.setConnection(airportConnection);
        flightRepository.save(flight);
    }

    public void deleteById(Long id) {
        flightRepository.deleteById(id);
    }
}
