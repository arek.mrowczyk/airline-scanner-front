package com.example.airlinesc.service;

import com.example.airlinesc.model.Country;
import com.example.airlinesc.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryService {

    private final CountryRepository countryRepository;

    @Autowired
    public CountryService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    public List<Country> getAll(){
        return countryRepository.findAll();
    }

    public Country getByCode(String code){
        return countryRepository.findByCode(code);
    }

    public Country getByName(String name){
        return countryRepository.findByName(name);
    }

    public void save(Country country){
        countryRepository.save(country);
    }

    public void deleteById(Long id){
        countryRepository.deleteById(id);
    }
}
