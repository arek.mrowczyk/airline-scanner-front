package com.example.airlinesc.service;

import com.example.airlinesc.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

/**
 * Created by gadzik on 31.03.20.
 */
@Service
public class AuthService implements UserDetailsService {

    private static final Logger LOGGER = Logger.getLogger("AuthService.class");


    private final AccountRepository accountRepository;

    @Autowired
    public AuthService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public void authenticate(UserDetails user) {
        SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword(), user.getAuthorities()));

        LOGGER.info("USER: " + SecurityContextHolder.getContext().getAuthentication().getName() + " sucessfully logged in with AUTHORITIES: " +
                SecurityContextHolder.getContext().getAuthentication().getAuthorities().toString());
    }

    public void unauthenticate() {
        SecurityContextHolder.clearContext();
        LOGGER.info("You were sucessfully logged out");
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        return accountRepository.findByLogin(login);
    }

}
