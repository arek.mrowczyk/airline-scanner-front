package com.example.airlinesc.repository;

import com.example.airlinesc.model.Airport;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AirportRepository extends CrudRepository<Airport, Long> {
    List<Airport> findAll();
    Airport findAirportById(Long id);
    Airport findAirportByAirportCode(String code);
    List<Airport> findAllByCountry_NameOrderByAirportCode(String countryName);
    Airport save(Airport a);
    void deleteById(Long id);
}
