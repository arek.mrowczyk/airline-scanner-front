package com.example.airlinesc.repository;

import com.example.airlinesc.model.AirportConnection;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AirportConnectionRepository extends CrudRepository<AirportConnection, Long> {
    List<AirportConnection> findAll();
    AirportConnection findAirportConnectionById(Long id);
    List<AirportConnection> findAllByOriginAirport_AirportCodeOrderByDestinationAirport_airportCode(String originCode);
    List<AirportConnection> findAllByDestinationAirport_AirportCodeOrderByOriginAirport_airportCode(String destinationCode);
    AirportConnection findByOriginAirport_AirportCodeAndDestinationAirport_AirportCode(String originCode, String destinationCode);
    AirportConnection save(AirportConnection ac);
    void deleteById(Long id);
}
