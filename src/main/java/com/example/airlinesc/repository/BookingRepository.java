package com.example.airlinesc.repository;

import com.example.airlinesc.model.Booking;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by gadzik on 20.04.20.
 */
public interface BookingRepository extends CrudRepository<Booking, Long> {
    List<Booking> findByOrderByFlight_Date();
    Booking findBookingById(Long id);
    List<Booking> findAllByAccount_LoginOrderByFlight_Date(String login);
    Booking save(Booking booking);
    void deleteById(Long id);
}
