package com.example.airlinesc.repository;

import com.example.airlinesc.model.Flight;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by gadzik on 15.03.20.
 */
@Repository
public interface FlightRepository extends CrudRepository<Flight, Long> {

    List<Flight> findAll();
    Flight findFlightById(Long id);

    List<Flight> findAllByConnection_OriginAirport_AirportCodeOrderByDate(String originCode);
    List<Flight> findAllByConnection_DestinationAirport_AirportCodeOrderByDate(String destinationCode);
    List<Flight> findAllByConnection_OriginAirport_AirportCodeAndConnection_DestinationAirport_AirportCodeOrderByDate(String originCode, String destinationCode);

    List<Flight> findAllByConnection_OriginAirport_AirportCodeAndDateBeforeOrderByDate(String originCode, LocalDateTime date);
    List<Flight> findAllByConnection_DestinationAirport_AirportCodeAndDateBeforeOrderByDate(String destinationCode, LocalDateTime date);

    List<Flight> findAllByConnection_OriginAirport_AirportCodeAndDateAfterOrderByDate(String originCode, LocalDateTime date);
    List<Flight> findAllByConnection_DestinationAirport_AirportCodeAndDateAfterOrderByDate(String destinationCode, LocalDateTime date);

    List<Flight> findAllByConnection_OriginAirport_AirportCodeAndDateBetweenOrderByDate(String originCode, LocalDateTime from, LocalDateTime to);
    List<Flight> findAllByConnection_DestinationAirport_AirportCodeAndDateBetweenOrderByDate(String destinationCode, LocalDateTime from, LocalDateTime to);
    List<Flight> findAllByConnection_OriginAirport_AirportCodeAndConnection_DestinationAirport_AirportCodeAndDateBetweenOrderByDate
            (String originCode, String destinationCode, LocalDateTime from, LocalDateTime to);

    Flight save(Flight flight);
    void deleteById(Long id);
}
