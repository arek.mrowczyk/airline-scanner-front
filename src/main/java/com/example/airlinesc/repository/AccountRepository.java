package com.example.airlinesc.repository;

import com.example.airlinesc.model.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by gadzik on 15.03.20.
 */
@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {
    List<Account> findAll();
    Account findAccountById(Long id);
    Account findByLogin(String login);
    List<Account> findAllByLastName(String lastName);
    Account save(Account account);
    void deleteById(Long id);
}
