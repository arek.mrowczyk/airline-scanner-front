package com.example.airlinesc.repository;

import com.example.airlinesc.model.Country;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CountryRepository extends CrudRepository<Country, Long> {
    List<Country> findAll();
    Country findByCode(String code);
    Country findByName(String name);
    Country save(Country country);
    void deleteById(Long id);
}
