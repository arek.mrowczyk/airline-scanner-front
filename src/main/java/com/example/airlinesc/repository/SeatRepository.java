package com.example.airlinesc.repository;

import com.example.airlinesc.model.Seat;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by gadzik on 07.05.20.
 */
public interface SeatRepository extends CrudRepository<Seat, Long> {
    Seat save(Seat seat);
    Seat getById(Long id);

}
