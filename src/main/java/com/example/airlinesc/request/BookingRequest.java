package com.example.airlinesc.request;

import com.example.airlinesc.model.Person;

import java.util.List;

/**
 * Created by gadzik on 20.04.20.
 */
public class BookingRequest {
    private String login;
    private Long flightId;
    private List<Person> people;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Long getFlightId() {
        return flightId;
    }

    public void setFlightId(Long flightId) {
        this.flightId = flightId;
    }

    public List<Person> getPeople() {
        return people;
    }

    public void setPeople(List<Person> people) {
        this.people = people;
    }
}
