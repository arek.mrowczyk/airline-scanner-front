package com.example.airlinesc.request;

public class BidirectionalConnectionCodeRequest {
    private String originCode;
    private String destinationCode;
    private String flightType;
    private boolean active;

    public BidirectionalConnectionCodeRequest(String originCode, String destinationCode, String flightType, boolean active) {
        this.originCode = originCode;
        this.destinationCode = destinationCode;
        this.flightType = flightType;
        this.active = active;
    }

    public String getOriginCode() {
        return originCode;
    }

    public void setOriginCode(String originCode) {
        this.originCode = originCode;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }

    public String getFlightType() {
        return flightType;
    }

    public void setFlightType(String flightType) {
        this.flightType = flightType;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "BidirectionalConnectionCodeRequest{" +
                "originCode='" + originCode + '\'' +
                ", destinationCode='" + destinationCode + '\'' +
                ", flightType='" + flightType + '\'' +
                ", active=" + active +
                '}';
    }
}
