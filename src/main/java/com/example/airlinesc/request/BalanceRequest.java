package com.example.airlinesc.request;

import java.math.BigDecimal;

/**
 * Created by gadzik on 05.06.20.
 */
public class BalanceRequest {
    private String login;
    private BigDecimal balance;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
