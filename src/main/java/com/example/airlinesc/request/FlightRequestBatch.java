package com.example.airlinesc.request;

import com.example.airlinesc.model.Seat;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by gadzik on 19.03.20.
 */
public class FlightRequestBatch {
    private Long id;
    private String aircraftModel;
    private BigDecimal fare;
    private Integer availableSeats;
    private Seat seat;
    private LocalDateTime date;
    private String originCode;
    private String destinationCode;

    public FlightRequestBatch(Long id, String aircraftModel, BigDecimal fare, Integer availableSeats, Seat seat, LocalDateTime date, String originCode, String destinationCode) {
        this.id = id;
        this.aircraftModel = aircraftModel;
        this.fare = fare;
        this.availableSeats = availableSeats;
        this.seat = seat;
        this.date = date;
        this.originCode = originCode;
        this.destinationCode = destinationCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAircraftModel() {
        return aircraftModel;
    }

    public void setAircraftModel(String aircraftModel) {
        this.aircraftModel = aircraftModel;
    }

    public BigDecimal getFare() {
        return fare;
    }

    public void setFare(BigDecimal fare) {
        this.fare = fare;
    }

    public Integer getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(Integer availableSeats) {
        this.availableSeats = availableSeats;
    }

    public Seat getSeat() {
        return seat;
    }

    public void setSeat(Seat seat) {
        this.seat = seat;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public String getOriginCode() {
        return originCode;
    }

    public void setOriginCode(String originCode) {
        this.originCode = originCode;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }
}
