package com.example.airlinesc.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by gadzik on 15.03.20.
 */
@Entity
@Table(name = "flight")
public class Flight {

    @SequenceGenerator(name = "flightSEQ", sequenceName = "flight_id_seq", allocationSize = 1)
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "flightSEQ")
    private Long id;

    @Column(name = "aircraft_model")
    private String aircraftModel;

    @Column(name = "fare")
    private BigDecimal fare;

    @Column(name = "available_seats")
    private Integer availableSeats;

    @Column(name = "date")
    private LocalDateTime date;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "airport_connection_id", referencedColumnName = "id")
    private AirportConnection connection;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "seat_id", referencedColumnName = "id")
    private Seat seat;

    public Flight() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAircraftModel() {
        return aircraftModel;
    }

    public void setAircraftModel(String aircraftModel) {
        this.aircraftModel = aircraftModel;
    }

    public BigDecimal getFare() {
        return fare;
    }

    public void setFare(BigDecimal fare) {
        this.fare = fare;
    }

    public Integer getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(Integer availableSeats) {
        this.availableSeats = availableSeats;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public AirportConnection getConnection() {
        return connection;
    }

    public void setConnection(AirportConnection connection) {
        this.connection = connection;
    }

    public Seat getSeat() {
        return seat;
    }

    public void setSeat(Seat seat) {
        this.seat = seat;
    }
}
