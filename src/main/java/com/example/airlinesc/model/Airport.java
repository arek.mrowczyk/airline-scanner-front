package com.example.airlinesc.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "airport")
public class Airport {

    @SequenceGenerator(name = "airportSEQ", sequenceName = "airport_id_seq", allocationSize = 1)
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "airportSEQ")
    private Long id;

    @Column(name = "airport_code", length = 20)
    private String airportCode;

    @Column(name = "airport_name", length = 100)
    private String airportName;

    @Column(name = "city_name", length = 40)
    private String cityName;

    @Column(name = "city_code", length = 40)
    private String cityCode;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "country_id", referencedColumnName = "id")
    private Country country;

    public Airport() {
    }

    public Airport(String airportCode, String airportName, String cityName, String cityCode, Country country) {
        this.airportCode = airportCode;
        this.airportName = airportName;
        this.cityName = cityName;
        this.cityCode = cityCode;
        this.country = country;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAirportCode() {
        return airportCode;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}