package com.example.airlinesc.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "airport_connection")
public class AirportConnection {
    private static final Boolean DEFAULT_ACTIVE = false;

    @SequenceGenerator(name = "airport_connectionSEQ", sequenceName = "airport_connection_id_seq", allocationSize = 1)
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "airport_connectionSEQ")
    private Long id;

    @Column(name = "flight_type", length = 80)
    private String flightType;

    @Column(name = "active")
    private Boolean active;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "destination_airport_id", referencedColumnName = "id")
    private Airport destinationAirport;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "origin_airport_id", referencedColumnName = "id")
    private Airport originAirport;

    public AirportConnection() {
    }

    public Long getId() {
        return id;
    }

    public String getFlightType() {
        return flightType;
    }

    public void setFlightType(String flightType) {
        this.flightType = flightType;
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Airport getDestinationAirport() {
        return destinationAirport;
    }

    public void setDestinationAirport(Airport destinationAirport) {
        this.destinationAirport = destinationAirport;
    }

    public Airport getOriginAirport() {
        return originAirport;
    }

    public void setOriginAirport(Airport originAirport) {
        this.originAirport = originAirport;
    }

    @PrePersist
    public void prePersist() {
        if (active == null) {
            active = DEFAULT_ACTIVE;
        }
    }

    @PreUpdate
    public void preUpdate() {
        if (active == null) {
            active = DEFAULT_ACTIVE;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AirportConnection that = (AirportConnection) o;
        return Objects.equals(flightType, that.flightType) &&
                Objects.equals(destinationAirport, that.destinationAirport) &&
                Objects.equals(originAirport, that.originAirport);
    }

    @Override
    public int hashCode() {
        return Objects.hash(flightType, destinationAirport, originAirport);
    }
}
