package com.example.airlinesc.model;

import javax.persistence.*;

/**
 * Created by gadzik on 07.05.20.
 */
@Entity
@Table(name = "seat")
public class Seat {

    @SequenceGenerator(name = "seatSEQ", sequenceName = "seat_id_seq", allocationSize = 1)
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seatSEQ")
    private Long id;

    @Column(name = "seats")
    private String seats;

    public Seat() {
    }

    public Seat(String seats) {
        this.seats = seats;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSeats() {
        return seats;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }
}
