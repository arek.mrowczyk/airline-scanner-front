package com.example.airlinesc.controller;

import com.example.airlinesc.model.Booking;
import com.example.airlinesc.request.BookingRequest;
import com.example.airlinesc.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by gadzik on 20.04.20.
 */
@RestController
public class BookingController {

    private static final Logger LOGGER = Logger.getLogger("BookingController.class");

    private final BookingService bookingService;

    @Autowired
    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @GetMapping("booking")
    public ResponseEntity<List<Booking>> getAll() {
        LOGGER.info("booking - getAll");
        return new ResponseEntity<>(bookingService.getAll(), HttpStatus.OK);
    }

    @GetMapping("booking/id/{id}")
    public ResponseEntity<Booking> getAllById(@PathVariable Long id) {
        LOGGER.info("booking - getAllById");
        return new ResponseEntity<>(bookingService.getById(id), HttpStatus.OK);
    }

    @GetMapping("booking/login/{login}")
    public ResponseEntity<List<Booking>> getAllByLogin(@PathVariable String login) {
        LOGGER.info("booking - getAllByLogin");
        return new ResponseEntity<>(bookingService.getAllByLogin(login), HttpStatus.OK);
    }

    @PostMapping("booking")
    public ResponseEntity<List<Booking>> save(@RequestBody BookingRequest request) {
        LOGGER.info("booking - save");
        List<Booking> bookings = bookingService.save(request);
        return new ResponseEntity<>(bookings, HttpStatus.OK);
    }

}
