package com.example.airlinesc.controller;

import com.example.airlinesc.model.Account;
import com.example.airlinesc.request.AccountRequest;
import com.example.airlinesc.request.BalanceRequest;
import com.example.airlinesc.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by gadzik on 15.03.20.
 */
@RestController
public class AccountController {

    private static final Logger LOGGER = Logger.getLogger("AccountController.class");

    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("accounts")
    public ResponseEntity<List<Account>> getAll(){
        LOGGER.info("accounts - getAll");
        return new ResponseEntity<>(accountService.getAll(), HttpStatus.OK);
    }

    @GetMapping("accounts/{id}")
    public ResponseEntity<Account> getById(@PathVariable Long id){
        LOGGER.info("accounts - getById");
        return new ResponseEntity<>(accountService.getById(id), HttpStatus.OK);
    }

    @GetMapping("accounts/login/{login}")
    public ResponseEntity<Account> getByLogin(@PathVariable String login){
        LOGGER.info("accounts - getByLogin");
        return new ResponseEntity<>(accountService.getByLogin(login), HttpStatus.OK);
    }

    @GetMapping("accounts/lastName/{lastName}")
    public ResponseEntity<List<Account>> getByLastName(@PathVariable String lastName){
        LOGGER.info("accounts - getByLastName");
        return new ResponseEntity<>(accountService.getAllByLastName(lastName), HttpStatus.OK);
    }

    @PostMapping("accounts")
    public ResponseEntity<Void> createOrUpdate(@RequestBody AccountRequest request){
        LOGGER.info("accounts - createOrUpdate");
        accountService.save(request);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("accounts/balance")
    public ResponseEntity<Void> balance(@RequestBody BalanceRequest request){
        LOGGER.info("accounts - balance");
        accountService.balanceUpdate(request);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("accounts/delete/{id}")
    public void deleteById(@PathVariable Long id){
        LOGGER.info("accounts - deleteById");
        accountService.deleteById(id);
    }

}
