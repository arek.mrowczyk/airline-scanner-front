package com.example.airlinesc.controller;

import com.example.airlinesc.model.Account;
import com.example.airlinesc.model.Airport;
import com.example.airlinesc.model.Country;
import com.example.airlinesc.model.Seat;
import com.example.airlinesc.request.AccountRequest;
import com.example.airlinesc.request.BidirectionalConnectionCodeRequest;
import com.example.airlinesc.request.FlightRequestBatch;
import com.example.airlinesc.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by gadzik on 19.03.20.
 */
@RestController
public class DataBatchController {

    private static final Logger LOGGER = Logger.getLogger("DataBatchController.class");

    private final AccountService accountService;
    private final AirportService airportService;
    private final AirportConnectionService airportConnectionService;
    private final CountryService countryService;
    private final FlightService flightService;
    private final SeatService seatService;

    private List<BigDecimal> longFlightsFares = new ArrayList<>();
    private List<BigDecimal> shortFlightFares = new ArrayList<>();
    private final List<String> airCraftModels = Arrays.asList("Airbus A330-300", "Boeing 777-200", "Superjet 100", "Embraer 44", "Bombardier 44", "ATR");

    @Autowired
    public DataBatchController(AccountService accountService, AirportService airportService, AirportConnectionService airportConnectionService, CountryService countryService, FlightService flightService, SeatService seatService) {
        this.accountService = accountService;
        this.airportService = airportService;
        this.airportConnectionService = airportConnectionService;
        this.countryService = countryService;
        this.flightService = flightService;
        this.seatService = seatService;
    }

    @PostMapping("data/insert")
    public ResponseEntity<Void> dataInsert() {
        Account admin = new Account();
        admin.setLogin("admin");
        admin.setPassword("admin".toCharArray());
        admin.setFirstName("admin");
        admin.setLastName("admin");
        admin.setBalance(BigDecimal.valueOf(999999.0));
        admin.setAuthorties("USER,ADMIN");
        admin.setDateOfBirth(LocalDate.now());

        Account airlines = new Account();
        airlines.setLogin("airlines");
        airlines.setPassword("airlines".toCharArray());
        airlines.setFirstName("airlines");
        airlines.setLastName("airlines");
        airlines.setBalance(BigDecimal.valueOf(10003500.0));
        airlines.setDateOfBirth(LocalDate.now());

        Account kamil = new Account();
        kamil.setLogin("kamil");
        kamil.setPassword("kamil".toCharArray());
        kamil.setFirstName("kamil");
        kamil.setLastName("szwed");
        kamil.setBalance(BigDecimal.valueOf(3000.0));
        kamil.setDateOfBirth(LocalDate.now());

        Account arek = new Account();
        arek.setLogin("arek");
        arek.setPassword("arek".toCharArray());
        arek.setFirstName("arek");
        arek.setLastName("mrowczyk");
        arek.setBalance(BigDecimal.valueOf(5000.0));
        arek.setDateOfBirth(LocalDate.now());

        Account gadzik = new Account();
        gadzik.setLogin("gadzik");
        gadzik.setPassword("gadzik".toCharArray());
        gadzik.setFirstName("kamil");
        gadzik.setLastName("gadzinsky");
        gadzik.setBalance(BigDecimal.valueOf(9000.0));
        gadzik.setDateOfBirth(LocalDate.now());

        List<Account> accounts = new ArrayList<>();
        accounts.add(airlines);
        accounts.add(admin);
        accounts.add(kamil);
        accounts.add(arek);
        accounts.add(gadzik);
        accounts.forEach(accountService::save);


        Country poland = new Country("POLAND", "PL");
        Country belarus = new Country("BELARUS", "BS");
        Country russia = new Country("RUSSIA", "RU");
        Country germany = new Country("GERMANY", "GE");
        Country greatBritan = new Country("GREAT BRITAIN", "GB");
        Country france = new Country("FRANCE", "FR");
        Country spain = new Country("SPAIN", "ES");
        Country portugal = new Country("PORTUGAL", "PR");
        Country italy = new Country("ITALY", "IT");

        List<Country> countries = new ArrayList<>();
        countries.add(poland);
        countries.add(belarus);
        countries.add(russia);
        countries.add(germany);
        countries.add(greatBritan);
        countries.add(france);
        countries.add(spain);
        countries.add(portugal);
        countries.add(italy);
        countries.forEach(countryService::save);

        Airport krk = new Airport("KRK", "KRAKOW JOHN PAUL II INTERNATIONAL AIRPORT", "KRAKOW", "KRK", poland);
        Airport mns = new Airport("MNS", "MINSK NATIONAL AIRPORT", "MINSK", "MNS", belarus);
        Airport mscD = new Airport("MSCD", "DOMODEDOVO INTERNATIONAL AIRPORT", "MOSCOW", "MSC", russia);
        Airport mscP = new Airport("MSCP", "PULKOVO AIRPORT", "MOSCOW", "MSC", russia);
        Airport ham = new Airport("HAM", "HAMBURG LUBEKA", "HAMBURG", "HAM", germany);
        Airport ber = new Airport("BER", "BERLIN BRANDENBURG WILLY BRANDT", "BERLIN", "BER", germany);
        Airport ldn = new Airport("LDN", "HEATHROW AIRPORT", "LONDON", "LDN", greatBritan);
        Airport bir = new Airport("BIR", "BIRMINGHAM AIRPORT", "LONDON", "BIR", greatBritan);
        Airport par = new Airport("PAR", "CHARLES DE GAULLE AIRPORT", "PARIS", "PAR", france);
        Airport mad = new Airport("MAD", "ADOLFO SUAREZ MADRID–BARAJAS AIRPORT", "MADRID", "MAD", spain);
        Airport lis = new Airport("LSB", "LISBONA PORTELA", "LISBONA", "LSB", portugal);
        Airport rom = new Airport("ROM", "ROMA FLUMICINO", "ROMA", "ROM", italy);

        List<Airport> airports = new ArrayList<>();
        airports.add(krk);
        airports.add(mns);
        airports.add(mscD);
        airports.add(mscP);
        airports.add(ham);
        airports.add(ber);
        airports.add(ldn);
        airports.add(bir);
        airports.add(par);
        airports.add(mad);
        airports.add(lis);
        airports.add(rom);
        airports.forEach(airportService::save);

        BidirectionalConnectionCodeRequest krk_mns = new BidirectionalConnectionCodeRequest("KRK", "MNS", "SHORT", true);
        BidirectionalConnectionCodeRequest krk_mscd = new BidirectionalConnectionCodeRequest("KRK", "MSCD", "LONG", true);
        BidirectionalConnectionCodeRequest krk_ham = new BidirectionalConnectionCodeRequest("KRK", "HAM", "SHORT", true);
        BidirectionalConnectionCodeRequest krk_ldn = new BidirectionalConnectionCodeRequest("KRK", "LDN", "LONG", true);
        BidirectionalConnectionCodeRequest krk_par = new BidirectionalConnectionCodeRequest("KRK", "PAR", "LONG", true);
        BidirectionalConnectionCodeRequest krk_mad = new BidirectionalConnectionCodeRequest("KRK", "MAD", "LONG", true);
        BidirectionalConnectionCodeRequest krk_rom = new BidirectionalConnectionCodeRequest("KRK", "ROM", "SHORT", true);
        BidirectionalConnectionCodeRequest krk_lsb = new BidirectionalConnectionCodeRequest("KRK", "LSB", "LONG", true);

        BidirectionalConnectionCodeRequest mns_mscp = new BidirectionalConnectionCodeRequest("MNS", "MSCP", "SHORT", true);
        BidirectionalConnectionCodeRequest mns_mscd = new BidirectionalConnectionCodeRequest("MNS", "BER", "LONG", true);
        BidirectionalConnectionCodeRequest mns_ham = new BidirectionalConnectionCodeRequest("MNS", "BIR", "LONG", true);
        BidirectionalConnectionCodeRequest mns_lsb = new BidirectionalConnectionCodeRequest("MNS", "LSB", "LONG", true);

        BidirectionalConnectionCodeRequest mscD_mns = new BidirectionalConnectionCodeRequest("MSCD", "MSCP", "SHORT", true);
        BidirectionalConnectionCodeRequest mscD_mscd = new BidirectionalConnectionCodeRequest("MSCD", "BER", "LONG", true);
        BidirectionalConnectionCodeRequest mscD_ham = new BidirectionalConnectionCodeRequest("MSCD", "HAM", "LONG", true);
        BidirectionalConnectionCodeRequest mscD_ldn = new BidirectionalConnectionCodeRequest("MSCD", "LDN", "LONG", true);
        BidirectionalConnectionCodeRequest mscD_par = new BidirectionalConnectionCodeRequest("MSCD", "PAR", "LONG", true);
        BidirectionalConnectionCodeRequest mscD_rom = new BidirectionalConnectionCodeRequest("MSCD", "ROM", "LONG", true);
        BidirectionalConnectionCodeRequest mscD_mad = new BidirectionalConnectionCodeRequest("MSCD", "MAD", "LONG", true);

        BidirectionalConnectionCodeRequest mscP_mns = new BidirectionalConnectionCodeRequest("MSCP", "LSB", "LONG", true);
        BidirectionalConnectionCodeRequest mscP_mscd = new BidirectionalConnectionCodeRequest("MSCP", "BIR", "LONG", true);
        BidirectionalConnectionCodeRequest mscP_ham = new BidirectionalConnectionCodeRequest("MSCP", "HAM", "LONG", true);
        BidirectionalConnectionCodeRequest mscP_ldn = new BidirectionalConnectionCodeRequest("MSCP", "LDN", "LONG", true);
        BidirectionalConnectionCodeRequest mscP_par = new BidirectionalConnectionCodeRequest("MSCP", "PAR", "LONG", true);

        BidirectionalConnectionCodeRequest ham_ber = new BidirectionalConnectionCodeRequest("HAM", "BER", "SHORT", true);
        BidirectionalConnectionCodeRequest ham_ldn = new BidirectionalConnectionCodeRequest("HAM", "LDN", "SHORT", true);
        BidirectionalConnectionCodeRequest ham_par = new BidirectionalConnectionCodeRequest("HAM", "BIR", "SHORT", true);
        BidirectionalConnectionCodeRequest ham_rom = new BidirectionalConnectionCodeRequest("HAM", "ROM", "SHORT", true);
        BidirectionalConnectionCodeRequest ham_mad = new BidirectionalConnectionCodeRequest("HAM", "MAD", "SHORT", true);
        BidirectionalConnectionCodeRequest ham_lsb = new BidirectionalConnectionCodeRequest("HAM", "LSB", "LONG", true);

        BidirectionalConnectionCodeRequest ber_ldn = new BidirectionalConnectionCodeRequest("BER", "LDN", "SHORT", true);
        BidirectionalConnectionCodeRequest ber_par = new BidirectionalConnectionCodeRequest("BER", "BIR", "SHORT", true);
        BidirectionalConnectionCodeRequest ber_rom = new BidirectionalConnectionCodeRequest("BER", "ROM", "SHORT", true);
        BidirectionalConnectionCodeRequest ber_mad = new BidirectionalConnectionCodeRequest("BER", "MAD", "SHORT", true);
        BidirectionalConnectionCodeRequest ber_lsb = new BidirectionalConnectionCodeRequest("BER", "LSB", "LONG", true);

        BidirectionalConnectionCodeRequest ldn_par = new BidirectionalConnectionCodeRequest("LDN", "BIR", "SHORT", true);
        BidirectionalConnectionCodeRequest ldn_rom = new BidirectionalConnectionCodeRequest("LDN", "ROM", "SHORT", true);
        BidirectionalConnectionCodeRequest ldn_mad = new BidirectionalConnectionCodeRequest("LDN", "MAD", "SHORT", true);
        BidirectionalConnectionCodeRequest ldn_lsb = new BidirectionalConnectionCodeRequest("LDN", "LSB", "LONG", true);

        BidirectionalConnectionCodeRequest par_mad = new BidirectionalConnectionCodeRequest("PAR", "MAD", "SHORT", true);
        BidirectionalConnectionCodeRequest par_lsb = new BidirectionalConnectionCodeRequest("PAR", "LSB", "SHORT", true);
        BidirectionalConnectionCodeRequest par_rom = new BidirectionalConnectionCodeRequest("PAR", "ROM", "SHORT", true);

        List<BidirectionalConnectionCodeRequest> connectionRequests = new ArrayList<>();
        connectionRequests.add(krk_mns);
        connectionRequests.add(krk_mscd);
        connectionRequests.add(krk_ham);
        connectionRequests.add(krk_ldn);
        connectionRequests.add(krk_par);
        connectionRequests.add(krk_mad);
        connectionRequests.add(krk_rom);
        connectionRequests.add(krk_lsb);

        connectionRequests.add(mns_mscp);
        connectionRequests.add(mns_mscd);
        connectionRequests.add(mns_ham);
        connectionRequests.add(mns_lsb);

        connectionRequests.add(mscD_mns);
        connectionRequests.add(mscD_mscd);
        connectionRequests.add(mscD_ham);
        connectionRequests.add(mscD_ldn);
        connectionRequests.add(mscD_par);
        connectionRequests.add(mscD_rom);
        connectionRequests.add(mscD_mad);

        connectionRequests.add(mscP_mns);
        connectionRequests.add(mscP_mscd);
        connectionRequests.add(mscP_ham);
        connectionRequests.add(mscP_ldn);
        connectionRequests.add(mscP_par);

        connectionRequests.add(ham_ber);
        connectionRequests.add(ham_ldn);
        connectionRequests.add(ham_par);
        connectionRequests.add(ham_rom);
        connectionRequests.add(ham_mad);
        connectionRequests.add(ham_lsb);

        connectionRequests.add(ber_ldn);
        connectionRequests.add(ber_par);
        connectionRequests.add(ber_rom);
        connectionRequests.add(ber_mad);
        connectionRequests.add(ber_lsb);

        connectionRequests.add(ldn_par);
        connectionRequests.add(ldn_rom);
        connectionRequests.add(ldn_mad);
        connectionRequests.add(ldn_lsb);

        connectionRequests.add(par_mad);
        connectionRequests.add(par_lsb);
        connectionRequests.add(par_rom);
        connectionRequests.forEach(airportConnectionService::bidirectionalConnectionOfCodes);


        shortFlightFares = generateBaseFares("100.00", "500.00", "10.00");
        longFlightsFares = generateBaseFares("500.00", "2000.00", "100.00");
        connectionRequests.forEach(request -> {
            flightGenerator(request, 2020, 6);
        });


        return new ResponseEntity<>(HttpStatus.OK);
    }

    private void flightGenerator(BidirectionalConnectionCodeRequest request, Integer year, Integer... months) {
        for (Integer month : months) {
            YearMonth yearMonth = YearMonth.of(year, month);
            int days = yearMonth.lengthOfMonth();
            Set<Integer> noFlightDays = generateNoFlightDaysForMonth(days, 8);
            for (int day = 1; day <= days; day++) {
                if (noFlightDays.contains(day)) {
                    continue;
                }
                LocalDateTime date = LocalDateTime.of(year, month, day, 15, 0);
                BigDecimal fare = calculateFare(getBaseFare(request.getFlightType()), date);
                String aircraftModel = getAircraft();
                LOGGER.info(request.getOriginCode() + " - " + request.getDestinationCode() + " " + aircraftModel
                        + " " + year + "/" + month + "/" + day);

                Seat seat = new Seat(readSeatsJson());
                seatService.save(seat);

                Seat seat2 = new Seat(readSeatsJson());
                seatService.save(seat2);

                FlightRequestBatch flightRequest = new FlightRequestBatch(null, aircraftModel, fare,
                        30, seat, date, request.getOriginCode(), request.getDestinationCode());
                flightService.saveForBatch(flightRequest);
                FlightRequestBatch flightRequest2 = new FlightRequestBatch(null, aircraftModel, fare,
                        30, seat2, date, request.getDestinationCode(), request.getOriginCode());
                flightService.saveForBatch(flightRequest2);
            }
        }
    }

    private BigDecimal calculateFare(BigDecimal baseFare, LocalDateTime date) {
        if (date.getDayOfWeek() == DayOfWeek.SUNDAY || date.getDayOfWeek() == DayOfWeek.SATURDAY) {
            return baseFare.multiply(new BigDecimal(1.20)).setScale(2, RoundingMode.HALF_DOWN);
        }
        if (date.getDayOfWeek() == DayOfWeek.MONDAY) {
            return baseFare.multiply(new BigDecimal(1.50)).setScale(2, RoundingMode.HALF_DOWN);
        }
        if (date.getDayOfWeek() == DayOfWeek.THURSDAY) {
            return baseFare.multiply(new BigDecimal(0.90)).setScale(2, RoundingMode.HALF_DOWN);
        } else {
            return baseFare;
        }
    }

    private Set<Integer> generateNoFlightDaysForMonth(Integer days, Integer amountNoFlightDays) {
        Random random = new Random();
        Set<Integer> noFlightDays = new HashSet<>();
        while (noFlightDays.size() < amountNoFlightDays) {
            noFlightDays.add(random.nextInt(days) + 1);
        }
        return noFlightDays;
    }

    private List<BigDecimal> generateBaseFares(String min, String max, String addBy) {
        List<BigDecimal> fares = new ArrayList<>();
        BigDecimal baseFare = new BigDecimal(min).setScale(2, RoundingMode.HALF_DOWN);
        while (baseFare.compareTo(new BigDecimal(max)) != 1) {
            fares.add(baseFare);
            baseFare = baseFare.add(new BigDecimal(addBy));
        }
        return fares;
    }

    private BigDecimal getBaseFare(String flightType) {
        Random random = new Random();
        if ("SHORT".equalsIgnoreCase(flightType)) {
            return shortFlightFares.get(random.nextInt(shortFlightFares.size()))
                    .setScale(2, RoundingMode.HALF_DOWN);
        } else if ("LONG".equalsIgnoreCase(flightType)) {
            return longFlightsFares.get(random.nextInt(longFlightsFares.size()))
                    .setScale(2, RoundingMode.HALF_DOWN);
        }
        return new BigDecimal(0.00).setScale(2, RoundingMode.HALF_DOWN);
    }

    private String getAircraft() {
        Random random = new Random();
        return airCraftModels.get(random.nextInt(airCraftModels.size()));
    }

    private String readSeatsJson(){
        StringBuilder sb = new StringBuilder();
        BufferedReader reader =
                null;
        try {
            reader = Files.newBufferedReader(Paths.get("src/main/resources/seats.json"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        reader.lines().forEach(sb::append);
        return sb.toString();
    }

}
