package com.example.airlinesc.controller;

import com.example.airlinesc.model.Flight;
import com.example.airlinesc.request.FlightRequest;
import com.example.airlinesc.service.FlightService;
import com.example.airlinesc.util.DateConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by gadzik on 15.03.20.
 */
@RestController
public class FlightController {

    private final FlightService flightService;

    @Autowired
    public FlightController(FlightService flightService) {
        this.flightService = flightService;
    }

    @GetMapping("flights")
    public ResponseEntity<List<Flight>> getAll() {
        return new ResponseEntity<>(flightService.getAll(), HttpStatus.OK);
    }

    @PostMapping("flights")
    public ResponseEntity<Void> createOrUpdate(@RequestBody FlightRequest request) {
        flightService.save(request);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("flights/origin/{originCode}")
    public ResponseEntity<List<Flight>> getAllByOrigin(@PathVariable String originCode) {
        return new ResponseEntity<>(flightService.getAllByOriginCode(originCode), HttpStatus.OK);
    }

    @GetMapping("flights/destination/{destinationCode}")
    public ResponseEntity<List<Flight>> getAllByDestination(@PathVariable String destinationCode) {
        return new ResponseEntity<>(flightService.getAllByDestinationCode(destinationCode), HttpStatus.OK);
    }

    @GetMapping("flights/origin/{originCode}/destination/{destinationCode}")
    public ResponseEntity<List<Flight>> getAllByOriginAndDestination(@PathVariable String originCode, @PathVariable String destinationCode) {
        return new ResponseEntity<>(flightService.getAllByOriginCodeAndDestinationCode(originCode, destinationCode), HttpStatus.OK);
    }

    @GetMapping("flights/origin/{originCode}/before/{date}")
    public ResponseEntity<List<Flight>> getAllByOriginBefore(@PathVariable String originCode, @PathVariable String date) {
        return new ResponseEntity<>(flightService.getAllByOriginCodeBeforeDate(originCode, DateConverter.toDateTimeNoHour(date)), HttpStatus.OK);
    }

    @GetMapping("flights/destination/{destinationCode}/before/{date}")
    public ResponseEntity<List<Flight>> getAllByDestinationCodeBefore(@PathVariable String destinationCode, @PathVariable String date) {
        return new ResponseEntity<>(flightService.getAllByDestinationCodeBeforeDate(destinationCode, DateConverter.toDateTimeNoHour(date)), HttpStatus.OK);
    }

    @GetMapping("flights/origin/{originCode}/after/{date}")
    public ResponseEntity<List<Flight>> getAllByOriginAfter(@PathVariable String originCode, @PathVariable String date) {
        return new ResponseEntity<>(flightService.getAllByOriginCodeAfterDate(originCode, DateConverter.toDateTimeNoHour(date)), HttpStatus.OK);
    }

    @GetMapping("flights/destination/{destinationCode}/after/{date}")
    public ResponseEntity<List<Flight>> getAllByDestinationCodeAfter(@PathVariable String destinationCode, @PathVariable String date) {
        return new ResponseEntity<>(flightService.getAllByDestinationCodeAfterDate(destinationCode, DateConverter.toDateTimeNoHour(date)), HttpStatus.OK);
    }

    @GetMapping("flights/origin/{originCode}/between/{from}/{to}")
    public ResponseEntity<List<Flight>> getAllByOriginBetween(@PathVariable String originCode, @PathVariable String from, @PathVariable String to) {
        return new ResponseEntity<>(flightService.getAllByOriginCodeBetweenDate(originCode, DateConverter.toDateTimeNoHour(from), DateConverter.toDateTimeNoHour(to)), HttpStatus.OK);
    }

    @GetMapping("flights/destination/{destinationCode}/between/{from}/{to}")
    public ResponseEntity<List<Flight>> getAllByDestinationBetween(@PathVariable String destinationCode,
                                                                   @PathVariable String from,
                                                                   @PathVariable String to) {
        return new ResponseEntity<>(flightService.getAllByDestinationCodeBetweenDate(
                destinationCode, DateConverter.toDateTimeNoHour(from), DateConverter.toDateTimeNoHour(to)), HttpStatus.OK);
    }

    @GetMapping("flights/origin/{originCode}/destination/{destinationCode}/between/{from}/{to}")
    public ResponseEntity<List<Flight>> getAllByOriginAndDestinationBetween(@PathVariable String originCode,
                                                                            @PathVariable String destinationCode,
                                                                            @PathVariable String from,
                                                                            @PathVariable String to) {
        return new ResponseEntity<>(flightService.getAllByOriginCodeAndDestinationCodeBetweenDate(
                originCode, destinationCode, DateConverter.toDateTimeNoHour(from), DateConverter.toDateTimeNoHour(to)), HttpStatus.OK);
    }

    @PostMapping("flights/delete/{id}")
    public void deleteById(@PathVariable Long id) {
        flightService.deleteById(id);
    }

}
