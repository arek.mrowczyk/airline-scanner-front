package com.example.airlinesc.controller;

import com.example.airlinesc.model.Country;
import com.example.airlinesc.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@RestController
public class CountryController {

    private static final Logger LOGGER = Logger.getLogger("CountryController.class");

    private final CountryService countryService;

    @Autowired
    public CountryController(CountryService countryService) {
        this.countryService = countryService;
    }

//    @PreAuthorize("hasAuthority('USER')")
    @GetMapping("countries")
    public ResponseEntity<List<Country>> getAll() {
        LOGGER.info("countries - getAll");
        return new ResponseEntity<>(countryService.getAll(), HttpStatus.OK);
    }

    @GetMapping("countries/code/{code}")
    public ResponseEntity<Country> getByCode(@PathVariable String code) {
        LOGGER.info("countries - getByCode");
        return new ResponseEntity<>(countryService.getByCode(code), HttpStatus.OK);
    }

    @GetMapping("countries/{name}")
    public ResponseEntity<Country> getByName(@PathVariable String name) {
        LOGGER.info("countries - getByName");
        return new ResponseEntity<>(countryService.getByName(name), HttpStatus.OK);
    }

    @PostMapping("countries")
    public void createOrUpdate(@RequestBody Country country) {
        LOGGER.info("countries - createOrUpdate");
        countryService.save(country);
    }

    @PostMapping("countries/delete/{id}")
    public void delete(@PathVariable Long id) {
        LOGGER.info("countries - delete");
        countryService.deleteById(id);
    }

}
