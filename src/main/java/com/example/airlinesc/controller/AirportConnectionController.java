package com.example.airlinesc.controller;

import com.example.airlinesc.model.Airport;
import com.example.airlinesc.model.AirportConnection;
import com.example.airlinesc.request.BidirectionalConnectionCodeRequest;
import com.example.airlinesc.request.BidirectionalConnectionIdRequest;
import com.example.airlinesc.service.AirportConnectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@RestController
public class AirportConnectionController {

    private static final Logger LOGGER = Logger.getLogger("AirportConnectionController.class");

    private final AirportConnectionService airportConnectionService;

    @Autowired
    public AirportConnectionController(AirportConnectionService airportConnectionService) {
        this.airportConnectionService = airportConnectionService;
    }

    @GetMapping("airport_connections")
    public ResponseEntity<List<AirportConnection>> getAll() {
        LOGGER.info("airport_connections - getAll");
        return new ResponseEntity<>(airportConnectionService.getAll(), HttpStatus.OK);
    }

    @GetMapping("airport_connections/{id}")
    public ResponseEntity<AirportConnection> getById(@PathVariable Long id) {
        LOGGER.info("airport_connections - getById");
        return new ResponseEntity<>(airportConnectionService.getById(id), HttpStatus.OK);
    }

    @GetMapping("airport_connections/origin/{originCode}/destination/{destinationCode}")
    public ResponseEntity<AirportConnection> getByOriginCodeAndDestinationCode(@PathVariable String originCode,
                                                                               @PathVariable String destinationCode) {
        LOGGER.info("airport_connections - getByOriginCodeAndDestinationCode");
        AirportConnection connection = airportConnectionService.getByOriginCodeAndDestinationCode(originCode, destinationCode);
        return new ResponseEntity<>(connection, HttpStatus.OK);
    }

    @GetMapping("airport_connections/origin/{originCode}")
    public ResponseEntity<List<AirportConnection>> getAllByOrigin(@PathVariable String originCode) {
        LOGGER.info("airport_connections - getByOrigin");
        return new ResponseEntity<>(airportConnectionService.getAllByOrigin(originCode), HttpStatus.OK);
    }

    @GetMapping("airport_connections/destination/{destinationCode}")
    public ResponseEntity<List<AirportConnection>> getAllByDestination(@PathVariable String destinationCode) {
        LOGGER.info("airport_connections - getAllByDestination");
        return new ResponseEntity<>(airportConnectionService.getAllByDestination(destinationCode), HttpStatus.OK);
    }

    @PostMapping("airport_connections/bidirectional")
    public ResponseEntity<Void> bidirectionalConnectionOfIds(@RequestBody BidirectionalConnectionIdRequest request) {
        LOGGER.info("airport_connections - bidirectionalConnectionOfIds");
        airportConnectionService.bidirectionalConnectionOfIds(request);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("airport_connections/codes/bidirectional")
    public ResponseEntity<Void> bidirectionalConnectionOfCodes(@RequestBody BidirectionalConnectionCodeRequest request) {
        LOGGER.info("airport_connections - bidirectionalConnectionOfCodes");
        airportConnectionService.bidirectionalConnectionOfCodes(request);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("airport_connections/delete/{id}")
    public void deleteById(@PathVariable Long id) {
        LOGGER.info("airport_connections - deleteById");
        airportConnectionService.deleteById(id);
    }
}
