package com.example.airlinesc.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AirlineController {


    @GetMapping("/")
    public String home() {
        return "Home page";
    }

}
