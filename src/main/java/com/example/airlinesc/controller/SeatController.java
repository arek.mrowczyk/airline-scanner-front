package com.example.airlinesc.controller;

import com.example.airlinesc.model.Seat;
import com.example.airlinesc.service.SeatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by gadzik on 07.05.20.
 */
@RestController
public class SeatController {

    private final SeatService seatService;

    @Autowired
    public SeatController(SeatService seatService) {
        this.seatService = seatService;
    }

    @PostMapping("seat")
    public ResponseEntity<Void> createOrUpdate(@RequestBody Seat seat) {
        seatService.save(seat);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("seat/id/{id}")
    public ResponseEntity<Seat> getById(@PathVariable Long id) {
        return new ResponseEntity<>(seatService.getById(id) ,HttpStatus.OK);
    }
}
