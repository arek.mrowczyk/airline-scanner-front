package com.example.airlinesc.controller;

import com.example.airlinesc.model.Airport;
import com.example.airlinesc.model.Country;
import com.example.airlinesc.service.AirportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@RestController
public class AirportController {

    private static final Logger LOGGER = Logger.getLogger("AirportController.class");

    private final AirportService airportService;

    @Autowired
    public AirportController(AirportService airportService) {
        this.airportService = airportService;
    }

    @GetMapping("airports")
    public ResponseEntity<List<Airport>> getAll() {
        LOGGER.info("airports - getAll");
        return new ResponseEntity<>(airportService.getAll(), HttpStatus.OK);
    }

    @GetMapping("airports/{id}")
    public ResponseEntity<Airport> getById(@PathVariable Long id) {
        LOGGER.info("airports - getById");
        return new ResponseEntity<>(airportService.getById(id), HttpStatus.OK);
    }

    @GetMapping("airports/code/{code}")
    public ResponseEntity<Airport> getByCode(@PathVariable String code) {
        LOGGER.info("airports - getByCode");
        return new ResponseEntity<>(airportService.getByAirportCode(code), HttpStatus.OK);
    }

    @GetMapping("airports/country/{country}")
    public ResponseEntity<List<Airport>> getAllByCountry(@PathVariable String country) {
        LOGGER.info("airports - getAllByCountry");
        return new ResponseEntity<>(airportService.getAllByCountry(country), HttpStatus.OK);
    }

    @PostMapping("airports")
    public void createOrUpdate(@RequestBody Airport airport) {
        LOGGER.info("airpots - createOrUpdate");
        airportService.save(airport);
    }

    @PostMapping("airports/delete/{id}")
    public void deleteById(@PathVariable Long id) {
        airportService.deleteById(id);
    }
}
