package com.example.airlinesc.controller;

import com.example.airlinesc.model.Account;
import com.example.airlinesc.request.UserRequest;
import com.example.airlinesc.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.logging.Logger;

/**
 * Created by gadzik on 06.04.20.
 */
@RestController
public class AuthController {

    private static final Logger LOGGER = Logger.getLogger("AuthController.class");

    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("authentication/login")
    public ResponseEntity<Void> login(@RequestBody UserRequest request) {
        LOGGER.info("AuthController - login");
        UserDetails user = authService.loadUserByUsername(request.getLogin());
        if(!user.getPassword().equals(request.getPassword())) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        authService.authenticate(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("authentication/logout")
    public ResponseEntity<Void> logout() {
        LOGGER.info("AuthController - logout");
        authService.unauthenticate();
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
