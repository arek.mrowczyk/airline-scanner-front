package com.example.airlinesc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AirlinescApplication {

    public static void main(String[] args) {
        SpringApplication.run(AirlinescApplication.class, args);
    }

}
