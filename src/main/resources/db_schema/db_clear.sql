DELETE FROM booking;
DELETE FROM flight;
DELETE FROM airport_connection;
DELETE FROM airport;
DELETE FROM country;
DELETE FROM account;
DELETE FROM seat;


DROP SEQUENCE country_id_seq;
DROP SEQUENCE airport_id_seq;
DROP SEQUENCE airport_connection_id_seq;
DROP SEQUENCE account_id_seq;
DROP SEQUENCE flight_id_seq;
DROP SEQUENCE booking_id_seq;
DROP SEQUENCE seat_id_seq;

    CREATE SEQUENCE country_id_seq
    MINVALUE 1
    START WITH 1
    INCREMENT BY 1;

    CREATE SEQUENCE airport_id_seq
    MINVALUE 1
    START WITH 1
    INCREMENT BY 1;

    CREATE SEQUENCE airport_connection_id_seq
    MINVALUE 1
    START WITH 1
    INCREMENT BY 1;

    CREATE SEQUENCE account_id_seq
    MINVALUE 1
    START WITH 1
    INCREMENT BY 1;

    CREATE SEQUENCE flight_id_seq
    MINVALUE 1
    START WITH 1
    INCREMENT BY 1;

    CREATE SEQUENCE booking_id_seq
    MINVALUE 1
    START WITH 1
    INCREMENT BY 1;

    CREATE SEQUENCE seat_id_seq
    MINVALUE 1
    START WITH 1
    INCREMENT BY 1;