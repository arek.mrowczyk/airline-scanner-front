DROP TABLE booking;
DROP TABLE flight;
DROP TABLE country;
DROP TABLE airport;
DROP TABLE airport_connection;
DROP TABLE special_service_request;
DROP TABLE account;
DROP TABLE seat;


DROP SEQUENCE country_id_seq;
DROP SEQUENCE airport_id_seq;
DROP SEQUENCE airport_connection_id_seq;
DROP SEQUENCE account_id_seq;
DROP SEQUENCE flight_id_seq;
DROP SEQUENCE booking_id_seq;
DROP SEQUENCE seat_id_seq;
