-- utworzenie uzytkownika oraz bazy danych
create account postgres with password 'postgres';
CREATE DATABASE as-db OWNER postgres;
GRANT ALL ON DATABASE as-db TO postgres;
GRANT ALL ON SCHEMA public to postgres;
GRANT ALL ON ALL TABLES IN SCHEMA public TO postgres;
GRANT ALL ON ALL SEQUENCES IN SCHEMA public TO postgres;


-- utworzenie tabel
CREATE TABLE country
(
    id BIGINT PRIMARY KEY NOT NULL,
    name VARCHAR(100),
    code VARCHAR(10)
);
CREATE TABLE airport
(
    id BIGINT PRIMARY KEY NOT NULL,
    country_id BIGINT,
    airport_code VARCHAR(20),
    airport_name VARCHAR(100),
    city_name VARCHAR(40),
    city_code VARCHAR(40)
);
CREATE TABLE airport_connection
(
    id BIGINT PRIMARY KEY NOT NULL,
    origin_airport_id BIGINT,
    destination_airport_id BIGINT,
    flight_type VARCHAR(80),
    active BOOLEAN DEFAULT true
);
CREATE TABLE special_service_request
(
    id BIGINT PRIMARY KEY NOT NULL,
    code VARCHAR(20),
    price DECIMAL,
    comment VARCHAR(1024)
);

CREATE TABLE account
(
    id BIGINT PRIMARY KEY NOT NULL,
    login VARCHAR(255),
    password VARCHAR(1024),
    first_name VARCHAR(255),
    last_name VARCHAR(255),
    date_of_birth DATE,
    authorties VARCHAR(255),
    balance DECIMAL,
    creation_date TIMESTAMP DEFAULT now()
);

CREATE TABLE flight
(
    id BIGINT PRIMARY KEY NOT NULL,
    aircraft_model VARCHAR(255),
    fare DECIMAL,
    available_seats INT ,
    date TIMESTAMP,
    airport_connection_id BIGINT,
    seat_id BIGINT
);

CREATE TABLE booking
(
    id BIGINT PRIMARY KEY NOT NULL,
    account_id BIGINT,
    flight_id BIGINT,
    price DECIMAL,
    first_name VARCHAR(255),
    last_name VARCHAR(255),
    email VARCHAR(80),
    booking_number VARCHAR(10)
);

CREATE TABLE seat
(
    id BIGINT PRIMARY KEY NOT NULL,
    seats TEXT
);

-- indexy oraz relacje
ALTER TABLE airport ADD FOREIGN KEY (country_id) REFERENCES country (id);
CREATE INDEX idx_airport_1 ON airport (country_id);
CREATE INDEX idx_airport_0 ON airport (airport_code);
CREATE INDEX idx_airport ON airport (city_name);
ALTER TABLE airport_connection ADD FOREIGN KEY (origin_airport_id) REFERENCES airport (id);
ALTER TABLE airport_connection ADD FOREIGN KEY (destination_airport_id) REFERENCES airport (id);
CREATE UNIQUE INDEX idx_airport_connection ON airport_connection (origin_airport_id, destination_airport_id);
CREATE INDEX idx_airport_has_airport ON airport_connection (origin_airport_id);
CREATE INDEX idx_airport_has_airport_0 ON airport_connection (destination_airport_id);

ALTER TABLE flight ADD FOREIGN KEY (airport_connection_id) REFERENCES airport_connection (id);
CREATE INDEX idx_flight_has_airport_connection ON flight (airport_connection_id);

ALTER TABLE booking ADD FOREIGN KEY (account_id) REFERENCES account (id);
ALTER TABLE booking ADD FOREIGN KEY (flight_id) REFERENCES flight (id);
CREATE INDEX idx_booking_has_account ON booking (account_id);
CREATE INDEX idx_booking_has_flight ON booking (flight_id);

ALTER TABLE flight ADD FOREIGN KEY (seat_id) REFERENCES seat (id);
CREATE INDEX idx_flight_has_seat ON flight (seat_id);

-- sekwencje

    CREATE SEQUENCE country_id_seq
    MINVALUE 1
    START WITH 1
    INCREMENT BY 1;

    CREATE SEQUENCE airport_id_seq
    MINVALUE 1
    START WITH 1
    INCREMENT BY 1;

    CREATE SEQUENCE airport_connection_id_seq
    MINVALUE 1
    START WITH 1
    INCREMENT BY 1;

    CREATE SEQUENCE account_id_seq
    MINVALUE 1
    START WITH 1
    INCREMENT BY 1;

    CREATE SEQUENCE flight_id_seq
    MINVALUE 1
    START WITH 1
    INCREMENT BY 1;

    CREATE SEQUENCE booking_id_seq
    MINVALUE 1
    START WITH 1
    INCREMENT BY 1;

    CREATE SEQUENCE seat_id_seq
    MINVALUE 1
    START WITH 1
    INCREMENT BY 1;

