import React from 'react';
import MainComponent from '../components/MainComponent/MainComponent';
import { DashboardComponent } from '../components/DashboardComponent/DashboardComponent';
import CountriesPage from '../pages/admin/CountriesPage/CountriesPage';
import UserSettingsPage from '../pages/UserSettingsPage/UserSettingsPage';
import AirportsPage from '../pages/admin/AirportsPage/AirportsPage';
import AirportConnectionsPage from '../pages/admin/AirportConnectionsPage/AirportConnectionsPage';
import { BookComponent } from '../components/BookComponent/BookComponent';
import FlightsPage from '../pages/admin/FlightsPage/FlightsPage';
import { UserFlightsComponent } from '../components/UserFlightsComponent/UserFlightsComponent';

/**
 * Interfejs routingu
 * 
 */
export interface IRouting {

    /**
     * Ścieżka do konkretnego komponentu
     */
    path: string;

    /**
     * Komponent się renderuje jeżeli jest dokładnie taka sama ścieżka jak pole path
     */
    exact?: boolean;

    /**
     * Komponent się renderuje jeżeli jest ścieżka zaczyna się tak samo jak pole path
     */
    strict?: boolean;

    /**
     * Element JSX jaki ma być wyrenderowany
     */
    render: JSX.Element;

    /**
     * Flaga, która mówi czy dostęp do ścieżki jest dostępny dla niezalogowanych użytkowników
     */
    auth: boolean;

    /**
     * Flaga, która mówi czy wyświetlać pozycję w drawerze
     */
    drawer?: boolean;

    /**
     * Tytuł który ma się wyświetlać przy tym screenie w drawerze
     */
    title?: string;

    /**
     * Flaga, która mówi czy dostęp tylko dla admina
     */
    admin?: boolean;
}

/**
 * Routing aplikacji na podstawie tych pól są generowane ścieżki w pliku App.tsx
 */
export const Routing: IRouting[] = [
    { path: '/', exact: true, render: <MainComponent />, auth: false },
    { path: '/home', strict: true, render: <DashboardComponent />, auth: true, drawer: true, title: "Strona główna" },
    { path: '/book', strict: true, render: <BookComponent />, auth: true },
    { path: '/flights', strict: true, render: <UserFlightsComponent />, auth: true, drawer: true, title: "Moje loty"},
    { path: '/myProfile', strict: true, render: <UserSettingsPage />, auth: true },
    { path: '/admin/countries', strict: true, render: <CountriesPage />, auth: true, drawer: true, title: "Państwa", admin: true },
    { path: '/admin/airports', strict: true, render: <AirportsPage />, auth: true, drawer: true, title: "Lotniska", admin: true },
    { path: '/admin/airportConnections', strict: true, render: <AirportConnectionsPage />, auth: true, drawer: true, title: "Połączenia lotnicze", admin: true },
    { path: '/admin/flights', strict: true, render: <FlightsPage />, auth: true, drawer: true, title: "Loty", admin: true },
]
