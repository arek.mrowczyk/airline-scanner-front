import { IConfiguration } from "./enviroment";

/**
 * Konfiguracja dla wersji produkcyjnej
 */
export const configProduction: IConfiguration = {
    api: 'http://localhost:8080'
};

export default configProduction;