import { IConfiguration } from "./enviroment";

/**
 * Konfiguracja dla wersji produkcyjno-developerskiej
 */
export const configProddev: IConfiguration = {
    api: 'http://localhost:8080'
};

export default configProddev;