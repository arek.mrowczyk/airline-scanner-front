import configDevelopment from "./enviroment.dev";
import configProduction from "./enviroment.prod";
import configProddev from "./enviroment.proddev";

/**
 * Model konfiguracji aplikacji
 */
export interface IConfiguration {
    api?: string;
}

/**
 * Konfiguracja aplikacji
 */
const Configuration = process.env.REACT_APP_ENV === 'development' ? configDevelopment : process.env.REACT_APP_ENV === 'proddev' ? configProddev : configProduction;

export default Configuration;