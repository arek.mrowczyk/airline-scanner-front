import { IConfiguration } from "./enviroment";

/**
 * Konfiguracja dla wersji deweloperskiej
 */
export const configDevelopment: IConfiguration = {
    api: ''
};

export default configDevelopment;