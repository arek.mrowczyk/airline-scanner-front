import { users, userMockPostAction, userMockGetAction } from "./data/user";
import { countries, countriesMockPostAction, countriesMockGetAction } from "./data/countries";
import { airports, airportsMockPostAction, airportsMockGetAction } from "./data/airports";

/**
 * Klasa reprezentująca informacje o Mock'ach
 */
interface MockInfo {

    /**
     * Ścieżki po jakich front będzie łączył się z backend'em
     */
    apiKeys: string[];

    /**
     * Dane mock'a
     */
    data: any;

    /**
     * Akcja wywoływana przy żądaniu POST
     * @param apiKey Ścieżka API
     * @param item Body jakie będzie przekazane w wyniku żądania POST
     */
    postAction: (apiKey: string, item: any) => any;

    /**
     * Akcja wywoływana przy żądaniu GET
     * @param apiKey Ścieżka API
     */
    getAction: (apiKey: string) => any;
}

/**
 * Lista mock'ów
 */
const mockTable: MockInfo[] = [
    {
        apiKeys: ['users', 'authentication/login', 'accounts'],
        data: users,
        postAction: userMockPostAction,
        getAction: userMockGetAction
    },
    {
        apiKeys: ['countries'],
        data: countries,
        postAction: countriesMockPostAction,
        getAction: countriesMockGetAction
    },
    {
        apiKeys: ['airports'],
        data: airports,
        postAction: airportsMockPostAction,
        getAction: airportsMockGetAction
    },
];

/**
 * Funkcja pobierająca mock po apiKey
 */
export function getMock<T>(apiKey: string): T {
    const data = mockTable.filter(x => x.apiKeys.findIndex(y => apiKey.startsWith(y)) !== -1)[0];
    return data.getAction(apiKey);
}

/**
 * Funckja wykonująca metodę akcji dla konkretnego mocka
 * @param item Obiekt przekazywany do mocka
 */
export function postMock<T>(apiKey: string, item: T): any {
    const data = mockTable.filter(x => x.apiKeys.findIndex(y => apiKey.startsWith(y)) !== -1)[0].postAction(apiKey, item);
    return data;
}