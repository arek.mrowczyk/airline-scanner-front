import { Country } from "../../models/api/country.model";

/**
 * Mock państw
 */
export const countries: Country[] = [
  {
    id: 1,
    name: "POLAND",
    code: "PL"
  },
  {
    id: 2,
    name: "BELARUS",
    code: "BS"
  },
  {
    id: 3,
    name: "RUSSIA",
    code: "RU"
  },
  {
    id: 4,
    name: "GERMANY",
    code: "GE"
  },
  {
    id: 5,
    name: "GREAT BRITAIN",
    code: "GB"
  },
  {
    id: 6,
    name: "FRANCE",
    code: "FR"
  },
  {
    id: 7,
    name: "SPAIN",
    code: "ES"
  },
  {
    id: 8,
    name: "PORTUGAL",
    code: "PR"
  },
  {
    id: 9,
    name: "ITALY",
    code: "IT"
  }
];


/**
 * Funkcja realizująca żądania POST
 * @param apiKey Ścieżka API
 */
export const countriesMockPostAction = (apiKey: string, item: any): any => {
  if (apiKey.startsWith('countries/delete')) {
    return deleteCountry(apiKey);
  }

  if (apiKey.startsWith('countries')) {
    return addOrEditCountry(item);
  }

};


/**
 * Funkcja realizująca żądania GET
 * @param apiKey Ścieżka API
 */
export const countriesMockGetAction = (apiKey: string): any => {

  if (apiKey.startsWith('countries')) {

    return getCountries();
  }

};



/**
 * Funkcja zwracająca listę państw
 */
const getCountries = (): Country[] => {
  return countries;
}


/**
 * Funkcja która dodaje lub edytuje państwo w zależności czy przekazany został id czy nie - jeśli został przekazany to edit jeśli nie to add...
 * @param item Państwo
 */
const addOrEditCountry = (item: any): Country => {
  if (item.id) {
    const index = countries.findIndex(c => c.id === item.id);
    if (index !== -1) {
      countries[index] = item;
      return item;
    }
  }
  const id = Math.max(...countries.map(country => country.id)) + 1;
  const country: any = {
    id,
    ...item
  };
  countries.push(country);
  return country;
}


/**
 * Funkcja która usuwa państwo z listy
 * @param apiKey apiKey forwardowany do funkcji - zawiera id kraju do usunięcia
 */
const deleteCountry = (apiKey: string): boolean => {
  const countryId = apiKey.replace('countries/delete/','');
  const index = countries.findIndex(c => c.id.toString() === countryId);
  console.log({ index });
  if (index !== -1) {
    countries.splice(index, 1);
    return true;
  }
  return false;
}