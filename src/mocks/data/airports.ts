import { Airport } from "../../models/api/airport.model";

/**
 * Mock lotnisk
 */
export const airports: Airport[] = [{
    id: 1,
    airportCode: "KRK",
    airportName: "KRAKOW JOHN PAUL II INTERNATIONAL AIRPORT",
    cityName: "KRAKOW",
    cityCode: "KRK",
    country: {
        id: 1,
        name: "POLAND",
        code: "PL"
    }
},
{
    id: 2,
    airportCode: "MNS",
    airportName: "MINSK NATIONAL AIRPORT",
    cityName: "MINSK",
    cityCode: "MNS",
    country: {
        id: 2,
        name: "BELARUS",
        code: "BS"
    }
},
{
    id: 3,
    airportCode: "MSCD",
    airportName: "DOMODEDOVO INTERNATIONAL AIRPORT",
    cityName: "MOSCOW",
    cityCode: "MSC",
    country: {
        id: 3,
        name: "RUSSIA",
        code: "RU"
    }
},
{
    id: 4,
    airportCode: "MSCP",
    airportName: "PULKOVO AIRPORT",
    cityName: "MOSCOW",
    cityCode: "MSC",
    country: {
        id: 3,
        name: "RUSSIA",
        code: "RU"
    }
},
{
    id: 5,
    airportCode: "HAM",
    airportName: "HAMBURG LUBEKA",
    cityName: "HAMBURG",
    cityCode: "HAM",
    country: {
        id: 4,
        name: "GERMANY",
        code: "GE"
    }
},
{
    id: 6,
    airportCode: "BER",
    airportName: "BERLIN BRANDENBURG WILLY BRANDT",
    cityName: "BERLIN",
    cityCode: "BER",
    country: {
        id: 4,
        name: "GERMANY",
        code: "GE"
    }
},
{
    id: 7,
    airportCode: "LDN",
    airportName: "HEATHROW AIRPORT",
    cityName: "LONDON",
    cityCode: "LDN",
    country: {
        id: 5,
        name: "GREAT BRITAIN",
        code: "GB"
    }
},
{
    id: 8,
    airportCode: "BIR",
    airportName: "BIRMINGHAM AIRPORT",
    cityName: "LONDON",
    cityCode: "BIR",
    country: {
        id: 5,
        name: "GREAT BRITAIN",
        code: "GB"
    }
},
{
    id: 9,
    airportCode: "PAR",
    airportName: "CHARLES DE GAULLE AIRPORT",
    cityName: "PARIS",
    cityCode: "PAR",
    country: {
        id: 6,
        name: "FRANCE",
        code: "FR"
    }
},
{
    id: 10,
    airportCode: "MAD",
    airportName: "ADOLFO SUAREZ MADRID–BARAJAS AIRPORT",
    cityName: "MADRID",
    cityCode: "MAD",
    country: {
        id: 7,
        name: "SPAIN",
        code: "ES"
    }
},
{
    id: 11,
    airportCode: "LSB",
    airportName: "LISBONA PORTELA",
    cityName: "LISBONA",
    cityCode: "LSB",
    country: {
        id: 8,
        name: "PORTUGAL",
        code: "PR"
    }
},
{
    id: 12,
    airportCode: "ROM",
    airportName: "ROMA FLUMICINO",
    cityName: "ROMA",
    cityCode: "ROM",
    country: {
        id: 9,
        name: "ITALY",
        code: "IT"
    }
}];


/**
 * Funkcja realizująca żądania POST
 * @param apiKey Ścieżka API
 */
export const airportsMockPostAction = (apiKey: string, item: any): any => {
    if (apiKey.startsWith('airports/delete')) {
        return deleteAirport(apiKey);
    }

    if (apiKey.startsWith('airports')) {
        return addOrEditAirport(item);
    }

};


/**
 * Funkcja realizująca żądania GET
 * @param apiKey Ścieżka API
 */
export const airportsMockGetAction = (apiKey: string): any => {

    if (apiKey.startsWith('airports')) {

        return getAirports();
    }

};



/**
 * Funkcja zwracająca listę lotnisk
 */
const getAirports = (): Airport[] => {
    return airports;
}

/**
 * Funkcja która dodaje lub edytuje lotnisko w zależności czy przekazany został id czy nie - jeśli został przekazany to edit jeśli nie to add...
 * @param item Lotnisko
 */
const addOrEditAirport = (item: any): Airport => {
    if (item.id) {
        const index = airports.findIndex(c => c.id === item.id);
        if (index !== -1) {
            airports[index] = item;
            return item;
        }
    }
    const id = Math.max(...airports.map(airports => airports.id)) + 1;
    const airport: any = {
        id,
        ...item
    };
    airports.push(airport);
    return airport;
}


/**
 * Funkcja która usuwa lotnisko z listy
 * @param apiKey apiKey forwardowany do funkcji - zawiera id lotniska do usunięcia
 */
const deleteAirport = (apiKey: string): boolean => {
    const airportId = apiKey.replace('airports/delete/', '');
    const index = airports.findIndex(c => c.id.toString() === airportId);
    if (index !== -1) {
        airports.splice(index, 1);
        return true;
    }
    return false;
}