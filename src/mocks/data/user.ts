import { User } from "../../models/api/user.model";
import { RegisterApiForm } from "../../models/api/registerApiForm.model";
import { LoginApiForm } from "../../models/api/loginApiForm.model";

/**
 * Mock kont użytkownika
 */
export const users: User[] = [];

/**
 * Funkcja realizująca żądania POST
 * @param apiKey Ścieżka API
 */
export const userMockPostAction = (apiKey: string, item: any): any => {
    if (apiKey.startsWith('authentication/login')) {
        return login(item);
    }

    if (apiKey.startsWith('accounts')) {
        return userCreate(item);
    }
};

/**
 * Funkcja realizująca żądania GET
 * @param apiKey Ścieżka API
 */
export const userMockGetAction = (apiKey: string): any => {

    if (apiKey.startsWith('accounts/login/')) {
        return userCheck(apiKey.replace('accounts/login/', ''));
    }

};

/**
 * Funkcja sprawdzająca czy dany użytkownik istnieje
 * @param item Nazwa użytkownika
 */
const userCheck = (item: string): User | undefined => {
    if (item === 'user') {
        return {
            id: 4,
            login: "arek",
            password: "arek",
            firstName: "arek",
            lastName: "mrowczyk",
            dateOfBirth: [2020, 4, 26],
            balance: 5000,
            creationDate: [2020, 4, 26, 16, 6, 41, 925627000],
            enabled: true,
            username: "arek",
            authorities: [{authority: "USER"}],
            accountNonExpired: true,
            accountNonLocked: true,
            credentialsNonExpired: true
        };

    }
    return undefined;
}

/**
 * Funkcja tworząca użytkownika
 * @param item Formularz rejestracyjny
 */
const userCreate = (item: RegisterApiForm): boolean => {
    const id = users.length;

    const user = {
        id,
        login: item.login,
        password: item.password,
        firstName: item.firstName,
        lastName: item.lastName,
        dateOfBirth: [2020, 4, 26],
        balance: 5000,
        creationDate: [2020, 4, 26, 16, 6, 41, 925627000],
        enabled: true,
        username: item.login,
        authorities: [{authority: "USER"}],
        accountNonExpired: true,
        accountNonLocked: true,
        credentialsNonExpired: true
    }

    users.push(user);

    return true;
}

/**
 * Funkcja logująca użytkownika
 * @param item Formularz logowania
 */
const login = (item: LoginApiForm): any => {
    if (item) {
        if (item.login === 'user' && item.password === '123123123') {
            return null;
        }
    }
    return 'error';
}