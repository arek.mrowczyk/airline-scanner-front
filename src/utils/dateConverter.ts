
/**
 * Konwerter daty
 * @param date Data typu string
 * @returns Zwraca odpowiedno sformatowaną datę w typie string
 */
export const dateConverter = (date: string): string => {
    const dat = new Date(date);

    const day = dat.getDate() > 9 ? dat.getDate() : `0${dat.getDate()}`;
    const month = dat.getMonth() > 9 ? dat.getMonth() : `0${dat.getMonth()}`;

    return `${day}-${month}-${dat.getFullYear()}`;
}


export const dateConverterFromNumberArray = (date: number[]) => {
    if(date.length === 3) return `${date[0]}-${String('00' + date[1]).slice(-2)}-${String('00' + date[2]).slice(-2)}`;
    return `${date[0]}-${String('00' + date[1]).slice(-2)}-${String('00' + date[2]).slice(-2)} ${String('00' + date[3]).slice(-2)}:${String('00' + date[4]).slice(-2)}`;
}