
/**
 * Funkcja która wykonuje określone metody w zależności od tego jaka wersja aplikacji jest uruchomiona
 * @param devAction Akcja dla wersji deweloperskiej
 * @param proddevAction Akcja dla wersji dewelopersko-produkcyjnej
 * @param prodAction Akcja dla wersji produkcyjnej
 */
export function doForBuildType<T = void>(devAction?: () => T, proddevAction?: () => T, prodAction?: () => T): T | void {
    if (process.env.REACT_APP_ENV === 'development' && devAction) {
        return devAction();
    } else if (process.env.REACT_APP_ENV === 'proddev' && proddevAction) {
        return proddevAction();
    } else if (prodAction) {
        return prodAction();
    }
}