import React from 'react';
import { shallow } from 'enzyme';
import { Provider } from '../../injector/providers';
import { inject } from '../../injector/injector';

class Test1 {
    public a = 1;
}

class Test2 {

    constructor(public t1: Test1) { }
}

class Test3 {
    constructor(public t2: Test2) { }
}

class Test4 {
    constructor(public t3: Test3, public tt: string){}
}

describe('injector tests', () => {

    const providers: Provider[] = [
        { provider: 'test1', useClass: Test1 },
        { provider: Test2, useDeps: [Test1] },
        { provider: Test1 },
        { provider: Test3, useDeps: [Test2] },
        { provider: Test4, useDeps: [Test3, 'value']}
    ];

    it('test for string provider', () => {
        const a = inject('test1', providers) as Test1;

        expect(a.a).toBe(1);
    })

    it('test for multi-layered provider', () => {
        const a = inject(Test3, providers) as Test3;

        expect(a.t2.t1.a).toBe(1);
    })

    it('test for multi-layered provider with values', () => {
        const a = inject(Test4, providers) as Test4;

        expect(a.t3.t2.t1.a).toBe(1);
        expect(a.tt).toBe('value');
    })
});