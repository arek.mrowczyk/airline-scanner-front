import React from 'react';
import { shallow } from 'enzyme';
import { Provider } from '../../injector/providers';
import { inject } from '../../injector/injector';
import { ValidationBuilder } from '../../validators/builder/validation.builder';
import { FormControl } from '../../models/formControl.model';
import { requiredValidatorBuilder } from '../../validators/register/required.validator';
import { usernameValidator } from '../../validators/register/username.validator';
import { registerPasswordValidator } from '../../validators/register/registerPassword.validator';
import { acceptedValidator } from '../../validators/register/accepted.validator';

describe('register form validator tests', () => {

    const validator = inject(ValidationBuilder) as ValidationBuilder<FormControl[], { fieldName: string }>;

    validator.useValidators(requiredValidatorBuilder(false), registerPasswordValidator, acceptedValidator);
    validator.useAsyncValidators(usernameValidator);

    const fields: FormControl[] = [
        { fieldName: 'username', touched: false, fieldValue: '', error: { message: '', info: {} } },
        { fieldName: 'password', touched: false, fieldValue: '', error: { message: '', info: {} } },
        { fieldName: 'confirmPassword', touched: false, fieldValue: '', error: { message: '', info: {} } },
        { fieldName: 'firstName', touched: false, fieldValue: '', error: { message: '', info: {} } },
        { fieldName: 'lastName', touched: false, fieldValue: '', error: { message: '', info: {} } },
        { fieldName: 'dateOfBirth', touched: false, fieldValue: '', error: { message: '', info: {} } },
        { fieldName: 'accepted', touched: false, fieldValue: false, error: { message: '', info: {} } }
    ];

    it('all form is empty', async () => {

        const result = await validator.validate(fields);

        expect(result[0].info.fieldName).toBe('username');
        expect(result[0].message).toBe('To pole jest wymagane');

        expect(result[1].info.fieldName).toBe('password');
        expect(result[1].message).toBe('To pole jest wymagane');

        expect(result[2].info.fieldName).toBe('confirmPassword');
        expect(result[2].message).toBe('To pole jest wymagane');

        expect(result[3].info.fieldName).toBe('firstName');
        expect(result[3].message).toBe('To pole jest wymagane');

        expect(result[4].info.fieldName).toBe('lastName');
        expect(result[4].message).toBe('To pole jest wymagane');

        expect(result[5].info.fieldName).toBe('dateOfBirth');
        expect(result[5].message).toBe('To pole jest wymagane');

        expect(result[6].info.fieldName).toBe('accepted');
        expect(result[6].message).toBe('Musisz zaakceptować warunki');

    })

    it('few fields filled', async () => {

        const firstNameField = fields[3];
        const lastNameField = fields[4];

        firstNameField.fieldValue = 'test';
        lastNameField.fieldValue = 'test';

        validator.clearErrors();

        const result = await validator.validate(fields);

        expect(result[0].info.fieldName).toBe('username');
        expect(result[0].message).toBe('To pole jest wymagane');

        expect(result[1].info.fieldName).toBe('password');
        expect(result[1].message).toBe('To pole jest wymagane');

        expect(result[2].info.fieldName).toBe('confirmPassword');
        expect(result[2].message).toBe('To pole jest wymagane');

        expect(result[3].info.fieldName).toBe('dateOfBirth');
        expect(result[3].message).toBe('To pole jest wymagane');

        expect(result[4].info.fieldName).toBe('accepted');
        expect(result[4].message).toBe('Musisz zaakceptować warunki');
    })

    it('password field test', async () => {
        
        const passwordField = fields[1];
        const confirmPasswordField = fields[2];

        passwordField.fieldValue = 'test';

        validator.clearErrors();

        const result1 = await validator.validate(fields);
        const passwordError = result1.filter(x => x.info.fieldName === 'password')[0];

        expect(passwordError.message).toBe('Hasła różnią się od siebie');

        confirmPasswordField.fieldValue = 'test';

        validator.clearErrors();

        const result2 = await validator.validate(fields);
        const passwordError2 = result2.find(x => x.info.fieldName === 'password');

        expect(passwordError2).toBeUndefined();

    })

    it('username field test', async () => {
        
        const usernameField = fields[0];

        if(process.env.REACT_APP_ENV === 'development') {

            usernameField.fieldValue = 'user';

            validator.clearErrors();

            const result = await validator.validate(fields);

            const usernameError = result.filter(x => x.info.fieldName === 'username')[0];

            expect(usernameError.message).toBe('Nazwa użytkownika jest używana');
        }

    })

})