# Airline Scanner

Tutaj będziemy zapisywać instrukcje, wskazówki lub użycia niektórych rzeczy z kodu. Proponuję żeby to tak zrobić żeby potem nie szukać nie wiadomo ile. 

## Wskazówki do Frontend'u

### **Wstrzykiwanie zależności**

Wstrzykiwanie zależności odbywa się tak, że tworzymy serwis tak jak zwykle. Dodajemy do konstruktora to czego potrzebuje. Potem gdy serwis będzie skończony przechodzimy do pliku `providers.ts`. Tam jest lista wszystkich providerów w aplikacji. W tym pliku dodajemy wpis z naszym serwisem. Obiekt `Provider` składa się z pól `provider` oraz `useDeps`. Dla pola `provider` podajemy referencję do naszego utworzonego serwisu. A do listy `useDeps` podajemy (o ile to możliwe) referencje do obiektów, które znajdują się w konstruktorze. Gdy to będzie zrobione wtedy możemy wywołać funkcję `inject` podając jej nasz serwis.

*Próbowałem wcześniej zrobić wstrzykiwanie zależności takie jak jest w np. Angularze ale nie udało się tak jak z paczkami z npm. Bo Babel domyślnie nie obsługuje emitowania metadanych więc nasz dekorator klasy gówno będzie wiedział jakie pola ma konstruktor itd. Dlatego to działa tak jak działa.*

### **Konfiguracja**

Konfiguracja jest w taki sposób zrobiona, że mamy jej dwie wersje `development` oraz `production`. Jedna albo druga jest uruchamiana w zależności od trybu w jakim aplikacja jest uruchomiona. Konfiguracja znajduje się w folderze `config` a w nim są 2 pliki `enviroment.dev.ts` oraz `enviroment.prod.ts`. Ten pierwszy jest `development` a drugi `production`. Jak chcemy coś dodać do konfiguracji musimy do tych obu plików dodać to pole, a w pliku `enviroment.ts` do interfejsu różnież to pole dodać. W kodzie jeżeli chcemy użyć jakiegoś pola z konfiguracji odwołujemy się tylko do `Configuration` z pliku `enviroment.ts`.

### **Walidacja**

Walidacja została zrobiona w taki sposób, że mamy klasę `ValidationBuilder`, która przyjmuje walidatory (tworzone typem generycznym `Validator<TObject, TResult>`) będące funkcjami o argumencie typu takiego jaki chcemy zwalidować i jednocześnie zwracające generyczny obiekt klasy `ValidationError<T>`, gdzie typ generyczny zawiera dodatkowe informacje. Gdy utworzymy taki builder potem wywołujemy metodę `validate`. Po wykonaniu, której otrzymamy listę obiektów `ValidationError<T>`. Jeżeli walidacja nie przebiegła pomyślnie, wtedy w polu `message` klasy `ValidationError<T>` będzie komunikat błędu. Jeżeli walidacja przeszła pomyślnie pole `message` będzie puste. Według tego schematu powinny być tworzone walidatory. Klasę `ValidationBuilder` można wstrzyknąć.

### **Redux**

Redux jest rozwiązany w takim schemacie: w folderze `store` mamy interfejs `IAppState`, który jest typem globalnego stanu aplikacji. Potem mamy `combineReducers`, gdzie mamy przypisywanie do pól stanu reducerów. A na końcu udostępniamy nasz `store`. Potem mamy funkcje akcji (w folderze `actions`), które przyjmują parametry lub nie i zwracają obiekt, który potem będzie przekazany do reducera (folder `reducers`) za pomocą funkcji `dispatch`.

### **Komponenty i typy**

Każdy komponent, klasę lub też funkcję starajmy się otypować jak się tylko ją dla. Żeby nie było potem problemów przy debugowaniu czy coś. Znacznie to pomaga tworzenie komponentów oraz można uniknąć wiele błędów.

## Wskazówki do Backend'u

## Wskazówki do bazy